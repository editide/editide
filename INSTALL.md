EdiTidE - Install
=================

EdiTidE can be run directly from the sources.
A few extra steps are recommended for a better system integration,
as detailed below.


Dependencies
------------

Mandatory dependencies:

* python3 >= 3.11
* pygobject-3.0
* gtk4 >= 4.14
* gtksourceview-5 >= 5.8

Recommended dependencies:

* grep >= 3.4                   (for searching)
* universal-ctags >= 5.9.2021   (for symbols)

Optional dependencies:

* vte-3.91      (for the terminal extension)
* libadwaita    (for the Adwaita theme and recoloring)
* libspelling   (for the spellchecking extension)
* git           (for the git extension)
* dos2unix      (for the eol extension)
* DejaVu font   (for better rendering on Microsoft Windows)


Linux
-----

* Dependencies should already be available on most Linux systems.
  Use your package manager to install the missing ones.

* Put EdiTidE files in your Python site-packages,
  e.g. `/usr/lib/python3.x/site-packages/editide/`.

* Copy or link `editide.desktop` to `/usr/share/applications/`.


Microsoft Windows
-----------------

* Get [MSYS2](https://www.msys2.org/) 64bits and install it,
  typically under `C:/msys64/` (adapt paths below to your setup).

* Launch MSYS2 terminal `C:/msys64/ucrt64.exe` and install dependencies:

```
    $ pacman -Syuu     # repeat until system is fully up-to-date
    $ pacman -S ${MINGW_PACKAGE_PREFIX}-python-gobject
    $ pacman -S ${MINGW_PACKAGE_PREFIX}-gtksourceview5
    $ pacman -S ${MINGW_PACKAGE_PREFIX}-grep
    $ pacman -S ${MINGW_PACKAGE_PREFIX}-ctags
    $ pacman -S ${MINGW_PACKAGE_PREFIX}-ttf-dejavu
```

* Put EdiTidE files in your Python site-packages,
  e.g. `C:/msys64/ucrt64/lib/python3.x/site-packages/editide/`.

* Optionally run `make && make install && make clean` from the win32 subfolder
  to generate a native `editide.exe` launcher.

* Adapt the start script `editide.cmd` to match your setup.

* Create a shortcut to `editide.cmd`, open properties, set the "Run" setting
  to "minimized", then place the shortcut in your start menu and/or in your
  "SendTo" folder for easily opening files from the Explorer.

* Open Windows font settings, install the DejaVu font by drag-n-drop TTF files
  from `C:/msys64/ucrt64/share/fonts/TTF/DejaVuSans*.ttf`.

* Optionally, if using a low-resolution display, add the following exports
  to `~/.bashrc` for a crisp grid-fitted font rendering:

```
    export PANGOCAIRO_BACKEND=fc
    export FREETYPE_PROPERTIES="truetype:interpreter-version=35"
```
