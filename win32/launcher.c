/**
* @copyright SPDX-License-Identifier: CC0-1.0
* @copyright SPDX-FileCopyrightText: NONE
* @see https://docs.python.org/3/extending/embedding.html
* @see https://docs.python.org/3/c-api/init_config.html
*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>


int main(int argc, char *argv[])
{
    PyStatus status;
    PyConfig config;

    PyConfig_InitPythonConfig(&config);

    /* Config input parameters */
    (void) PyConfig_SetBytesString(&config, &config.home, Py_STRINGIFY(PY_HOME));
    config.write_bytecode = 0;   /* don't write pycache */
    config.parse_argv = 0;       /* pass all CLI parameters to application */
    config.faulthandler = 1;     /* improve tracebacks */

    /* Decode command line arguments */
    status = PyConfig_SetBytesArgv(&config, argc, argv);
    if (PyStatus_Exception(status)) {
        goto exception;
    }

    /* Configure target */
    (void) PyConfig_SetBytesString(&config, &config.program_name, Py_STRINGIFY(PY_PROGNAME));
    (void) PyConfig_SetBytesString(&config, &config.run_module, Py_STRINGIFY(PY_MODULE));

    /* Initialize Python */
    status = Py_InitializeFromConfig(&config);
    if (PyStatus_Exception(status)) {
        goto exception;
    }

    PyConfig_Clear(&config);

    /* Run config.run_module */
    return Py_RunMain();

exception:
    PyConfig_Clear(&config);

    if (PyStatus_IsExit(status)) {
        return status.exitcode;
    }

    /* Display the error message and exit the process with non-zero exit code */
    Py_ExitStatusException(status);   /* noreturn */
}
