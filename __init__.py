#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Package initialization."""

import builtins
import gettext
import sys

import gi


# Check minimal Python version
assert sys.version_info >= (3, 11)

# Introspection deps
if hasattr(gi, 'disable_legacy_autoinit'):
    gi.disable_legacy_autoinit()
gi.require_version('Gdk', '4.0')
gi.require_version('Gtk', '4.0')
gi.require_version('GtkSource', '5')

# Misc builtins
builtins.ED_APPNAME = 'editide'
builtins.ED_WEBSITE = "https://editide.frama.io/"
builtins.ED_VERSION = '4.dev0'
builtins.ED_ICON = 'editide'
builtins.ED_PATH = __path__[0]
builtins.ED_DEBUG = sys.argv.count('--debug')
builtins.ed_log = lambda *_a: print(*_a, sep=":\t", file=sys.stderr) if ED_DEBUG else None

# Translations
gettext.install(ED_APPNAME)
