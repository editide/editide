#!/bin/sh
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

cd "$(dirname "${BASH_SOURCE[0]}")"

app="editide"
iconslist=""

for dim in 16 22 24 32 48 64 96 128 256 ; do
	dimdir="${dim}x${dim}"
	icondir="./hicolor/${dimdir}/apps"
	iconfile="${icondir}/${app}.png"
	test -d "${icondir}" || mkdir -p "${icondir}"
	test -f "${iconfile}" && rm "${iconfile}"

	rsvg-convert \
		--output="${iconfile}" \
		--width=${dim} \
		--height=${dim} \
		"./hicolor/scalable/apps/${app}.svg"

	iconslist="${iconslist} ${iconfile}"
done

test -d "../../win32" && magick ${iconslist} "../../win32/${app}.ico"

exit 0
