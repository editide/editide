#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Embedded Python interpreter."""

import importlib

from gi.repository import GLib, Gdk, Gtk, GtkSource

from .. import __spec__ as ed_pkgspec
from ..core.utils.misc import ed_set_textbuffer_defaults


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    return {'py': (cmd_py, _("Python Interpreter"), False, [])}


# Open an embedded Python interpreter in the bottom pane.
# Syntax:
#     `py`
# Parameters are ignored.
#
def cmd_py(wbench, doc, *param):
    """Start the Python interpreter."""
    stack = wbench.get_ui('ed_stack_bottom')
    if stack.get_child_by_name('ext-pyconsole'):
        stack.set_visible_child_name('ext-pyconsole')
    else:
        pyconsole = EdExtPyInterpreterWidget(wbench)
        stack.add_titled(pyconsole, 'ext-pyconsole', _("Python"))
    wbench.get_ui('ed_box_bottom').set_visible(True)
    return True


class EdExtPyInterpreterWidget(Gtk.Box):
    """Embedded Python interpreter widget."""
    __gtype_name__ = __qualname__
    WELCOME = _("*** Python Interpreter ***\n'wbench' holds the current workbench.\n")

    def __init__(self, wbench):
        super().__init__(orientation='vertical')
        mod = importlib.import_module(".helpers.interpreter", __spec__.parent)
        self.interpreter = mod.EdExtPyInterpreter({'wbench': wbench, '__spec__': ed_pkgspec})
        self.input = []
        self.history = []
        self.hist_pos = 0
        self.prompt = ">>>"
        self.plabel = Gtk.Label(label=self.prompt, single_line_mode=True)
        self.plabel.set_direction(Gtk.TextDirection.LTR)
        self.entry = Gtk.Entry(activates_default=True, has_frame=False, hexpand=True, secondary_icon_name='edit-clear-symbolic')
        self.entry.set_direction(Gtk.TextDirection.LTR)
        logview = GtkSource.View(editable=False, cursor_visible=False, wrap_mode='char', bottom_margin=3)
        self.logbuf = logview.get_buffer()
        self.logbuf.set_highlight_matching_brackets(False)
        self.logbuf.set_style_scheme(None)
        self.logbuf.set_text(self.WELCOME + " ")
        ed_set_textbuffer_defaults(self.logbuf)
        for w in (self.plabel, self.entry, logview):
            w.add_css_class('monospace')
            w.add_css_class('ed-class-sourcecode')
        self.scrollwin = Gtk.ScrolledWindow(has_frame=False, vexpand=True)
        self.scrollwin.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.ALWAYS)
        self.scrollwin.set_child(logview)
        ebox = Gtk.Box(orientation='horizontal', spacing=6)
        ebox.set_direction(Gtk.TextDirection.LTR)
        ebox.append(self.plabel)
        ebox.append(self.entry)
        self.append(self.scrollwin)
        self.append(ebox)
        logview.get_gutter(Gtk.TextWindowType.LEFT).insert(EdExtPyPromptRenderer(), 0)
        # Accessibility
        self.entry.update_property([Gtk.AccessibleProperty.LABEL], [_("Python commands input")])
        logview.update_property([Gtk.AccessibleProperty.LABEL], [_("Python output")])
        # Events
        ev_key = Gtk.EventControllerKey(name='ed-key')
        ev_key.connect('key-pressed', self.on_key_press)
        ebox.add_controller(ev_key)
        self.entry.connect('activate', self.on_activate)
        self.entry.connect('icon-press', self.on_clear)
        self.connect('notify::parent', self.on_reparent)
        self.connect('map', self.on_map)

    def scroll_end(self):
        """Scroll down output log to the end."""
        adj = self.scrollwin.get_vadjustment()
        adj.set_value(adj.get_upper())

    def on_clear(self, entry, pos):
        """Callback on entry's clear icon pressed."""
        entry.set_text("")
        if self.prompt == "...":
            self.input = ["print('<UserCancelMultilineStatement>')"]
            self.on_activate(entry)

    def on_activate(self, entry):
        """Callback on activate."""
        code_input = entry.get_text().splitlines() or [""]
        self.history.extend(filter(None, code_input))
        self.hist_pos = len(self.history)
        for input_line in code_input:
            self.input.append(input_line)
            incomplete = False
            try:
                with self.interpreter.io_context():
                    incomplete = self.interpreter.runsource("\n".join(self.input), "<console>")
                entry.set_text("")
            except Exception as e:
                self.interpreter.write(f"### Exception: {e}\n")
            except SystemExit:
                GLib.idle_add(self.on_exit, priority=GLib.PRIORITY_LOW)
            itr = self.logbuf.get_end_iter()
            if incomplete:
                self.logbuf.insert_with_tags_by_name(itr, f"\n{input_line}", self.prompt)
                self.prompt = "..."
            else:
                result = self.interpreter.get_output().removesuffix("\n").removesuffix("\r")
                result = "\n" + result if result else result
                self.logbuf.insert_with_tags_by_name(itr, f"\n{input_line}", self.prompt)
                self.logbuf.insert_with_tags_by_name(itr, result, 'result')
                self.input.clear()
                self.prompt = ">>>"
        self.plabel.set_text(self.prompt)
        GLib.idle_add(self.scroll_end)

    def on_key_press(self, event, keyval, keycode, state):
        """Callback on key press event."""
        if state == 0:   # no modifiers
            if keyval == Gdk.KEY_Up:
                if self.hist_pos > 0:
                    self.hist_pos -= 1
                    self.entry.set_text(self.history[self.hist_pos])
                    self.entry.set_position(-1)
                return Gdk.EVENT_STOP
            if keyval == Gdk.KEY_Down:
                last = len(self.history)
                if self.hist_pos < last:
                    self.hist_pos += 1
                    self.entry.set_text("" if last == self.hist_pos else self.history[self.hist_pos])
                    self.entry.set_position(-1)
                return Gdk.EVENT_STOP
        return Gdk.EVENT_PROPAGATE

    def on_map(self, w):
        """Callback on mapped."""
        GLib.idle_add(lambda: self.entry.grab_focus() and False)

    def on_reparent(self, w, pspec):
        """Callback on parent change."""
        if self.get_parent() is None:   # widget detached, going to be destroyed
            self.entry.disconnect_by_func(self.on_activate)
            self.disconnect_by_func(self.on_reparent)
            self.disconnect_by_func(self.on_map)
            self.interpreter.locals.clear()
            self.interpreter = None

    def on_exit(self):
        """Callback on interpreter exit."""
        self.get_parent().remove(self)


class EdExtPyPromptRenderer(GtkSource.GutterRendererText):
    """Gutter renderer for Python prompts."""
    __gtype_name__ = __qualname__

    def __init__(self):
        super().__init__(alignment_mode='first')
        self.set_direction(Gtk.TextDirection.LTR)
        self.tag_start = None
        self.tag_continue = None
        self.tag_result = None

    def update_tags_style(self):
        """Update tags to match the current theme."""
        c = self.get_view().get_color()
        c.alpha = 0.65
        tag = self.get_buffer().get_tag_table().lookup('result')
        tag.props.foreground_rgba = c

    def do_css_changed(self, change):   # override
        """Custom handler for style change."""
        GtkSource.GutterRendererText.do_css_changed(self, change)
        self.update_tags_style()

    def do_change_buffer(self, old_buffer):   # override
        """Custom handler for textbuffer change."""
        if new_buffer := self.get_buffer():
            w, h = self.measure(">>>")
            self.set_size_request(w + 7, -1)
            self.tag_start = new_buffer.create_tag('>>>')
            self.tag_continue = new_buffer.create_tag('...')
            self.tag_result = new_buffer.create_tag('result')
            self.update_tags_style()

    def do_query_data(self, gutter_lines, line):   # override
        """Custom handler for data query."""
        itr = gutter_lines.get_iter_at_line(line)
        if itr.has_tag(self.tag_start) or itr.ends_tag(self.tag_start):
            self.set_text(">>>", 3)
        elif itr.has_tag(self.tag_continue) or itr.ends_tag(self.tag_continue):
            self.set_text("...", 3)
        else:
            self.set_text("   ", 3)
