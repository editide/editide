#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for spellchecking."""

import importlib

import gi

from ..core.utils.misc import ed_isinstance


__all__ = ['ed_ext_init', 'ed_ext_notif_doc_added']


def ed_ext_init(app):
    """Initialize extension."""
    if not app.get_setting('ext_spellcheck', 'enable', bool, True):
        return
    try:
        global Spelling
        gi.require_version('Spelling', '1')
        Spelling = importlib.import_module('gi.repository.Spelling', __spec__.parent)
        Spelling.init()
    except Exception:
        ed_log("error", "could not initialize 'gi.repository.Spelling'")


def ed_ext_notif_doc_added(wbench, doc):
    """Notification on new document added to the workbench."""
    if 'Spelling' in globals() and ed_isinstance(doc, 'EdSrcEditor'):
        provider = Spelling.Provider.get_default()
        lang = provider.get_default_code()
        if user_lang := wbench.get_config('spell_lang', str):
            if provider.supports_language(user_lang):
                lang = user_lang
            else:
                ed_log("error", f"language '{user_lang}' not supported by the spellchecker")
        checker = Spelling.Checker.new(provider, lang)
        adapter = Spelling.TextBufferAdapter.new(doc.buffer, checker)
        doc.view.insert_action_group('spelling', adapter)
        doc.view.get_extra_menu().append_section(None, adapter.get_menu_model())
        active = wbench.app.get_setting('ext_spellcheck', 'default_active', bool, False)
        adapter.set_enabled(active)
