#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for changing text size."""

from gi.repository import GLib, Gio, Gtk


__all__ = ['ed_ext_init', 'ed_ext_notif_wbench_opened']


def ed_ext_init(app):
    """Initialize extension."""
    app.ext_data['zoom'] = {'factor': 100}
    app.ext_data['zoom']['default'] = app.get_setting('ext_zoom', 'default_size', str, "100%")
    app.ext_data['zoom']['cssp'] = app.apply_css(1)
    __ext_zoom_action(None, None)
    app.set_accels_for_action('win.zoom(0)', ('<Control>0', '<Control>KP_0'))
    app.set_accels_for_action('win.zoom(5)', ('<Control>plus', '<Control>KP_Add'))
    app.set_accels_for_action('win.zoom(-5)', ('<Control>minus', '<Control>KP_Subtract'))


def ed_ext_notif_wbench_opened(wbench):
    """Notification on new workbench."""
    action = Gio.SimpleAction.new('zoom', GLib.VariantType('i'))
    action.connect('activate', __ext_zoom_action)
    wbench.win.add_action(action)


def __ext_zoom_action(action, param):
    """Action handler for zoom in/out/reset."""
    inc = param.unpack() if param else 0
    app = Gio.Application.get_default()
    default = app.ext_data['zoom']['default']
    factor = 100 if inc == 0 else app.ext_data['zoom']['factor'] + inc
    if factor > 0:
        if Gtk.check_version(4, 16, 0):
            css = f"textview.ed-class-sourcecode {{font-size: {factor}%;}}"
        else:
            css = f"textview.ed-class-sourcecode {{font-size: calc({default} * {factor/100});}}"
        app.ext_data['zoom']['cssp'].load_from_string(css)
        app.ext_data['zoom']['factor'] = factor
