#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for tracking source editor modifications."""

from gi.repository import GLib, Gdk, Gtk, GtkSource, Graphene

from ..core.utils.misc import ed_isinstance


__all__ = ['ed_ext_init', 'ed_ext_notif_doc_added']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_notif_doc_added(wbench, doc):
    """Notification on new document added to the workbench."""
    if wbench.app.get_setting('ext_trackmod', 'enable', bool, False):
        if ed_isinstance(doc, 'EdSrcEditor'):
            color = wbench.app.get_setting('ext_trackmod', 'color', str, "#00CFBF80")
            renderer = EdExtModificationsRenderer(color)
            gutter = doc.view.get_gutter(Gtk.TextWindowType.LEFT)
            gutter.insert(renderer, GtkSource.ViewGutterPosition.LINES + 1)


class EdExtModificationsRenderer(GtkSource.GutterRenderer):
    """Simple gutter renderer tracking insertions/deletions with color in margin."""
    __gtype_name__ = __qualname__
    WIDTH = 3

    def __init__(self, color_spec):
        super().__init__(width_request=self.WIDTH)
        self.color = Gdk.RGBA()
        self.color.parse(color_spec)
        self.enabled = False
        self.tag = Gtk.TextTag.new('ed-change-tracker')
        self.quark = GLib.quark_from_string('changed')

    def on_modified_changed(self, buffer):
        """Callback on buffer changed flag."""
        # Don't track new documents, wait for file load or save
        if not buffer.get_modified():
            self.enabled = True

    def on_insert_text(self, buffer, where, txt, txtlen):
        """Callback on text inserted into buffer."""
        if self.enabled and self.get_view().get_editable() and not buffer.get_loading():
            ibeg = where.copy()
            ibeg.backward_chars(txtlen)
            buffer.apply_tag(self.tag, ibeg, where)
            self.queue_draw()

    def on_delete_range(self, buffer, ibeg, iend):
        """Callback on text removed from buffer."""
        if self.enabled and self.get_view().get_editable() and not buffer.get_loading():
            iend2 = iend.copy()
            iend2.forward_char()
            buffer.apply_tag(self.tag, ibeg, iend2)
            self.queue_draw()

    def do_change_buffer(self, old_buffer):   # override
        """Custom handler for textbuffer change."""
        if old_buffer:
            old_buffer.disconnect_by_func(self.on_modified_changed)
            old_buffer.disconnect_by_func(self.on_insert_text)
            old_buffer.disconnect_by_func(self.on_delete_range)
        if new_buffer := self.get_buffer():
            new_buffer.get_tag_table().add(self.tag)
            self.tag.set_priority(0)   # lowest prio
            new_buffer.connect('modified-changed', self.on_modified_changed)
            new_buffer.connect_after('insert-text', self.on_insert_text)
            new_buffer.connect_after('delete-range', self.on_delete_range)

    def do_query_data(self, gutter_lines, line):   # override
        """Custom handler for data query."""
        itr = gutter_lines.get_iter_at_line(line).copy()
        ok = itr.forward_to_tag_toggle(self.tag)
        if ok and (itr.ends_tag(self.tag) or itr.get_line() == line):
            gutter_lines.add_qclass(line, self.quark)
        else:
            gutter_lines.remove_qclass(line, self.quark)

    def do_snapshot_line(self, snap, gutter_lines, line):   # override
        """Custom handler for drawing."""
        if gutter_lines.has_qclass(line, self.quark):
            y, h = gutter_lines.get_line_yrange(line, GtkSource.GutterRendererAlignmentMode.CELL)
            rect = Graphene.Rect().init(0.0, y, self.WIDTH, h)
            snap.append_color(self.color, rect)
