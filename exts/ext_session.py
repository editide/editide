#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Session management."""

import json

from gi.repository import Gio

from ..core.ui.dialogs import EdFileChooser
from ..core.utils.misc import ed_get_gfile_display_path


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    return {
        'session': (cmd_session, _("Session Management"), True, ["save", "restore"]),
    }


def __ext_session_save(fc, wbench, session):
    """Callback on filechooser for saving."""
    if gfile := fc.get_file():
        try:
            contents = bytes(json.dumps(session), 'utf-8')
            gfile.replace_contents(contents, None, False, Gio.FileCreateFlags.NONE, None)
            wbench.notifier.status(_("Session saved"))
        except Exception as e:
            wbench.notifier.error(_("Unable to save session!"), f"{e}")


def __ext_session_load(fc, wbench):
    """Callback on filechooser for loading."""
    session = {}
    if gfile := fc.get_file():
        try:
            success, contents, etag = gfile.load_contents(None)
            if success:
                session = json.loads(contents)
        except Exception as e:
            wbench.notifier.error(_("Unable to load session!"), f"{e}")
    if session.get('version', 0) == 1 and 'files' in session:
        # Restore files with context
        for uri, ctx in session['files']:
            ext_hints = {_k: _v for _k, _v in ctx.items() if _k.isidentifier()}
            wbench.app.on_open(wbench.app, [Gio.File.new_for_uri(uri)], 1, "", ext_hints)
    else:
        sec = ed_get_gfile_display_path(gfile) if gfile else None
        wbench.notifier.error(_("Invalid session file!"), sec)


# Save and restore session files.
# Syntax:
#     `session <save|restore>`
# Session files store the current project (if any), the open files, and bookmarks.
#
def cmd_session(wbench, doc, action="", /):
    """Command for managing sessions."""
    if action == "save":
        # Collect session contexts
        session = {'version': 1, 'files': []}
        if wbench.project is not None:
            session['files'].append((wbench.get_prj_gpath().get_uri(), {}))
        for d in wbench.get_docs_sorted():
            gf = d.get_gfile()
            if gf is not None:
                session['files'].append((gf.get_uri(), d.get_context()))
        # Save to file
        gpath = wbench.get_prj_gpath()
        fname = ("" if wbench.project is None else wbench.project.get_name()) + ".session"
        fc = EdFileChooser(wbench.win, _("Save Session"))
        fc.set_response_handler(__ext_session_save, wbench, session)
        fc.show_save(folder=gpath, name=fname)
    elif action == "restore":
        # Open session backup
        fc = EdFileChooser(wbench.win, _("Open Session"))
        fc.set_response_handler(__ext_session_load, wbench)
        fc.show_open(folder=wbench.get_prj_gpath())
    else:
        return False
    return True
