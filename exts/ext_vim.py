#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for emulating Vim keyboard shortcuts."""

import weakref

from gi.repository import GObject, Gtk, GtkSource

from ..core.utils.misc import ed_isinstance


__all__ = ['ed_ext_init', 'ed_ext_notif_doc_added']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_notif_doc_added(wbench, doc):
    """Notification on new document added to the workbench."""
    if not wbench.app.get_setting('ext_vim', 'enable', bool, False):
        return
    if ed_isinstance(doc, 'EdSrcEditor'):
        im = GtkSource.VimIMContext.new()
        im.set_client_widget(doc.view)
        im.connect('edit', __ext_vim_edit, weakref.ref(doc))
        im.connect('write', __ext_vim_write, weakref.ref(doc))
        im.connect('execute-command', __ext_vim_execute_command, weakref.ref(doc))
        ev_key = Gtk.EventControllerKey(propagation_phase='capture')
        ev_key.set_im_context(im)
        doc.view.add_controller(ev_key)
        # UI feedback
        cmd_bar = Gtk.Label(visible=False, single_line_mode=True, tooltip_text=_("Vim command"))
        cmd = Gtk.Label(visible=False, single_line_mode=True, tooltip_text=_("Vim command"))
        cmd_bar.add_css_class('accent')
        cmd.add_css_class('accent')
        cmd_bar.connect('notify::label', lambda _l, _p: _l.set_visible(len(_l.get_label()) > 0))
        cmd.connect('notify::label', lambda _l, _p: _l.set_visible(len(_l.get_label()) > 0))
        im.bind_property('command-bar-text', cmd_bar, 'label', GObject.BindingFlags.SYNC_CREATE)
        im.bind_property('command-text', cmd, 'label', GObject.BindingFlags.SYNC_CREATE)
        prev = doc.get_doc_ui('ed_spinner_doc')
        box = prev.get_parent()
        box.insert_child_after(cmd, prev)
        box.insert_child_after(cmd_bar, prev)


def __ext_vim_edit(im, view, path, doc_ref):
    """Callback on Vim edit request."""
    doc = doc_ref()
    if path:
        doc.wbench.cmdlauncher.run("file", path)
    else:
        doc.wbench.cmdlauncher.run("reload")


def __ext_vim_write(im, view, path, doc_ref):
    """Callback on Vim write request."""
    doc = doc_ref()
    if path:
        if gf := doc.get_gfile():
            doc.wbench.save_doc_as(doc, gfile=gf.get_parent().resolve_relative_path(path))
        else:
            doc.wbench.save_doc_as(doc, name=path)
    else:
        doc.wbench.save_doc(doc)


def __ext_vim_execute_command(im, cmd, doc_ref):
    """Callback on Vim command execution request."""
    doc = doc_ref()
    ret = True
    match cmd:
        case ":n":
            # New window
            doc.wbench.win.activate_action('win.newwbench', None)
        case ":wq" | ":x":
            # Save current file and close it
            doc.wbench.save_doc(doc, True)
        case ":q" | ":quit":
            # Close file
            doc.wbench.close_doc_request(doc.get_widget())
        case ":q!" | ":quit!":
            # Close file without saving
            doc.wbench.close_doc(doc)
        case ":vsp" | ":vsplit":
            # Split vertically
            doc.wbench.cmdlauncher.run("layout", "v")
        case ":sp" | ":split":
            # Split horizontally
            doc.wbench.cmdlauncher.run("layout", "h")
        case _:
            ret = False
    return ret
