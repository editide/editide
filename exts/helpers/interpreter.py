#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Python interactive interpreter, improved."""

import builtins
import code
import contextlib
import io
import sys


__all__ = ['EdExtPyInterpreter']


class EdExtPyInterpreter(code.InteractiveInterpreter):
    """InteractiveInterpreter with custom I/O."""
    DEFAULT_LOCALS = {'__name__': "__console__", '__doc__': None}

    def __init__(self, user_locals):
        super().__init__(user_locals | self.DEFAULT_LOCALS)
        self.io_out = io.StringIO()
        self.io_in = io.StringIO()
        self.io_in.close()   # forbid using stdin, that freezes the interface

    def write(self, data):   # override
        """Override default write function, use internal buffer instead of stderr."""
        self.io_out.write(data)

    def get_output(self):
        """Get buffered output from internal buffer."""
        out = self.io_out.getvalue()
        self.io_out.close()
        return out

    @contextlib.contextmanager
    def io_context(self):
        """Special I/O context using internal buffer instead of stdout."""
        std_in, std_out = sys.stdin, sys.stdout
        underscore = builtins._   # backup gettext
        try:
            self.io_out = io.StringIO()
            sys.stdin, sys.stdout = self.io_in, self.io_out
            yield None   # return of __enter__, will block until __exit__
        finally:
            builtins._ = underscore
            sys.stdin, sys.stdout = std_in, std_out
