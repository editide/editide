#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for tweaking font settings."""

from gi.repository import Gtk


__all__ = ['ed_ext_init']


def ed_ext_init(app):
    """Initialize extension."""
    settings = Gtk.Settings.get_default()
    if font := app.get_setting('ext_font', 'font_name', str, ""):
        settings.set_property('gtk-font-name', font)
    if app.get_setting('ext_font', 'crisp_rendering', bool, False):
        # Only works with fontconfig, freetype v35, and hinted font like DejaVu
        if settings.find_property('gtk-font-rendering'):
            settings.set_property('gtk-font-rendering', "manual")
        settings.set_property('gtk-hint-font-metrics', True)
        settings.set_property('gtk-xft-antialias', 1)
        settings.set_property('gtk-xft-hinting', 1)
        settings.set_property('gtk-xft-hintstyle', "hintfull")
        settings.set_property('gtk-xft-rgba', "none")
