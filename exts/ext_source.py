#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extend source tabs features."""

from gi.repository import GLib, Gio, GtkSource

from ..core.utils.misc import ed_isinstance
from ..core.utils.thread import ed_run_subprocess_in_thread


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


ED_EXT_A2H_PARAMS = {'space': " ", 'tab': "\t"}
ED_EXT_COPY_PARAMS = ['name', 'path', 'raw', 'dir', 'uri']
ED_EXT_EOL_PARAMS = {'dos':  {'lf': ["unix2dos"], 'cr':    ["unix2dos", "mac2unix"]},
                     'mac':  {'lf': ["unix2mac"], 'cr-lf': ["unix2mac", "dos2unix"]},
                     'unix': {'cr': ["mac2unix"], 'cr-lf': ["dos2unix"]}}
ED_EXT_SORT_PARAMS = {'nocase': GtkSource.SortFlags.CASE_SENSITIVE,
                      'nodup': GtkSource.SortFlags.REMOVE_DUPLICATES,
                      'reverse': GtkSource.SortFlags.REVERSE_ORDER}
ED_EXT_TRIM_PARAMS = {'start': r"^[[:blank:]]+",
                      'end': r"[[:blank:]]+$",
                      'both': r"([[:blank:]]+$)?(^[[:blank:]]+)?"}


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    lm = GtkSource.LanguageManager.get_default()
    lang_ids = (_l for _l in lm.get_language_ids() if not lm.get_language(_l).get_hidden())
    charsets = (_e.get_charset() for _e in GtkSource.Encoding.get_all())
    return {
        'a2h':      (cmd_asc2hex,  _("Ascii to Hex"),        False, list(ED_EXT_A2H_PARAMS.keys())),
        'h2a':      (cmd_hex2asc,  _("Hex to Ascii"),        False, []),
        'admin':    (cmd_admin,    _("Edit File as Admin"),  False, []),
        'copy':     (cmd_copy,     _("Copy File Info"),      True,  ED_EXT_COPY_PARAMS),
        'eol':      (cmd_eol,      _("Convert Newlines"),    True,  list(ED_EXT_EOL_PARAMS.keys())),
        'filter':   (cmd_filter,   _("Filter Lines Regex"),  None,  []),
        'go':       (cmd_goto,     _("Go to Position"),      None,  []),
        'inc':      (cmd_inc,      _("Increment Numbers"),   None,  []),
        'join':     (cmd_join,     _("Join Selected Lines"), False, []),
        'lang':     (cmd_lang,     _("Set Highlight Lang"),  None,  sorted(lang_ids)),
        'lock':     (cmd_lock,     _("Read-Only State"),     True,  ["off", "on"]),
        'margin':   (cmd_margin,   _("Right Margin"),        True,  ["off", "on"]),
        'refactor': (cmd_refactor, _("Refactor Symbol"),     None,  []),
        'reload':   (cmd_reload,   _("Reload File"),         False, sorted(charsets)),
        'sort':     (cmd_sort,     _("Sort Selected Lines"), False, list(ED_EXT_SORT_PARAMS.keys())),
        't2s':      (cmd_tab2sp,   _("Tabs to Spaces"),      False, []),
        'trim':     (cmd_trim,     _("Trim Blank Chars"),    True,  list(ED_EXT_TRIM_PARAMS.keys())),
    }


def __ext_eol_run(ret, ex, ctx):
    """Callback for eol command result."""
    tools = ctx.get('tools', [])
    fpath = ctx.get('fpath', "")
    if not ex and tools and fpath:   # if no error, then process next command
        cmd = [tools.pop(), "-q", "-o", fpath]
        ed_run_subprocess_in_thread(cmd, __ext_eol_run, ctx)


# Convert selected text to hexadecimal.
# Syntax:
#     `a2h [separator]`
# The optional separator will be inserted between each pair of nibbles.
# It must be only one byte (like `,`). Special values `space` and `tab` can be used.
# The hexadecimal result depends on the document's encoding.
#
def cmd_asc2hex(wbench, doc, sep="", /):
    """Command for converting selected ASCII text to HEX."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.view.get_editable():
        if txt := doc.get_selected_text():
            enc = doc.sfile.get_encoding() or GtkSource.Encoding.get_utf8()
            b = txt.encode(encoding=enc.get_charset())
            sep = ED_EXT_A2H_PARAMS.get(sep, sep)
            new = b.hex(sep) if len(sep) == 1 else b.hex()
            doc.replace_selection(new.upper())
            return True
    return False


# Convert selected hexadecimal string to text.
# Syntax:
#     `h2a`
# Parameters are ignored.
# The conversion uses the document's encoding.
#
def cmd_hex2asc(wbench, doc, *param):
    """Command for converting selected HEX text to ASCII."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.view.get_editable():
        if txt := doc.get_selected_text():
            b = bytes.fromhex(txt)
            enc = doc.sfile.get_encoding() or GtkSource.Encoding.get_utf8()
            new = b.decode(encoding=enc.get_charset())
            doc.replace_selection(new)
            return True
    return False


# Request admin rights for editing current file.
# Syntax:
#     `admin`
# Parameters are ignored.
#
def cmd_admin(wbench, doc, *param):
    """Command for requesting admin rights on current file."""
    if ed_isinstance(doc, 'EdSrcEditor'):
        if gf := doc.get_gfile():
            uri = gf.get_uri()
            if uri.startswith("file://"):
                new_uri = "admin" + uri[4:]
                gf_admin = Gio.File.new_for_uri(new_uri)
                doc.sfile.set_location(gf_admin)
                # Request a load or save to reflect the new status in the GUI
                if doc.is_modified():
                    msg = _("Save now as admin?")
                    doc.notifier.question(msg, None, _("Save"), lambda _a: _a and doc.save())
                else:
                    msg, sec = _("Reopen now as admin?"), _("Current modifications will be lost!")
                    doc.notifier.question(msg, sec, _("Reload"), doc.on_reload_response)
                return True
    return False


# Copy current document metadata into clipboard.
# Syntax:
#     `copy <metadata>`
# Available metadata are listed in command completion.
#
def cmd_copy(wbench, doc, what="", /):
    """Command for copying file properties."""
    if doc is not None and what in ED_EXT_COPY_PARAMS:
        doc.copy_file_location(what)
        return True
    return False


# Convert document end-of-lines.
# Syntax:
#     `eol <dos|mac|unix>`
# Does nothing if current and target newlines are identical.
#
def cmd_eol(wbench, doc, target="", /):
    """Command for converting end-of-lines."""
    if ed_isinstance(doc, 'EdSrcEditor') and not doc.is_modified():
        nl = doc.sfile.get_newline_type().value_nick
        if tools := ED_EXT_EOL_PARAMS.get(target, {}).get(nl, []):
            gfile = doc.get_gfile()
            if not gfile:
                pass
            elif fpath := gfile.peek_path():
                ctx = {'fpath': fpath, 'tools': tools[:]}
                __ext_eol_run(None, None, ctx)
                return True
    return False


# Filter document lines by regex pattern.
# Syntax:
#     `filter  <positive_pattern>`
#     `filter !<negative_pattern>`
# Lines not matching the pattern will be removed.
#
def cmd_filter(wbench, doc, regex="", /):
    """Command for filtering lines by regex."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.view.get_editable():
        if invert := regex.startswith("!"):
            regex = regex[1:]
        GLib.idle_add(doc.filter_lines, regex, invert, priority=GLib.PRIORITY_LOW)
        return True
    return False


# Go to position in document.
# Syntax:
#     `go <position>`
# For a text document, the position is the line number.
#
def cmd_goto(wbench, doc, pos="", /):
    """Command for going to position."""
    if pos.isdecimal() and doc is not None:
        doc.go_to(int(pos))
        return True
    return False


# Increment selected numbers.
# Syntax:
#     `inc <by_value:format>`
# The by_value increment can be any valid integer value, like `-1` or `0x10`.
# Optionally a format can be provided for the result:
#   - `:x`    --> hexadecimal
#   - `:04o`  --> octal, 4 digits
#   - `:#06b` --> binary, 6 digits, with `0b` prefix
# By default `1` is used if no parameter provided,
# and format will be `:d` if omitted.
#
def cmd_inc(wbench, doc, param="1", /):
    """Command for incrementing selected numbers."""
    param += "" if ":" in param else ":d"
    inc, fmt = param.split(":", 1)
    inc, fmt = int(inc, 0), f"{{:{fmt}}}"
    if ed_isinstance(doc, 'EdSrcEditor'):
        editable = doc.view.get_editable()
        ibeg, iend = doc.buffer.get_selection_bounds() or (doc.get_cursor_iter(),) * 2
        line0, line1 = ibeg.get_line(), iend.get_line()
        off0, off1 = sorted([ibeg.get_line_offset(), iend.get_line_offset()])
        doc.buffer.begin_user_action()
        for line in reversed(range(line0, line1 + 1)):
            i0 = doc.buffer.get_iter_at_line_offset(line, off0).iter
            i1 = doc.buffer.get_iter_at_line_offset(line, off1).iter
            txt = doc.buffer.get_text(i0, i1, False)
            try:
                j = len(txt)
                n = int(txt.strip(), 0) + inc
                txt = fmt.format(n).rjust(j)
            except Exception:
                ed_log("error", f"could not increment at line '{line+1}'")
            else:
                doc.buffer.delete_interactive(i0, i1, editable)
                doc.buffer.insert_interactive(i0, txt, -1, editable)
        doc.buffer.place_cursor(doc.buffer.get_iter_at_line_offset(line0, off0).iter)
        doc.buffer.end_user_action()
        return True
    return False


# Join selected lines.
# Syntax:
#     `join`
# Parameters are ignored.
#
def cmd_join(wbench, doc, *param):
    """Command for joining lines."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.view.get_editable():
        if doc.get_has_selection_2():
            doc.buffer.join_lines(*doc.get_selection_bounds_2())
            return True
    return False


# Select highlight language.
# Syntax:
#     `lang <language_id>`
# Available language IDs are listed in command completion.
#
def cmd_lang(wbench, doc, lang="", /):
    """Command for setting the systax highlight language."""
    if ed_isinstance(doc, 'EdSrcEditor'):
        doc.set_lang(GtkSource.LanguageManager.get_default().get_language(lang))
        return True
    return False


# Set or clear the editor's read-only state.
# Syntax:
#     `lock <off|on>`
# The setting may be reset to its default if the file gets reloaded.
#
def cmd_lock(wbench, doc, lock="", /):
    """Command for changing the read-only flag."""
    if ed_isinstance(doc, 'EdSrcEditor'):
        if lock == "off" and not doc.view.get_editable():
            doc.view.set_editable(True)
        elif lock == "on" and doc.view.get_editable():
            doc.view.set_editable(False)
        else:
            return False
        doc.update_properties()
        return True
    return False


# Setup editor right margin.
# Syntax:
#     `margin <off|on|position>`
# Values `on`/`off` will show/hide the margin.
# The right margin position can be adjusted by entering an numeric value.
#
def cmd_margin(wbench, doc, margin="", /):
    """Command for configuring the right margin."""
    if margin == "off":
        wbench.set_config('margin_show', False)
    elif margin == "on":
        wbench.set_config('margin_show', True)
    elif margin.isdecimal():
        wbench.set_config('margin_pos', int(margin))
    else:
        return False
    for d in wbench.get_docs_sorted():
        if ed_isinstance(d, 'EdSrcEditor'):
            d.set_right_margin()
    return True


# Refactor symbol in editors.
# Syntax:
#     `refactor <new_name>`
# The selected symbol will be replaced by the new name in all open editors.
#
def cmd_refactor(wbench, doc, newname="", /):
    """Command for refactoring a symbol."""
    oldname = doc.get_selected_text() if doc else ""
    if oldname and newname:
        ss = GtkSource.SearchSettings(search_text=oldname, at_word_boundaries=True,
                                      case_sensitive=True, regex_enabled=False, wrap_around=True)
        for d in wbench.get_docs_sorted():
            if ed_isinstance(d, 'EdSrcEditor'):
                sc = GtkSource.SearchContext.new(d.buffer, ss)
                sc.replace_all(newname, -1)
        return True
    return False


# Reload the current document.
# Syntax:
#     `reload [charset]`
# The optional charset parameter will be used for encoding.
# Available charsets are listed in command completion.
# If not specified, the charset will be automatically detected.
#
def cmd_reload(wbench, doc, charset="", /):
    """Command for reloading a source file."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.get_gfile() is not None:
        if doc.is_modified():
            with doc.notifier.set_category('reload'):
                msg = _("Current modifications will be lost! Continue anyway?")
                doc.notifier.question(msg, None, _("Reload"), lambda _a: _a and doc.load(charset))
        else:
            doc.load(charset)
        return True
    return False


# Sort selected lines.
# Syntax:
#     `sort [options...]`
# Available sorting options:
#   - `nocase` : case-insensitive sorting (collate)
#   - `nodup`  : remove duplicated lines
#   - `reverse`: descending order
# Options can be combined, with space separator.
#
def cmd_sort(wbench, doc, opts="", /):
    """Command for sorting lines."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.view.get_editable():
        if doc.get_has_selection_2():
            flags = GtkSource.SortFlags.CASE_SENSITIVE
            for opt in set(opts.split()):
                if opt in ED_EXT_SORT_PARAMS:
                    if flags & ED_EXT_SORT_PARAMS[opt]:
                        flags ^= ED_EXT_SORT_PARAMS[opt]
                    else:
                        flags |= ED_EXT_SORT_PARAMS[opt]
                else:
                    return False
            doc.buffer.sort_lines(*doc.get_selection_bounds_2(), flags, 0)
            return True
    return False


# Replace tabs by spaces in whole document.
# Syntax:
#     `t2s`
# Parameters are ignored.
#
def cmd_tab2sp(wbench, doc, *param):
    """Command for replacing tabs by spaces."""
    if ed_isinstance(doc, 'EdSrcEditor') and doc.view.get_editable():
        ss = GtkSource.SearchSettings(search_text=r"\t+", at_word_boundaries=False,
                                      case_sensitive=True, regex_enabled=True, wrap_around=True)
        sc = GtkSource.SearchContext.new(doc.buffer, ss)
        doc.buffer.begin_user_action()
        itr = doc.buffer.get_start_iter()
        while True:
            res, ibeg, iend, wrap = sc.forward(itr)
            if not res:
                break
            off = ibeg.get_offset()
            diff = doc.view.get_visual_column(iend) - doc.view.get_visual_column(ibeg)
            sc.replace(ibeg, iend, " " * diff, -1)
            itr = doc.buffer.get_iter_at_offset(off + diff)
        doc.buffer.end_user_action()
        return True
    return False


# Trim document lines.
# Syntax:
#     `trim [position]`
# The optional position can be:
#   - `start`: remove leading blanks only
#   - `end`  : remove trailing blanks only
#   - `both` : remove leading and trailing blanks
# If unspecified, the command will remove trailing blanks only.
#
def cmd_trim(wbench, doc, where='end', /):
    """Command for trimming blank characters."""
    if ed_isinstance(doc, 'EdSrcEditor') and where in ED_EXT_TRIM_PARAMS:
        pattern = ED_EXT_TRIM_PARAMS[where]
        ss = GtkSource.SearchSettings(search_text=pattern, at_word_boundaries=False,
                                      case_sensitive=False, regex_enabled=True, wrap_around=True)
        sc = GtkSource.SearchContext.new(doc.buffer, ss)
        sc.replace_all("", -1)
        return True
    return False
