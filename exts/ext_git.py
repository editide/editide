#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for git operations."""

from ..core.utils.misc import ed_get_vars
from ..core.utils.thread import ed_run_subprocess_in_thread


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


ED_EXT_GIT_PARAMS = {
    'blame':    ["git", "-C", "{fileDirname}", "blame", "--", "{fileBasename}"],
    'diff':     ["git", "-C", "{fileDirname}", "diff", "--patch", "--no-color", "--submodule=diff",
                 "--", "{fileBasename}"],
    'diff-prj': ["git", "diff", "--patch", "--no-color", "--submodule=diff"],
    'log':      ["git", "-C", "{fileDirname}", "log", "--", "{fileBasename}"],
    'log-prj':  ["git", "log"],
    'show':     ["git", "-C", "{fileDirname}", "show"],
    'show-prj': ["git", "show"],
    'status':   ["git", "status"],
    'tree':     ["git", "-C", "{fileDirname}", "log", "--pretty=oneline", "--graph", "--decorate",
                 "--all", "--", "{fileBasename}"],
    'tree-prj': ["git", "log", "--pretty=oneline", "--graph", "--decorate", "--all"],
}


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    return {
        'git': (cmd_git, _("Git Requests"), True, list(ED_EXT_GIT_PARAMS.keys())),
    }


def __ext_git_result(ret, ex, ctx):
    """Callback for git command result."""
    wbench = ctx.get('wbench', None)
    subcmd = ctx.get('subcmd', "")
    if wbench is not None and not wbench.is_quitting():
        err = ex or ((ret.stderr.strip().splitlines() or [""])[0][:512] if ret.returncode else None)
        if err:
            wbench.notifier.error(_("Git {} error!").format(subcmd), f"{err}")
        else:
            hints = {'lang': 'diff' if subcmd in ('diff', 'diff-prj', 'show', 'show-prj') else None,
                     'goto': ctx.get('line', 1) if subcmd == 'blame' else 1}
            wbench.add_doc(None).set_text_and_lock(f"[git {subcmd}]", ret.stdout, **hints)


# Query versioning information from git.
# Syntax:
#     `git <subcommand> [params...]`
# Available subcommands:
#   - `blame`   : blame current file
#   - `diff`    : current file unstaged changes
#   - `diff-prj`: project unstaged changes
#   - `log`     : current file log (active branch)
#   - `log-prj` : project log (active branch)
#   - `show`    : show commit details (current file submodule scope)
#   - `show-prj`: show commit details (project scope)
#   - `status`  : project status
#   - `tree`    : current file log tree (all branches)
#   - `tree-prj`: project log tree (all branches)
# Additional parameters will be passed to git, can be used e.g. to diff against a specific commit.
#
def cmd_git(wbench, doc, param="", /):
    """Start git command."""
    user_params = param.split() or [""]
    subcmd = user_params.pop(0)
    if wbench.project is not None and subcmd in ED_EXT_GIT_PARAMS:
        v = ed_get_vars(wbench, doc)
        proc = [_p.format_map(v) for _p in ED_EXT_GIT_PARAMS[subcmd]]
        if "--" in proc:
            i = proc.index("--")
            proc = proc[:i] + user_params + proc[i:]
        else:
            proc += user_params
        opts = {'cwd': "{workspaceFolder}".format_map(v)}
        ctx = {'wbench': wbench, 'subcmd': subcmd, 'line': v.get('lineNumber', 0)}
        ed_run_subprocess_in_thread(proc, __ext_git_result, ctx, **opts)
        return True
    return False
