#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extend project features."""

from gi.repository import GLib, Gio

from ..core.utils.misc import ed_get_gfile_display_name


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    return {
        'file':   (cmd_file,   _("Find File in Project"),   False, []),
        'reopen': (cmd_reopen, _("Reopen Closed File"),     False, []),
        'reveal': (cmd_reveal, _("Reveal File in Sidebar"), False, []),
        'task':   (cmd_task,   _("Execute Task"),           None,  []),
    }


def __ext_project_is_uri(path):
    """Check if path is a valid URI."""
    ret = False
    try:
        ret = GLib.uri_is_valid(path, GLib.UriFlags.NONE)
    except GLib.Error:
        pass
    return ret


# Search and open files from project.
# Syntax:
#     `file <filename>`
# Available files are listed in command completion, updated on project's loading.
# If no filename provided, the current selected text will be used.
# If several files share the same name, all will get open.
# File names are case-sensitive.
#
def cmd_file(wbench, doc, fname="", /):
    """Command for opening a file from project's file list."""
    fname = fname or (doc.get_selected_text() if doc else "")
    if not fname:
        return False
    if any(_c in fname for _c in "\\/"):
        if fname.startswith("~/") or fname.startswith("~\\"):
            fname = GLib.get_home_dir() + fname[1:]
        if gf_doc := doc.get_gfile() if doc else None:
            if cwd := gf_doc.get_parent().peek_path():
                gf = Gio.File.new_for_commandline_arg_and_cwd(fname, cwd)
            else:
                gf = gf_doc.get_parent().resolve_relative_path(fname)
        else:
            gf = Gio.File.new_for_commandline_arg(fname)
        if not gf.is_native() or gf.query_exists():
            wbench.request_open([gf], "")
            return True
        raise ValueError(_("File not found: '{}'").format(gf.get_parse_name()))
    flist = [_gf for _gf in wbench.projectview.get_gfiles()
             if ed_get_gfile_display_name(_gf) == fname]
    if flist:
        wbench.request_open(flist, "")
        return True
    return False


# Re-open the last closed file.
# Syntax:
#     `reopen`
# Parameters are ignored.
#
def cmd_reopen(wbench, doc, *param):
    """Command for re-opening the last closed document."""
    if wbench.last_closed:
        uri = wbench.last_closed[-1]
        wbench.request_open([Gio.File.new_for_uri(uri)], "")
        return True
    return False


# Reveal current file in project's browser.
# Syntax:
#     `reveal`
# Parameters are ignored.
# Nothing happens if the current file is not listed by the project.
#
def cmd_reveal(wbench, doc, *param):
    """Command for revealing the current file in the project browser pane."""
    wbench.sidebar.show_page('project')
    wbench.on_project_reveal()
    return True


# Run a task, as defined in project's config.
# Syntax:
#     `task <name>`
# Available tasks are listed in command completion, updated on project's loading.
#
def cmd_task(wbench, doc, task="", /):
    """Command for executing a project task."""
    if task and wbench.project is not None:
        wbench.project.run_task(task)
        return True
    return False
