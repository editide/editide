#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extensions."""

import importlib
import os

from ..core.utils.misc import ed_get_userpath


__all__ = ['ed_extensions_init', 'ed_extensions_notify', 'ed_extensions_get_data']


ed_ext_modules = []


def ed_extensions_init(app):
    """Detect and import extensions."""
    # https://docs.python.org/3/library/importlib.html#importing-a-source-file-directly
    for extpath, sx in ((os.path.dirname(__file__), ''), (ed_get_userpath("exts4"), '_user')):
        if not os.path.isdir(extpath):
            continue
        with os.scandir(extpath) as idir:
            for f in idir:
                n = f.name.lower()
                if n.startswith("ext_") and n.endswith(".py") and f.is_file():
                    shortname = f.name[:-3]
                    if shortname.isidentifier():
                        try:
                            modname = f'{__spec__.name}{sx}.{shortname}'
                            modspec = importlib.util.spec_from_file_location(modname, f.path)
                            m = importlib.util.module_from_spec(modspec)
                            modspec.loader.exec_module(m)
                            m.ed_ext_init(app)
                            ed_ext_modules.append(m)
                            ed_log("ext", f"loaded extension '{modname}'")
                        except Exception as e:
                            raise Warning(f"Failure loading extension '{modname}'") from e
    ed_ext_modules.sort(key=lambda _m: _m.__name__)


def ed_extensions_get_providers(action):
    """Get extension providers for specified action."""
    for extm in ed_ext_modules:
        for sym in dir(extm):
            if sym == f'ed_ext_{action}':
                yield getattr(extm, sym)


def ed_extensions_notify(feature, *args):
    """Notify extension providers."""
    for provider in ed_extensions_get_providers(f'notif_{feature}'):
        provider(*args)


def ed_extensions_get_data(feature, *args):
    """Get data from extension providers."""
    for provider in ed_extensions_get_providers(f'get_{feature}'):
        yield provider(*args)
