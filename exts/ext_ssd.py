#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Extension for server-side decorations."""

import os
import weakref

from gi.repository import Gio


__all__ = ['ed_ext_init', 'ed_ext_notif_wbench_opened']


ED_EXT_SSD_STYLE = """
#ed-uid-mainwin headerbar {
    color: inherit;
    background: none;
    border-radius: 0;
    border: none;
    border-bottom: 1px solid @borders;
    box-shadow: none;
}
"""


def ed_ext_init(app):
    """Initialize extension."""
    if app.get_setting('ext_ssd', 'enable', bool, False):
        os.environ['GTK_CSD'] = "0"
        app.apply_css(1, txt=ED_EXT_SSD_STYLE)
        app.set_accels_for_action('win.fullscreen', ('F11',))


def ed_ext_notif_wbench_opened(wbench):
    """Notification on new workbench."""
    if wbench.app.get_setting('ext_ssd', 'enable', bool, False):
        # Keep subtitle only
        wbench.get_ui('ed_label_title').set_visible(False)
        # Unparent window contents
        hb = wbench.win.get_titlebar()
        hb.set_show_title_buttons(False)
        hb.remove_css_class('titlebar')
        wbench.win.set_titlebar(None)
        wbench.win.set_decorated(True)
        # Reparent window contents
        box = wbench.get_ui('ed_box_main')
        box.prepend(hb)
        # Fullscreen management
        action = Gio.SimpleAction.new('fullscreen', None)
        action.connect('activate', __ext_ssd_fullscreen, weakref.ref(wbench.win))
        wbench.win.add_action(action)


def __ext_ssd_fullscreen(action, param, win_ref):
    """Action handler for fullscreen."""
    win = win_ref()
    if win.is_fullscreen():
        win.unfullscreen()
    else:
        win.fullscreen()
