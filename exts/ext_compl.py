#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Code completion from document's local symbols."""

from gi.repository import GObject, GLib, Gio, Gtk, GtkSource

from ..core.utils.misc import ed_get_kind_color, ed_get_text_filter_change


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_completion():
    """Get completion providers."""
    return EdExtDocSymbolsProvider, ['ctags']


class EdExtDocSymbolsProvider(GObject.Object, GtkSource.CompletionProvider):
    """Code completion provider for document's local symbols."""
    __gtype_name__ = __qualname__

    def __init__(self):
        super().__init__()
        self.ctx_word = None
        f = Gtk.CustomFilter.new(self.compare)
        self.compl_items = Gio.ListStore.new(EdExtCompletionItem)
        self.filter_items = Gtk.FilterListModel.new(self.compl_items, f)
        self.filter_items.set_incremental(True)   # async filtering in idle
        self.symbolsmanager = None

    def do_get_title(self):   # override
        """Override title getter."""
        return 'DocumentSymbols'

    def do_activate(self, ctx, complitem):   # override
        """Override completion proposal activation."""
        ok, ibeg, iend = ctx.get_bounds()
        if ok:
            editable = ctx.get_view().get_editable()
            buf = ctx.get_buffer()
            buf.begin_user_action()
            buf.delete_interactive(ibeg, iend, editable)
            buf.insert_interactive(ibeg, complitem.name, -1, editable)
            buf.end_user_action()

    def do_display(self, ctx, complitem, cell):   # override
        """Override completion proposal display."""
        match cell.get_column():
            case GtkSource.CompletionColumn.ICON:
                cell.set_markup(complitem.icon)
                cell.get_widget().set_attributes(None)
            case GtkSource.CompletionColumn.BEFORE:
                cell.set_text(complitem.before)
                cell.get_widget().set_attributes(None)
            case GtkSource.CompletionColumn.TYPED_TEXT:
                cell.set_text(complitem.name)
                cell.get_widget().set_attributes(None)
            case GtkSource.CompletionColumn.AFTER:
                cell.set_text(complitem.after)
                cell.get_widget().set_attributes(None)
            case GtkSource.CompletionColumn.COMMENT:
                cell.set_text(complitem.comment)
            case GtkSource.CompletionColumn.DETAILS:
                pass

    def do_populate_async(self, ctx, cancellable, cbk, user_data):   # override
        """Override completion proposals populate request, on new user input."""
        self.update_filter(ctx)
        ctx.set_proposals_for_provider(self, self.filter_items)
        ##cbk_data = () if user_data is None else (user_data,)
        ##res = Gio.Task.new(self, cancellable, cbk, *cbk_data)
        ### Directly set the GTask result; no need to run in thread, we already have filter_items
        ##res.return_boolean(True)

    def do_populate_finish(self, res):   # override
        """Override completion proposals populate finish."""
        if Gio.Task.is_valid(res, self) and res.propagate_boolean():
            return self.filter_items
        return None

    def do_refilter(self, ctx, model):   # override
        """Override completion proposals refilter, on refined user input."""
        self.update_filter(ctx)

    def register(self, symbolsmanager):
        """Register a symbol provider to the completion provider."""
        self.symbolsmanager = symbolsmanager
        self.symbolsmanager.register_notif(self.on_symbols_update)

    def unregister(self, symbolsmanager):
        """Unregister a symbol provider from the completion provider."""
        self.symbolsmanager = None

    def update_filter(self, ctx):
        """Update filter from completion context, on user input."""
        word = ctx.get_word()
        if len(word) < 3 and not ctx.get_activation() & GtkSource.CompletionActivation.USER_REQUESTED:
            word = None
        if self.ctx_word != word:
            change = ed_get_text_filter_change(self.ctx_word, word)
            self.ctx_word = word
            self.filter_items.get_filter().changed(change)

    def compare(self, complitem):
        """Filter function for selecting completion items to show."""
        return False if self.ctx_word is None else self.ctx_word in complitem.name

    def rebuild_compl_items(self):
        """Rebuild completion items from symbols."""
        self.compl_items.remove_all()
        for sym in self.symbolsmanager.loop_symbols():
            self.compl_items.append(EdExtCompletionItem(sym.props))

    def on_symbols_update(self):
        """Callback on symbols parsing finished."""
        if self.symbolsmanager is not None:
            GLib.idle_add(self.rebuild_compl_items)


class EdExtCompletionItem(GObject.Object, GtkSource.CompletionProposal):
    """Code completion item for provider."""
    __gtype_name__ = __qualname__

    def __init__(self, sym):
        super().__init__()
        kl = sym['kl']
        color = ed_get_kind_color(kl)
        scope = sym.get('scope', "").split(":", 1)[-1]
        self.name = sym['name']
        self.icon = f"""<span color="{color}" weight="bold" style="italic">{kl}</span>"""
        self.before = sym.get('typeref', "").split(":", 1)[-1]
        self.after = sym.get('signature', "")
        self.comment = scope or None
