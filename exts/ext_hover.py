#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Hover indications from document's local symbols."""

from gi.repository import GObject, GLib, Gdk, Gtk, GtkSource, Graphene

from ..core.utils.misc import ed_get_kind_color


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_completion():
    """Get completion providers."""
    return EdExtDocHoverProvider, ['ctags']


class EdExtDocHoverProvider(GObject.Object, GtkSource.HoverProvider):
    """Hover provider for document's local symbols."""
    __gtype_name__ = __qualname__

    def __init__(self):
        super().__init__()
        self.symbolsmanager = None

    def do_populate(self, ctx, disp):   # override
        """Fill hover list for selected symbol."""
        popup = disp.get_ancestor(Gtk.Popover.__gtype__)
        popup.set_overflow(Gtk.Overflow.HIDDEN)
        popup.set_can_focus(False)
        buffer = ctx.get_buffer()
        if buffer.get_has_selection():
            ibeg, iend = buffer.get_selection_bounds()
            ok, ibeg_ctx, iend_ctx = ctx.get_bounds()
            if ok and ibeg_ctx.compare(iend) < 0 < iend_ctx.compare(ibeg):
                word = buffer.get_text(ibeg, iend, False)
                line = ibeg.get_line() + 1
                for sym in self.symbolsmanager.find_symbol(word):
                    if sym['line'] == line:
                        continue
                    if w := EdExtHoverSymbolItem.new_from_symbol(sym.props):
                        disp.append(w)
                color = Gdk.RGBA()
                if color.parse(word):
                    disp.append(EdExtHoverColorItem(color))
        return True

    def register(self, symbolsmanager):
        """Register a symbol provider to the hover provider."""
        self.symbolsmanager = symbolsmanager

    def unregister(self, symbolsmanager):
        """Unregister a symbol provider from the hover provider."""
        self.symbolsmanager = None


class EdExtHoverColorItem(Gtk.Widget):
    """Hover item widget for colors."""
    __gtype_name__ = __qualname__

    def __init__(self, color):
        super().__init__(width_request=72, height_request=24, overflow='hidden')
        self.color = color
        self.color.alpha = 1.0

    def do_snapshot(self, snap):   # override
        """Drawing handler."""
        w, h = self.get_width(), self.get_height()
        rect = Graphene.Rect().init(0.0, 0.0, w, h)
        snap.append_color(self.color, rect)


class EdExtHoverSymbolItem(Gtk.Box):
    """Hover item widget for symbols."""
    __gtype_name__ = __qualname__

    def __init__(self, kl, info):
        super().__init__(orientation='horizontal')
        self.add_css_class('ed-class-hoveritem')
        color = ed_get_kind_color(kl)
        markup = f"""<span color="{color}" weight="bold" style="italic">{kl}</span>"""
        self.append(Gtk.Label(label=markup, use_markup=True, single_line_mode=True))
        self.append(Gtk.Label(label=info, single_line_mode=True, hexpand=True, xalign=0, ellipsize='end'))

    @classmethod
    def new_from_symbol(cls, sym):
        """Create new hover item widget from symbol."""
        if sig := sym.get('signature', ""):
            sig = f"{sym['name']} {sig}"
        tref = sym.get('typeref', "").split(":", 1)[-1]
        if info := " : ".join(filter(None, (sig, tref))) or sym.get('source', ""):
            sym_item = cls(sym['kl'], info)
            if line := sym['line']:
                sym_item.set_target(line)
            return sym_item
        return None

    def set_target(self, line):
        """Setup click target."""
        ev_click = Gtk.GestureClick(name='ed-pclick', button=Gdk.BUTTON_PRIMARY)
        ev_click.connect('released', self.on_event_click, line)
        self.add_controller(ev_click)

    def on_event_click(self, event, n, x, y, line):
        """Click event handler."""
        self.activate_action('ext.go', GLib.Variant('s', f"{line}"))
