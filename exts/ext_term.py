#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Terminal extension."""

import importlib
import shlex

import gi
from gi.repository import GLib, Gtk


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    gir = gi.Repository.get_default()
    if versions := [_v for _v in sorted(gir.enumerate_versions('Vte')) if _v.startswith('3.')]:
        gi.require_version('Vte', versions[-1])
        return {'term': (cmd_term, _("Add Terminal"), False, [])}
    return {}   # Vte not available


# Open an embedded terminal in the bottom pane.
# Syntax:
#     `term [title]`
# The title is optional.
#
def cmd_term(wbench, doc, title="", /):
    """Open a terminal."""
    extterm = EdExtTerm(wbench)
    term_id = extterm.get_id()
    stack = wbench.get_ui('ed_stack_bottom')
    stack.add_titled(extterm, f'ext-term-{term_id}', _("Term: {0}").format(title or term_id))
    return True


class EdExtTerm(Gtk.ScrolledWindow):
    """Embedded terminal."""
    __gtype_name__ = __qualname__
    CNT = 0

    def __init__(self, wbench):
        super().__init__(has_frame=False)
        Vte = importlib.import_module('gi.repository.Vte', __spec__.parent)
        self.instance_id = self.__class__.CNT = self.__class__.CNT + 1
        workdir = wbench.get_path_context()
        default_shell = GLib.getenv('SHELL') or Vte.get_user_shell()
        shell = wbench.app.get_setting('ext_term', 'shell', str, default_shell)
        self.term = Vte.Terminal()
        self.term.spawn_async(Vte.PtyFlags.DEFAULT, workdir, shlex.split(shell),
                              None, GLib.SpawnFlags.DEFAULT, None, None, -1, None, None)
        self.term.set_audible_bell(False)
        self.term.set_bold_is_bright(True)
        self.term.set_scrollback_lines(10_000)
        self.term.connect('map', self.on_map)
        self.term.connect('child-exited', self.on_exit)
        self.connect('notify::parent', self.on_reparent)
        self.set_child(self.term)

    def get_id(self):
        """Get terminal's instance ID."""
        return self.instance_id

    def on_map(self, term):
        """Callback on terminal mapped."""
        GLib.idle_add(lambda: term.grab_focus() and False)

    def on_reparent(self, w, pspec):
        """Callback on parent change."""
        if self.get_parent() is None:   # widget detached, going to be destroyed
            self.term.disconnect_by_func(self.on_map)
            self.term.disconnect_by_func(self.on_exit)
            self.disconnect_by_func(self.on_reparent)
            self.term = None

    def on_exit(self, term, status):
        """Callback on terminal exited."""
        self.get_parent().remove(self)
