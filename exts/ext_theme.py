#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Theming extension."""

import importlib
import os

import gi
from gi.repository import GLib, Gio, Gtk, GtkSource

from ..core.utils.misc import ed_get_kind_color, ed_get_source_theme_scheme


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


ED_EXT_THEME_PARAMS = {
    'default': (None,  'DEFAULT'),
    'dark':    (True,  'FORCE_DARK'),
    'light':   (False, 'FORCE_LIGHT'),
}

ED_EXT_THEME_RECOLOR = [
    ('dialog_bg_color',            "mix(@window_bg_color, white, .1)"),
    ('dialog_fg_color',            "@window_fg_color"),
    ('popover_bg_color',           "mix(@window_bg_color, white, .12)"),
    ('popover_fg_color',           "@window_fg_color"),
    ('view_bg_color',              "@ed_notebook_bg_color"),
    ('view_fg_color',              "@ed_notebook_fg_color"),
    ('card_bg_color',              "mix(@window_bg_color, white, .12)"),
    ('card_fg_color',              "@window_fg_color"),
    ('headerbar_bg_color',         "mix(@window_bg_color, @window_fg_color, .06)"),
    ('headerbar_fg_color',         "@window_fg_color"),
    ('sidebar_bg_color',           "mix(@window_bg_color, @window_fg_color, .06)"),
    ('sidebar_fg_color',           "@window_fg_color"),
    ('secondary_sidebar_bg_color', "mix(@window_bg_color, @window_fg_color, .03)"),
    ('secondary_sidebar_fg_color', "@window_fg_color"),
]


def ed_ext_init(app):
    """Initialize extension."""
    default_adw = "GNOME" in f"{GLib.getenv('XDG_CURRENT_DESKTOP')}"
    if app.get_setting('ext_theme', 'adwaita', bool, default_adw):
        try:
            global Adw
            gi.require_version('Adw', '1')
            Adw = importlib.import_module('gi.repository.Adw', __spec__.parent)
            Adw.init()
        except Exception:
            ed_log("error", "could not initialize 'gi.repository.Adw'")
        else:
            app.apply_css(1, path=os.path.join(os.path.dirname(__file__), "ext_theme_adw.css"))
    if variant := app.get_setting('ext_theme', 'variant', str, None):
        cmd_theme(None, None, variant)
    Gtk.Settings.get_default().connect('notify::gtk-application-prefer-dark-theme', __ext_theme_changed)
    app.ext_data['theme'] = {'cssp': app.apply_css(2)}
    __ext_theme_reload_style(app)


def ed_ext_get_commands(app):
    """Get extension commands."""
    ssm = GtkSource.StyleSchemeManager.get_default()
    return {
        'style': (cmd_style, _("Syntax Highlight Style"), True, list(ssm.get_scheme_ids())),
        'theme': (cmd_theme, _("Theme Variant"), True, list(ED_EXT_THEME_PARAMS.keys())),
    }


def __ext_theme_changed(gobj, pspec):
    """Callback on theme changed."""
    GLib.idle_add(__ext_theme_update, priority=GLib.PRIORITY_DEFAULT)


def __ext_theme_update():
    """Update documents theme."""
    app = Gio.Application.get_default()
    for wbench in app.instances.values():
        for doc in wbench.docs.values():
            doc.update_theme()
    __ext_theme_reload_style(app)


def __ext_theme_reload_style(app):
    """Reload dynamic stylesheet."""
    css = ""
    if scheme := ed_get_source_theme_scheme(app):
        if style := scheme.get_style('text'):
            fg, bg = None, None
            if style.props.background_set:
                bg = style.props.background
                css += f"@define-color ed_notebook_bg_color {bg};\n"
            if style.props.foreground_set:
                fg = style.props.foreground
                css += f"@define-color ed_notebook_fg_color {fg};\n"
            if app.get_setting('ext_theme', 'recolor', bool, True):
                if 'Adw' in globals():
                    css += __ext_theme_recolor(scheme, fg, bg)
    app.ext_data['theme']['cssp'].load_from_string(css)


def __ext_theme_recolor(scheme, fg, bg):
    """Generate global colors from style scheme."""
    css = ""
    if scheme.get_id().startswith('Adwaita'):
        return css
    if fg and bg:
        css += f"@define-color window_bg_color mix({bg}, {fg}, .035);\n"
    if fg:
        if v := scheme.get_metadata('variant'):
            base = "white" if v == 'dark' else "black"
            css += f"@define-color window_fg_color mix({fg}, {base}, .75);\n"
        else:
            css += f"@define-color window_fg_color {fg};\n"
    for color, fallback in ED_EXT_THEME_RECOLOR:
        c = scheme.get_metadata(color) or fallback
        css += f"@define-color {color} {c};\n"
    return css


# Change the syntax highlight style.
# Syntax:
#     `style <scheme_id>`
# Available schemes are listed in command completion.
#
def cmd_style(wbench, doc, scheme_id="", /):
    """Command for changing the syntax highlight style."""
    ssm = GtkSource.StyleSchemeManager.get_default()
    if scheme_id in ssm.get_scheme_ids():
        is_dark = Gtk.Settings.get_default().get_property('gtk-application-prefer-dark-theme')
        theme_variant = 'dark' if is_dark else 'light'
        scheme_variant = ssm.get_scheme(scheme_id).get_metadata('variant')
        if scheme_variant not in ('dark', 'light'):
            scheme_variant = theme_variant
            ed_log("warn", f"variant missing for style scheme '{scheme_id}'")
        wbench.app.settings.set('core', f'highlight_theme_{scheme_variant}', scheme_id)
        if theme_variant == scheme_variant:
            __ext_theme_update()
        else:
            cmd_theme(wbench, doc, scheme_variant)
        return True
    return False


# Change the UI theme variant.
# Syntax:
#     `theme <variant>`
# Available variants are listed in command completion.
#
def cmd_theme(wbench, doc, variant="", /):
    """Command for changing the theme style."""
    if variant in ED_EXT_THEME_PARAMS:
        dark, cs = ED_EXT_THEME_PARAMS[variant]
        ed_get_kind_color.cache_clear()
        if 'Adw' in globals():
            Adw.StyleManager.get_default().set_color_scheme(getattr(Adw.ColorScheme, cs))
        elif dark is None:
            Gtk.Settings.get_default().reset_property('gtk-application-prefer-dark-theme')
        else:
            Gtk.Settings.get_default().set_property('gtk-application-prefer-dark-theme', dark)
        return True
    return False
