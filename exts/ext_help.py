#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Help extension."""

import importlib
import os
import sys

from gi.repository import GLib, Gtk, GtkSource


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


ED_EXT_HELP_FILES = {}


def ed_ext_init(app):
    """Initialize extension."""
    with os.scandir(ED_PATH) as idir:
        for f in idir:
            n = f.name.lower()
            if n.endswith(".md") and f.is_file():
                ED_EXT_HELP_FILES[n[:-3]] = f.path


def ed_ext_get_commands(app):
    """Get extension commands."""
    help_params = ['about', 'commands', 'shortcuts'] + list(ED_EXT_HELP_FILES.keys())
    return {
        'help': (cmd_help, _("Help Topics"), True, sorted(help_params)),
    }


# Get help on specified topic.
# Syntax:
#     `help <topic>`
# Available topics are listed in command completion.
#
def cmd_help(wbench, doc, topic="", /):
    """Command for querying help."""
    title = f"[help {topic}]"
    if topic == 'shortcuts':
        wbench.win.activate_action('win.show-help-overlay', None)
    elif topic == 'about':
        sysinfo = GLib.get_os_info('PRETTY_NAME')
        sysinfo += f"\nPython {sys.version}"
        for m in (GLib, Gtk, GtkSource):
            if hasattr(m, 'MAJOR_VERSION'):
                sysinfo += f"\n{m._namespace} {m.MAJOR_VERSION}.{m.MINOR_VERSION}.{m.MICRO_VERSION}"
        ok, copying = GLib.file_get_contents(os.path.join(ED_PATH, "COPYING"))
        a = Gtk.AboutDialog(transient_for=wbench.win, modal=True,
                            version=ED_VERSION,
                            logo_icon_name=ED_ICON,
                            comments=_("Browse and edit source code"),
                            website=ED_WEBSITE,
                            license_type='custom',
                            license=copying.decode('utf-8') if ok else None)
        a.set_system_information(sysinfo)
        a.show()
    elif topic == 'commands':
        inspect = importlib.import_module('inspect', __spec__.parent)
        txt = "# Commands Reference\n\n"
        for cmd, data in sorted(wbench.cmdlauncher.commands.items()):
            cbk, descr, menu, params = data
            cbk_doc = inspect.getcomments(cbk) or inspect.getdoc(cbk)
            txt += f"\n## {cmd}\n\n"
            txt += "\n".join(_l.removeprefix("# ") for _l in cbk_doc.splitlines())
            txt += "\n\n"
        wbench.add_doc(None).set_text_and_lock(title, txt, lang='markdown')
    elif topic in ED_EXT_HELP_FILES:
        ok, data = GLib.file_get_contents(ED_EXT_HELP_FILES[topic])
        if ok:
            wbench.add_doc(None).set_text_and_lock(title, data.decode('utf-8'), lang='markdown')
        return ok
    else:
        return False
    return True
