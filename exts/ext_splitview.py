#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Manage notebooks layout in split view."""


__all__ = ['ed_ext_init', 'ed_ext_get_commands']


def ed_ext_init(app):
    """Initialize extension."""
    pass


def ed_ext_get_commands(app):
    """Get extension commands."""
    return {
        'split': (cmd_split, _("Split Documents View"), True, ['1', 'h', 'v', '4']),
    }


def __ext_splitview_get_position(nb):
    """Get column and row of notebook."""
    w = nb.get_widget()
    return w.get_parent().query_child(w)[:2]


def __ext_splitview_get_at(wbench, c, r):
    """Get notebook at position."""
    if w := wbench.get_ui('ed_grid_splitview').get_child_at(c, r):
        return [_n for _n in wbench.notebooks if w is _n.get_widget()].pop()
    return None


def __ext_splitview_merge(nb_src, nb_dst):
    """Merge a notebook to another one."""
    for d in nb_src.wbench.get_docs_sorted():
        if d.notebook is nb_src:
            nb_dst.wbench.move_page(nb_src, nb_dst, d.get_widget())


def __ext_splitview_move(nb, c, r):
    """Move a notebook to position."""
    w = nb.get_widget()
    w.get_parent().get_layout_manager().get_layout_child(w).set_properties(column=c, row=r)


def __ext_splitview_remove(wbench, nb):
    """Remove notebook from grid view."""
    nb.get_widget().get_parent().remove(nb.get_widget())
    wbench.notebooks.remove(nb)


# Split documents view into multiple notebooks.
# Syntax:
#     `split <layout>`
# Available layouts are listed in command completion.
# A horizontal side-by-side layout will be use by default if no layout provided.
#
def cmd_split(wbench, doc, layout="h", /):
    """Command for splitting the documents view into multiple notebooks."""
    match layout:
        case '1':
            nb_main = __ext_splitview_get_at(wbench, 0, 0)
            for nb in wbench.notebooks[:]:
                if nb is not nb_main:
                    __ext_splitview_merge(nb, nb_main)
                    __ext_splitview_remove(wbench, nb)
        case 'h':
            if __ext_splitview_get_at(wbench, 1, 1) is not None:
                __ext_splitview_merge(__ext_splitview_get_at(wbench, 0, 1), __ext_splitview_get_at(wbench, 0, 0))
                __ext_splitview_merge(__ext_splitview_get_at(wbench, 1, 1), __ext_splitview_get_at(wbench, 1, 0))
                __ext_splitview_remove(wbench, __ext_splitview_get_at(wbench, 0, 1))
                __ext_splitview_remove(wbench, __ext_splitview_get_at(wbench, 1, 1))
            elif nb := __ext_splitview_get_at(wbench, 0, 1):
                __ext_splitview_move(nb, 1, 0)
            elif __ext_splitview_get_at(wbench, 1, 0) is None:
                wbench.add_notebook(1, 0, 1, 1)
        case 'v':
            if __ext_splitview_get_at(wbench, 1, 1) is not None:
                __ext_splitview_merge(__ext_splitview_get_at(wbench, 1, 0), __ext_splitview_get_at(wbench, 0, 0))
                __ext_splitview_merge(__ext_splitview_get_at(wbench, 1, 1), __ext_splitview_get_at(wbench, 0, 1))
                __ext_splitview_remove(wbench, __ext_splitview_get_at(wbench, 1, 0))
                __ext_splitview_remove(wbench, __ext_splitview_get_at(wbench, 1, 1))
            elif nb := __ext_splitview_get_at(wbench, 1, 0):
                __ext_splitview_move(nb, 0, 1)
            elif __ext_splitview_get_at(wbench, 0, 1) is None:
                wbench.add_notebook(0, 1, 1, 1)
        case '4':
            for r in range(2):
                for c in range(2):
                    if __ext_splitview_get_at(wbench, c, r) is None:
                        wbench.add_notebook(c, r, 1, 1)
        case _:
            return False
    if wbench.active_notebook not in wbench.notebooks:
        wbench.select_notebook(wbench.notebooks[0])
    wbench.get_ui('ed_button_splitsinglemode').set_visible(len(wbench.notebooks) > 1)
    return True
