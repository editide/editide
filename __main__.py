#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Entry points."""

import sys

from gi.repository import GLib

from .core.application import EdApp


def ed_run():
    """Instantiate and run the application."""
    GLib.set_prgname(ED_APPNAME)
    GLib.set_application_name(_("EdiTidE"))
    return EdApp('localhost.EdiTidE4').run(sys.argv)


if __name__ == '__main__':
    ##sys.addaudithook(lambda *_args, **_kwargs: print(_args, _kwargs, file=sys.stderr))
    sys.exit(ed_run())
