EdiTidE - Readme
================

EdiTidE is a source-code editor, with basic project support.


Main features
-------------

* Syntax highlighting
* Dark/light themes
* Project browser
* Search in file or project (regex support)
* Symbols of active source file (requires `universal-ctags`)
* No data cached on disk
* Command bar (extendable)
* Block/column edition mode


Install
-------

See [INSTALL](INSTALL.md) for instructions.


Restrictions
------------

The operating system and source files should use UTF-8 as encoding.

This application is intended to remain relatively simple.
If you need advanced features like code completion, debugging, git
integration or embedded terminal, then better use a real IDE like
`Atom`, `Code::Blocks`, `CodeLite`, `Geany` or `Gnome-Builder`.


Source editor
-------------

The editor automatically detects file type and encoding on loading.
If not correct, the command `reload` can be used to specify a charset.

The command bar provides actions like sorting lines, converting to hex,
or trimming blank characters. Run `help commands` for details.

Syntax highlighting language can be changed with the `lang` command.

Configure tab/spaces indentation from the menu button in status bar.
By default, EditorConfig settings will be used, if present.

There is a block mode available for column editing. Delimit the block
corners using the start and end cursors of the selection, then press
<kbd>Ctrl</kbd>+<kbd>B</kbd> or <kbd>Shift</kbd>+<kbd>Ctrl</kbd>+<kbd>B</kbd>
to respectively squash the boxed text or fill blanks with spaces.
When editing in block mode, the master cursor is the first line's one,
others will vertically follow.


Project browser
---------------

The project pane in the sidebar allows to browse the files tree.

Excluded paths can be specified in a JSON project configuration,
and optionally follow the gitignore specification.


Search
------

The search pane allows searching in the current file or project.

From the "Find" entry, press <kbd>Enter</kbd> once to highlight occurrences
in the current file, or twice to start searching in the whole project.

Limitations:
When searching in whole project, only single-line matches are
supported, and replacing is not possible.
Project search reads from disk, unsaved modifications will be ignored.


Symbols
-------

The symbols pane shows the objects definition in the current source
file, as reported by `ctags`.

Select a symbol in source and press <kbd>F12</kbd> to jump to its definition.


Configuration
-------------

Most settings are hardcoded in the app's source code, directly edit it
to adapt to your needs.

A few global settings are user-configurable, loaded from
`${XDG_CONFIG_HOME}/editide/settings.toml`.

Project-specific configuration can be stored in a file `.editide.json`
placed at the root folder of the project.

Users can define their default project settings under
`${XDG_CONFIG_HOME}/editide/defaults.json`.

For file-specific settings, use [EditorConfig](https://editorconfig.org/).
Supported properties: `indent_style`, `indent_size`, `tab_width`,
`insert_final_newline`, `trim_trailing_whitespace`.
The right margin position will follow `max_line_length`, but won't auto-wrap.
When saving a new file, `charset` and `end_of_line` are used if specified.


Privacy
-------

No data collected, no telemetry, no connections to remote servers.

This application doesn't save anything on disk without user action (i.e.
save buttons or shortcuts).

When using remote language servers (if enabled in the project's config),
the content of the files may be sent over the network, please refer to
the LSP tools and their configuration.


Disclaimer
----------

THIS SOFTWARE IS PROVIDED "AS IS", WITH ABSOLUTELY NO WARRANTY, REAL
OR IMPLIED.
