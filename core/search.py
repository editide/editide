#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Multi-files search manager."""

import re
import subprocess

from gi.repository import GLib, Gio, Pango, Gdk, Gtk

from .ui.contextmenu import EdTreeMenu
from .utils.misc import ed_set_clipboard_text
from .utils.pathmatch import EdMatchPattern, EdPathMatch
from .utils.thread import EdThread


__all__ = ['EdMultiSearch']


ED_CMDLINE_MAX = 16_000   # max. 128k on Linux, 32k on Win32


class EdMultiSearch():
    """Search in files."""

    def __init__(self, wbench, tv, progress):
        self.wbench = wbench
        self.treeview = tv
        self.progress = progress
        self.thread = None
        self.current_file = (None, None, None)   # path, TreeIter, GFile
        self.count_matches = 0
        self.count_files = 0
        if self.treeview.get_model() is None:
            # [file/match, weight, line, line-visible, tooltip, GFile]
            treestore = Gtk.TreeStore(str, int, str, bool, str, Gio.File)
            self.treeview.set_model(treestore)
            self.setup_treeview()
        # Popup menu
        self.wbench.menumanager.add_action('search.hide', None, self.on_menu_hide)
        self.wbench.menumanager.add_action('search.copy', None, self.on_menu_copy)
        self.wbench.menumanager.add_action('search.openall', None, self.on_menu_openall)
        self.wbench.menumanager.add_action('search.openwin', None, self.on_menu_openwin)
        menu = EdTreeMenu(self.wbench.menumanager, self.treeview)
        menu.add_item(_("_Hide"), 'search.hide')
        menu.add_item(_("_Copy Result"), 'search.copy')
        menu.add_item(_("Open _All Files"), 'search.openall')
        menu.add_item(_("Open in New _Window"), 'search.openwin')
        # Events
        ev_click = Gtk.GestureClick(name='ed-mclick', button=Gdk.BUTTON_MIDDLE)
        ev_click.connect('released', self.on_event_click)
        self.treeview.add_controller(ev_click)
        self.treeview.connect('row-activated', self.on_row_activated)

    def free(self):
        """Release resources and break circular references."""
        self.treeview.disconnect_by_func(self.on_row_activated)
        self.reset()
        self.wbench = None

    def setup_treeview(self):
        """Setup search results treeview."""
        c = Gtk.TreeViewColumn()
        c.set_expand(True)
        c.set_spacing(6)
        # Column cell: line number
        cr = Gtk.CellRendererText()
        cr.set_alignment(1.0, 0.5)
        cr.set_sensitive(False)
        c.pack_start(cr, False)
        c.add_attribute(cr, 'text', 2)
        c.add_attribute(cr, 'visible', 3)
        # Column cell: file name or match
        cr = Gtk.CellRendererText(ellipsize='end')
        c.pack_end(cr, True)
        c.add_attribute(cr, 'text', 0)
        c.add_attribute(cr, 'weight', 1)
        # Add column
        self.treeview.append_column(c)
        self.treeview.set_tooltip_column(4)
        self.treeview.set_level_indentation(-16)

    def reset(self):
        """Reset search results view."""
        self.treeview.get_column(0).set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        self.treeview.get_model().clear()
        self.treeview.get_column(0).set_sizing(Gtk.TreeViewColumnSizing.GROW_ONLY)
        self.current_file = (None, None, None)
        self.count_matches = 0
        self.count_files = 0

    def ping_search_progress(self):
        """Pulse the progress indication."""
        if self.thread is None:
            return GLib.SOURCE_REMOVE
        self.progress.progress_pulse()
        return GLib.SOURCE_CONTINUE

    def find(self, gfiles, gfroot, **settings):
        """Search text in project."""
        if self.thread is None and settings['find']:
            self.reset()
            self.wbench.stopthread.clear()
            gf_dups = tuple(_gf.dup() for _gf in gfiles)   # GFile not thread-safe, use duplicates
            gfr_dup = None if gfroot is None else gfroot.dup()
            self.thread = EdThread(target=self.find_async, args=(gf_dups, gfr_dup), kwargs=settings)
            self.thread.start()
            self.wbench.notifier.status(_("Searching in files..."), False)
            self.progress.set_progress_fraction(0.1)
            self.progress.progress_pulse()   # start bouncing
            GLib.timeout_add(200, self.ping_search_progress)
        else:
            self.wbench.notify_search_failure()

    def find_async(self, gfiles, gfroot, /, *, find="", wb=False, cs=False, es=False, rx=False, fltr=None, **kwargs):
        """Async search operation, to be run in own thread."""
        args = ["grep", "--color=never", "--no-messages", "--with-filename", "--line-number"]
        args += ["--binary-files=without-match", "--devices=skip"]
        args += ["--no-ignore-case"] if cs else ["--ignore-case"]
        args += ["--word-regexp"] if wb else []
        args += ["--perl-regexp"] if rx else ["--basic-regexp"] if es else ["--fixed-strings"]
        args += [f"--regexp={find}", "--"]
        maxlen = ED_CMDLINE_MAX - sum(len(_a) + 1 for _a in args)
        try:
            GLib.usleep(100_000)   # let GLib process remaining events
            pattern = re.compile(r":(\d+):")
            for fgroup in self.group_files(self.filter_files(gfiles, gfroot, fltr), maxlen):
                ret = subprocess.run(args + fgroup, text=True, capture_output=True, check=False,
                                                    encoding='utf-8', errors='replace')
                matches = tuple(pattern.split(_m, 1) for _m in ret.stdout.splitlines() if ":" in _m)
                if self.wbench is None or self.wbench.stopthread.is_set():
                    break
                if matches:
                    self.thread.run_in_maincontext(self.add_file_matches, matches)
        except Exception as e:
            self.thread.run_in_maincontext(self.wbench.notifier.error, _("Search error!"), f"{e}")
        self.thread.flush()
        self.thread.run_in_maincontext(self.find_end)

    def find_end(self):
        """End of search operation."""
        self.thread = None
        if self.wbench is not None and not self.wbench.is_quitting():
            self.progress.set_progress_fraction(0.0)   # cancel bouncing
            if self.wbench.stopthread.is_set():
                msg = _("Search stopped! {0} matches in {1} files")
            else:
                msg = _("Found {0} matches in {1} files")
            self.wbench.notifier.status(msg.format(self.count_matches, self.count_files), False)

    @staticmethod
    def group_files(gfiles, maxlen):
        """Yield subset lists of files, for limiting batch processing."""
        flist = []
        flen = 0
        for gf in gfiles:
            path = gf.peek_path()
            if path is None:
                continue
            elif flen + len(path) < maxlen:
                flist.append(path)
                flen += len(path) + 1
            elif flist:
                yield flist
                flist = [path]
                flen = len(path) + 1
            else:
                raise ValueError("Search: file path too long!")
        if flist:
            yield flist

    @staticmethod
    def filter_files(gfiles, gfroot, fltr):
        """Yield files matching name patterns."""
        if fltr:
            fltr_list = filter(None, re.split(r"(?<!\\) +", fltr))   # unescape spaces
            mpatterns = tuple(EdMatchPattern.from_user(_f) for _f in fltr_list)
            for gf in gfiles:
                path = gf.peek_path() if gfroot is None else gfroot.get_relative_path(gf)
                if EdPathMatch().match_patterns(path, mpatterns):
                    yield gf
        else:
            yield from gfiles

    def add_file_matches(self, matches):
        """Feed search results treeview with last batch of matches."""
        if self.wbench.stopthread.is_set():
            return
        new_count_matches = self.count_matches + len(matches)
        if new_count_matches > 20_000:   # hard limit!
            self.wbench.stopthread.set()
            self.wbench.notifier.warn(_("Search in project: too many matches!"),
                                      _("Displayed results are incomplete."))
            return
        self.count_matches = new_count_matches
        treestore = self.treeview.get_model()
        currentpath, itr, gfile = self.current_file
        for path, line, match in matches:
            if path != currentpath:
                if itr is not None:
                    self.treeview.expand_row(treestore.get_path(itr), False)
                currentpath = path
                gfile = Gio.File.new_for_path(path)
                tt = GLib.markup_escape_text(path)
                itr = treestore.append(None, [GLib.path_get_basename(path), Pango.Weight.BOLD, None, False, tt, gfile])
                self.count_files += 1
            mtxt = re.sub(r"\s+", " ", match.strip())[:256]
            treestore.append(itr, [mtxt, Pango.Weight.NORMAL, f"{line}:", True, GLib.markup_escape_text(mtxt), gfile])
        del matches
        if itr is not None:
            self.treeview.expand_row(treestore.get_path(itr), False)
        self.current_file = (currentpath, itr, gfile)

    def on_menu_hide(self, itr):
        """Callback on menu action hide."""
        self.treeview.get_model().remove(itr)

    def on_menu_copy(self, itr):
        """Callback on menu action copy."""
        if txt := self.treeview.get_model().get_value(itr, 0):
            ed_set_clipboard_text(txt)

    def on_menu_openall(self, itr):
        """Callback on menu action openall."""
        gfiles = [_row[5] for _row in self.treeview.get_model()]
        self.wbench.request_open(gfiles, "")

    def on_menu_openwin(self, itr):
        """Callback on menu action openwin."""
        treestore = self.treeview.get_model()
        if gfile := treestore.get_value(itr, 5):
            line = treestore.get_value(itr, 2)
            ext_hints = {'goto': int(line.strip(":"))} if line else {}
            self.wbench.request_open([gfile], "newwin=true", **ext_hints)

    def on_event_click(self, event, n, x, y):
        """Callback on treeview button release event."""
        if n == 1:
            if path := self.treeview.get_path_at_pos(x, y):
                m = self.treeview.get_model()
                m.remove(m.get_iter(path[0]))
                event.set_state(Gtk.EventSequenceState.CLAIMED)

    def on_row_activated(self, tv, path, column):
        """Callback on treeview row activation."""
        treestore = tv.get_model()
        itr = treestore.get_iter(path)
        if itr is not None:
            if treestore.iter_has_child(itr):
                if tv.row_expanded(path):
                    tv.collapse_row(path)
                else:
                    tv.expand_to_path(path)
                tv.get_selection().unselect_all()
            elif treestore.get_value(itr, 3):   # line-visible
                gfile = treestore.get_value(itr, 5)
                line = int(treestore.get_value(itr, 2).strip(":"))
                self.wbench.request_open([gfile], "", goto=line)   # jump to match in file
            else:
                tv.get_selection().unselect_all()
