#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Context menus."""

from gi.repository import GLib, Gio, Gdk, Gtk


__all__ = ['EdContextMenuManager', 'EdTreeMenu']


class EdContextMenuManager():
    """Context menus manager."""
    __gtype_name__ = __qualname__

    def __init__(self, wbench, widget):
        assert widget.get_layout_manager() is not None
        self.wbench = wbench
        self.widget = widget
        self.action_groups = {}   # name: Gio.ActionGroup
        self.context_data = ()
        self.popups = []

    def free(self):
        """Release resources and break circular references."""
        for agroup in self.action_groups.values():
            for a in agroup.list_actions():
                agroup.lookup_action(a).disconnect_by_func(self.on_action)
                agroup.remove_action(a)
        for p in self.popups:
            p.free()
            p.unparent()
        self.action_groups.clear()
        self.popups.clear()
        self.wbench = None

    def add_action_group(self, group_name):
        """Add menu action."""
        ag = Gio.SimpleActionGroup()
        self.widget.insert_action_group(group_name, ag)
        self.action_groups[group_name] = ag

    def add_action(self, group_action, gvar, cbk, *user_data):
        """Add menu action."""
        assert callable(cbk)
        group_name, action_name = group_action.split('.')
        a = Gio.SimpleAction.new(action_name, None if gvar is None else GLib.VariantType(gvar))
        a.connect('activate', self.on_action, cbk, *user_data)
        if group_name not in self.action_groups:
            self.add_action_group(group_name)
        self.action_groups[group_name].add_action(a)

    def set_context_data(self, *data):
        """Set context data for current menu (oneshot)."""
        self.context_data = data

    def attach_menu(self, menu):
        """Bind popup menu to this manager."""
        menu.set_parent(self.widget)
        self.popups.append(menu)

    def on_action(self, action, param, cbk, *user_data):
        """Callback on menu action."""
        args = ()
        args += self.context_data
        args += () if param is None else (param.unpack(),)
        args += user_data
        cbk(*args)
        self.context_data = ()
        self.wbench.focus_active_doc()


class EdContextMenu(Gtk.PopoverMenu):
    """Context menu."""
    __gtype_name__ = __qualname__

    def __init__(self, manager, **kwargs):
        super().__init__(**kwargs)
        self.manager = manager
        self.manager.attach_menu(self)
        self.set_menu_model(Gio.Menu())

    def free(self):
        """Release resources and break circular references."""
        self.manager = None

    def add_item(self, title, detailed_action):
        """Add menu item."""
        self.get_menu_model().append(title, detailed_action)


class EdTreeMenu(EdContextMenu):
    """Context menu for TreeViews."""
    __gtype_name__ = __qualname__

    def __init__(self, manager, tv):
        super().__init__(manager, autohide=True, has_arrow=False, halign='start')
        # Events
        ev_click = Gtk.GestureClick(name='ed-sclick', button=Gdk.BUTTON_SECONDARY)
        ev_click.connect('pressed', self.on_event_click)
        tv.add_controller(ev_click)

    def on_event_click(self, event, n, x, y):
        """Callback on click event."""
        if n == 1:  ## event.get_current_event().triggers_context_menu():
            # If right click on valid row, then show the popup menu
            tv = event.get_widget()
            if path := tv.get_path_at_pos(x, y):
                tv.get_selection().select_path(path[0])
                px, py = tv.translate_coordinates(self.manager.widget, x, y)
                rect = Gdk.Rectangle()
                rect.x, rect.y, rect.width, rect.height = px, py, 1, 1
                self.set_pointing_to(rect)
                self.popup()
                self.manager.set_context_data(tv.get_model().get_iter(path[0]))
            else:
                tv.get_selection().unselect_all()
            event.set_state(Gtk.EventSequenceState.CLAIMED)
