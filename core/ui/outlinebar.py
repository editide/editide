#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Outline scrollbar."""

from gi.repository import GLib, Gdk, Gtk, Graphene


__all__ = ['EdOutlineBar']


ED_OUTLINEBAR_WIDTH = 7
ED_MIN_HANDLE_HEIGHT = 4


class EdOutlineHandle(Gtk.Widget):
    """Outline scrollbar handle."""
    __gtype_name__ = __qualname__

    def __init__(self, adj):
        super().__init__(overflow='hidden')
        self.add_css_class('ed-class-outlinebarslider')
        self.adj = adj

    def get_alloc_rect(self, w, h):
        """Get allocation rectangle inside parent's width/height."""
        pg, up, val = self.adj.get_page_size(), self.adj.get_upper(), self.adj.get_value()
        min_h, nat_h, min_b, nat_b = self.measure(Gtk.Orientation.VERTICAL, -1)
        rect = Gdk.Rectangle()
        if pg < up:   # check if full sourceview visible
            min_h = max(min_h, ED_MIN_HANDLE_HEIGHT)
            p = round(h * pg / up)                 # page size
            d = max(0, min_h - p)                  # extra height to ensure minimal handle size
            y = round((h - p) * val / (up - pg))   # page position
            # Center handle around page position, with non-linear correction around scrollbar ends
            a = y // 2 if y < d else d - (h - p - y) // 2 if y > h - p - d else d // 2
            rect.x, rect.y, rect.width, rect.height = 0, y - a, w, p + d
        else:
            rect.x, rect.y, rect.width, rect.height = 0, 0, w, h
        return rect

    def adjustment_update(self):
        """React on adjustment size/position change."""
        w, h = self.get_parent().get_width(), self.get_parent().get_height()
        if h > 0:   # ensure parent is allocated
            self.size_allocate(self.get_alloc_rect(w, h), -1)

    def do_snapshot(self, snap):   # override
        """Drawing handler."""
        pg, up = self.adj.get_page_size(), self.adj.get_upper()
        if pg < up:   # no handle if full sourceview visible
            w, h = self.get_width(), self.get_height()
            color = self.get_color()
            rect = Graphene.Rect().init(0.0, 0.0, w, h)
            snap.append_color(color, rect)


class EdOutlineMarks(Gtk.Widget):
    """Outline scrollbar marks."""
    __gtype_name__ = __qualname__

    def __init__(self, scrollable):
        super().__init__()
        self.scrollable = scrollable
        self.marks = {}   # side: (color, set(positions))
        self.last_up = -1

    def set_marks(self, side, color_spec, marks_set):
        """Update list of marks to be outlined."""
        assert side in ('left', 'fill', 'right')
        if side not in self.marks or marks_set or self.marks[side][1]:
            color = Gdk.RGBA()
            color.parse(color_spec)
            color.alpha = 1.0
            self.marks[side] = (color, marks_set)
            self.queue_draw()

    def clear_marks(self, side):
        """Remove marks on given side."""
        if side in self.marks:
            del self.marks[side]
            self.queue_draw()

    def adjustment_update(self, adj):
        """React on adjustment size change."""
        up = adj.get_upper()
        if up != self.last_up:
            self.last_up = up
            self.queue_draw()

    def do_snapshot(self, snap):   # override
        """Drawing handler."""
        min_req, nat_req = self.scrollable.get_preferred_size()
        w, h = self.get_width(), min(self.get_height(), nat_req.height)
        for side, data in self.marks.items():
            c, mset = data
            x = w // 2 + 1 if side == 'right' else 1
            d = w - 2 if side == 'fill' else (w - 1) // 2 - 1
            for m in mset:
                rect = Graphene.Rect().init(x, round(h * m) - 1.0, d, 2.0)
                snap.append_color(c, rect)


class EdOutlineBar(Gtk.Widget):
    """Outline scrollbar, core part handling registration and events."""
    __gtype_name__ = __qualname__

    def __init__(self, scrollable):
        super().__init__()
        self.set_size_request(ED_OUTLINEBAR_WIDTH, 2 * ED_MIN_HANDLE_HEIGHT)
        self.add_css_class('ed-class-outlinebar')
        self.update_pending = {}
        self.scrollwin = scrollable.get_ancestor(Gtk.ScrolledWindow.__gtype__)
        self.adj = self.scrollwin.get_vadjustment()
        self.drawer = EdOutlineMarks(scrollable)
        self.handle = EdOutlineHandle(self.adj)
        self.drawer.set_parent(self)
        self.handle.set_parent(self)
        # Events
        sflags = Gtk.EventControllerScrollFlags.VERTICAL | Gtk.EventControllerScrollFlags.DISCRETE
        ev_scroll = Gtk.EventControllerScroll(name='ed-scroll', flags=sflags)
        ev_click = Gtk.GestureClick(name='ed-pclick', button=Gdk.BUTTON_PRIMARY)
        ev_drag = Gtk.GestureDrag(name='ed-drag')
        ev_scroll.connect('scroll', self.on_event_scroll)
        ev_click.connect('pressed', self.on_event_click)
        ev_drag.connect('drag-update', self.on_event_drag)
        self.add_controller(ev_scroll)
        self.add_controller(ev_click)
        self.add_controller(ev_drag)
        self.adj.connect('changed', self.on_adj_changed)
        self.adj.connect('value-changed', self.on_adj_value_changed)

    def free(self):
        """Release resources and break circular references."""
        self.adj.disconnect_by_func(self.on_adj_changed)
        self.adj.disconnect_by_func(self.on_adj_value_changed)
        self.drawer.unparent()
        self.handle.unparent()
        self.adj = None
        self.drawer = None
        self.handle = None

    def set_search_context(self, searchcontext, side):
        """Register a new search context."""
        assert side in ('left', 'fill', 'right')
        self.drawer.clear_marks(side)
        if searchcontext is not None:
            searchcontext.connect('notify::occurrences-count', self.on_search_occurrences, side)

    def on_search_occurrences(self, searchcontext, pspec, side):
        """Callback on new search results."""
        if not self.update_pending.get(side, False):
            self.update_pending[side] = True
            GLib.idle_add(self.update_matches, searchcontext, side, priority=GLib.PRIORITY_LOW)

    def update_matches(self, searchcontext, side):
        """Collect matches and forward to drawer."""
        self.update_pending[side] = False
        n = searchcontext.get_occurrences_count()
        b = searchcontext.get_buffer()
        style = searchcontext.get_match_style() or b.get_style_scheme().get_style('search-match')
        color = style.props.background
        matches = set()   # no line duplicates
        if 0 < n < 5000:   # hard limit!
            n_lines = b.get_line_count() + 1
            iend = b.get_start_iter()
            while True:
                res, ibeg, iend, wrap = searchcontext.forward(iend)
                if res and not wrap:
                    matches.add((ibeg.get_line() + 0.5) / n_lines)
                else:
                    break
        self.drawer.set_marks(side, color, matches)

    def on_adj_changed(self, adj):
        """Callback on adjustment size change."""
        self.drawer.adjustment_update(adj)
        self.handle.adjustment_update()

    def on_adj_value_changed(self, adj):
        """Callback on adjustment value change."""
        self.handle.adjustment_update()

    def on_event_scroll(self, event, dx, dy):
        """Scroll event handler."""
        styp = (Gtk.ScrollType.PAGE_UP if dy < 0 else
                Gtk.ScrollType.PAGE_DOWN if dy > 0 else
                Gtk.ScrollType.NONE)
        self.scrollwin.emit('scroll-child', styp, False)

    def on_event_click(self, event, n, x, y):
        """Click event handler."""
        h = self.get_height()
        pg, up = self.adj.get_page_size(), self.adj.get_upper()
        self.adj.set_value((up * y / h) - (pg / 2.0))   # page centered on pointer

    def on_event_drag(self, event, dx, dy):
        """Drag update event handler."""
        ok, x, y = event.get_start_point()
        if ok:
            self.on_event_click(event, 1, x, y + dy)

    def do_size_allocate(self, width, height, baseline):   # override
        """Size allocation handler."""
        rect = Gdk.Rectangle()
        rect.x, rect.y, rect.width, rect.height = 0, 0, width, height
        self.drawer.size_allocate(rect, baseline)
        self.handle.size_allocate(self.handle.get_alloc_rect(width, height), baseline)
