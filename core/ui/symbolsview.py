#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Symbols browser."""

from gi.repository import GLib, Pango, Gtk

from .contextmenu import EdTreeMenu
from ..utils.misc import ed_get_kind_color, ed_set_clipboard_text


__all__ = ['EdSymbolsView']


class EdSymbolsView():
    """Browse symbols."""

    def __init__(self, wbench, tv):
        self.wbench = wbench
        self.treeview = tv
        self.sort = False
        self.limit = -1
        if self.treeview.get_model() is None:
            # [kind-letter, symbol-name, line, tooltip, color]
            treestore = Gtk.TreeStore(str, str, int, str, str)
            self.treeview.set_model(treestore)
            self.setup_treeview()
        # Popup menu
        self.wbench.menumanager.add_action('symbol.copy', None, self.on_menu_copy)
        self.wbench.menumanager.add_action('symbol.search', None, self.on_menu_search)
        menu = EdTreeMenu(self.wbench.menumanager, self.treeview)
        menu.add_item(_("_Copy Symbol Name"), 'symbol.copy')
        menu.add_item(_("_Search in Project"), 'symbol.search')
        # Events
        self.treeview.connect('row-activated', self.on_row_activated)

    def free(self):
        """Release resources and break circular references."""
        self.treeview.disconnect_by_func(self.on_row_activated)
        self.reset()
        self.wbench = None

    def setup_treeview(self):
        """Setup symbols treeview."""
        c = Gtk.TreeViewColumn()
        c.set_expand(True)
        c.set_spacing(6)
        # Column cell: kind
        cr = Gtk.CellRendererText(family='monospace', weight=Pango.Weight.BOLD, style='italic')
        c.pack_start(cr, False)
        c.add_attribute(cr, 'text', 0)
        c.add_attribute(cr, 'foreground', 4)
        # Column cell: symbol name
        cr = Gtk.CellRendererText(ellipsize='end')
        c.pack_start(cr, True)
        c.add_attribute(cr, 'text', 1)
        # Add column
        self.treeview.append_column(c)
        self.treeview.set_tooltip_column(3)

    def reset(self):
        """Reset view."""
        self.treeview.get_model().clear()

    def update(self, syms, /, *, sort=False, limit=2000):
        """Update treeview with symbols."""
        self.sort = sort
        self.limit = limit
        complete = True
        treestore = self.treeview.get_model()
        if treestore is not None:   # in case called at shutdown
            treestore.clear()
            try:
                self.add_syms_for_parent(treestore, None, syms)
            except OverflowError:
                c = self.treeview.get_color()
                c.alpha = 0.5
                treestore.append(None, ["+", "...", 0, GLib.markup_escape_text(_("Show More...")), c.to_string()])
                complete = False
            self.expand()
        return complete

    def add_syms_for_parent(self, treestore, parent, syms):
        """Add symbols under parent scope."""
        for s in (sorted(syms) if self.sort else syms):
            if s['extras'] == 'reference':
                continue   # don't show refs in tree
            if self.limit == 0:
                raise OverflowError
            self.limit -= 1
            tip = self.get_tooltip(s)
            c = ed_get_kind_color(s['kl'])
            itr = treestore.append(parent, [s['kl'], s['name'], s['line'], tip, c])
            self.add_syms_for_parent(treestore, itr, s.children)

    @staticmethod
    def get_tooltip(sym):
        """Get tooltip for symbol."""
        tip = []
        for p in ('nameref', 'kind', 'inherits', 'signature', 'typeref', 'line'):
            if v := sym[p]:
                v = str(v)
                if p.endswith('ref'):
                    v = v.split(":", 1)[-1]
                tip.append(f"<b>{p.capitalize()}:</b> {GLib.markup_escape_text(v)}")
        return "\n".join(tip) or "???"

    def expand(self):
        """Expand all treeview lines."""
        self.treeview.expand_all()

    def collapse(self):
        """Collapse all treeview lines."""
        self.treeview.collapse_all()

    def on_menu_copy(self, itr):
        """Callback on menu action copy."""
        if txt := self.treeview.get_model().get_value(itr, 1):
            ed_set_clipboard_text(txt)

    def on_menu_search(self, itr):
        """Callback on menu action search."""
        if txt := self.treeview.get_model().get_value(itr, 1):
            self.wbench.set_search_settings(find=txt, repl="", wb=True, cs=True, rx=False, es=False)
            self.wbench.on_search_start()

    def on_row_activated(self, tv, path, column):
        """Callback on treeview row activation."""
        treestore = tv.get_model()
        itr = treestore.get_iter(path)
        if itr is not None:
            w, d = self.wbench.get_active_doc()
            if d is not None:
                line = treestore.get_value(itr, 2)
                if line == 0 and treestore.get_value(itr, 0) == "+":
                    self.wbench.on_symbols_showmore()
                else:
                    d.go_to(line)   # jump to symbol line
