#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Custom dialogs helpers."""

from gi.repository import GLib, Gio, Gtk


__all__ = ['EdFileChooser', 'EdMessage']


ed_active_dialogs = set()   # Gtk doesn't hold references to native dialogs, keep one here


class EdDialogResponseHandlerMixin():
    """Mixin helper for dialog async responses."""

    def setup_response_handler(self):
        """Initialize response handlers."""
        self.resp_handlers = {}   # Gtk.ResponseType: (callback, user-args)
        ed_active_dialogs.add(self)

    def add_response_handler(self, resp_id, cbk, *user_args):
        """Set callback handler for response ID."""
        assert callable(cbk)
        self.resp_handlers[resp_id] = (cbk, user_args)

    def on_response(self, dialog, resp_id):
        """Callback on dialog response."""
        self.parent = None
        ed_active_dialogs.discard(self)
        if resp_id in self.resp_handlers:
            cbk, user_args = self.resp_handlers[resp_id]
            GLib.idle_add(cbk, *user_args)
        self.resp_handlers.clear()
        if dialog:
            self.disconnect_by_func(self.on_response)
            dialog.destroy()


class EdFileChooser(EdDialogResponseHandlerMixin, Gtk.FileDialog):
    """Custom file-chooser dialog."""
    __gtype_name__ = __qualname__

    def __init__(self, parent, title, /, *, filters=None, **kwargs):
        super().__init__(title=title, modal=True, **kwargs)
        self.parent = parent
        self.files = None
        if filters:
            filters_list = Gio.ListStore.new(Gtk.FileFilter)
            for n, p in filters:
                ff = Gtk.FileFilter()
                ff.set_name(n)
                ff.add_pattern(p)
                filters_list.append(ff)
            self.set_filters(filters_list)
        self.setup_response_handler()

    def set_response_handler(self, cbk, *user_args):
        """Set callback handler for response."""
        self.add_response_handler(Gtk.ResponseType.ACCEPT, cbk, self, *user_args)

    def get_file(self):
        """Get selected file."""
        if isinstance(self.files, Gio.File):
            return self.files
        return None

    def get_files(self):
        """Get selected files list."""
        if isinstance(self.files, Gio.ListModel):
            return tuple(self.files)
        if gf := self.get_file():
            return (gf,)
        return ()

    def set_initial_location(self, /, *, folder=None, gfile=None, name=None):
        """Set initial location."""
        if gfile is not None:
            self.set_initial_file(gfile)
        else:
            if folder is not None:
                self.set_initial_folder(folder)
            if name:
                self.set_initial_name(name)

    def show_open(self, **kwargs):
        """Show file chooser dialog for opening single file."""
        self.set_initial_location(**kwargs)
        self.open(self.parent, None, self.on_files, self.open_finish)

    def show_open_multiple(self, **kwargs):
        """Show file chooser dialog for opening single file."""
        self.set_initial_location(**kwargs)
        self.open_multiple(self.parent, None, self.on_files, self.open_multiple_finish)

    def show_save(self, **kwargs):
        """Show file chooser dialog for opening single file."""
        self.set_initial_location(**kwargs)
        self.save(self.parent, None, self.on_files, self.save_finish)

    def show_select_folder(self, **kwargs):
        """Show file chooser dialog for opening single file."""
        self.set_initial_location(**kwargs)
        self.select_folder(self.parent, None, self.on_files, self.select_folder_finish)

    def on_files(self, dialog, res, fetch_cbk):
        """Callback on file dialog return."""
        try:
            self.files = fetch_cbk(res)
        except GLib.Error:
            self.files = None
        self.on_response(None, Gtk.ResponseType.ACCEPT if self.files else Gtk.ResponseType.CANCEL)


class EdMessage(EdDialogResponseHandlerMixin, Gtk.MessageDialog):
    """Custom message dialog."""
    __gtype_name__ = __qualname__

    def __init__(self, parent, button_ids, title, msg):
        super().__init__(transient_for=parent, modal=True, buttons='none', text=title, secondary_text=msg)
        self.parent = parent
        for b in button_ids:
            bname = b.lstrip('+-')
            w = self.add_button(_(bname.capitalize()), getattr(Gtk.ResponseType, bname.upper()))
            if b.startswith('+'):
                w.add_css_class('suggested-action')
            if b.startswith('-'):
                w.add_css_class('destructive-action')
        self.set_default_response(Gtk.ResponseType.CANCEL)
        self.setup_response_handler()
        self.connect('response', self.on_response)

    def show_message(self):
        """Show message dialog."""
        self.set_visible(True)
