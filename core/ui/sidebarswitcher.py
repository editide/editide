#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Manage sidebar elements visibility."""

from gi.repository import GObject, GLib, Gtk


__all__ = ['EdSidebarSwitcher']


class EdSidebarSwitcher():
    """Sidebar stack switcher."""

    def __init__(self, sidebar, stack, switcher):
        self.sidebar = sidebar
        self.stack = stack
        self.switcher = switcher
        self.stackpages = self.stack.get_pages()   # GtkStack only keeps a weak ref
        self.buttons = {}   # page_name: button
        for pos, page in enumerate(self.stackpages):
            self.add_button_for_page(page, pos)
        self.stackpages.connect('items-changed', self.on_stack_changed)

    def free(self):
        """Release resources and break circular references."""
        self.stackpages.disconnect_by_func(self.on_stack_changed)
        self.buttons.clear()
        self.stackpages = None

    def add_button_for_page(self, page, pos):
        """Add a switch button for page at position."""
        name = page.get_name()
        icon = page.get_icon_name()
        assert name not in self.buttons
        b = Gtk.ToggleButton(icon_name=icon, has_frame=False, focus_on_click=False)
        b.set_tooltip_text(page.get_title())
        b.add_css_class('image-button')
        b.add_css_class('large-icons')
        self.switcher.insert_child_after(b, list(self.switcher)[pos-1] if pos > 0 else None)
        self.buttons[name] = b
        page.get_child().bind_property('visible', b, 'visible', GObject.BindingFlags.SYNC_CREATE)
        page.get_child().connect('map', self.on_map, name)
        page.get_child().connect('unmap', self.on_unmap, name)
        b.connect('toggled', self.on_button_toggled, name)

    def get_visible_page_name(self):
        """Get name of visible page, or None."""
        if self.sidebar.get_visible():
            return self.stack.get_visible_child_name()
        return None

    def set_visibility(self, show=None, /):
        """Change sidebar visibility."""
        if show is None:
            show = not self.sidebar.get_visible()   # toggle
        self.sidebar.set_visible(show)

    def show_page(self, pagename):
        """Set visible page."""
        self.stack.set_visible_child_name(pagename)
        self.set_visibility(True)

    def show_next_page(self):
        """Switch to next page."""
        if not self.sidebar.get_visible():
            return
        if children := [_c.get_child() for _c in self.stackpages]:
            c = n = children.index(self.stack.get_visible_child())
            while True:
                n = (n + 1) % len(children)
                if n == c:
                    return
                if children[n].get_visible():
                    self.stack.set_visible_child(children[n])
                    return

    def on_stack_changed(self, select_model, pos, removed, added):
        """Callback on stack modified."""
        for p in range(added):
            page = select_model.get_item(pos + p)
            GLib.idle_add(self.add_button_for_page, page, pos + p, priority=GLib.PRIORITY_DEFAULT)

    def on_button_toggled(self, button, pagename):
        """Callback on switcher button toggled."""
        page = self.stack.get_child_by_name(pagename)
        if button.get_active() == page.get_mapped():
            pass
        elif page.get_mapped():
            self.set_visibility(False)
        else:
            self.stack.set_visible_child_name(pagename)
            self.set_visibility(True)

    def on_map(self, page, name):
        """Callback on page mapped, i.e. becomes visible."""
        self.buttons[name].set_active(True)

    def on_unmap(self, page, name):
        """Callback on page unmapped, i.e. becomes invisible."""
        self.buttons[name].set_active(False)
