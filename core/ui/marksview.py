#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Marks browser."""

import re

from gi.repository import GLib, Pango, Gtk


__all__ = ['EdMarksView']


class EdMarksView():
    """View list of marked places."""

    def __init__(self, wbench, tv):
        self.wbench = wbench
        self.treeview = tv
        if self.treeview.get_model() is None:
            # [file/match, weight, line, line-visible, tooltip, doc-id]
            treestore = Gtk.TreeStore(str, int, str, bool, str, str)
            self.treeview.set_model(treestore)
            self.setup_treeview()
        # Events
        self.treeview.connect('map', self.on_map)
        self.treeview.connect('row-activated', self.on_row_activated)

    def free(self):
        """Release resources and break circular references."""
        self.treeview.disconnect_by_func(self.on_map)
        self.treeview.disconnect_by_func(self.on_row_activated)
        self.reset()
        self.wbench = None

    def setup_treeview(self):
        """Setup search results treeview."""
        c = Gtk.TreeViewColumn()
        c.set_expand(True)
        c.set_spacing(6)
        # Column cell: line number
        cr = Gtk.CellRendererText()
        cr.set_alignment(1.0, 0.5)
        cr.set_sensitive(False)
        c.pack_start(cr, False)
        c.add_attribute(cr, 'text', 2)
        c.add_attribute(cr, 'visible', 3)
        # Column cell: file name or match
        cr = Gtk.CellRendererText(ellipsize='end')
        c.pack_end(cr, True)
        c.add_attribute(cr, 'text', 0)
        c.add_attribute(cr, 'weight', 1)
        # Add column
        self.treeview.append_column(c)
        self.treeview.set_tooltip_column(4)
        self.treeview.set_level_indentation(-16)

    def reset(self):
        """Reset view."""
        self.treeview.get_column(0).set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        self.treeview.get_model().clear()
        self.treeview.get_column(0).set_sizing(Gtk.TreeViewColumnSizing.GROW_ONLY)

    def load_from_docs(self, docs):
        """Load marks from documents."""
        self.reset()
        for d in docs:
            if marks := d.get_context().get('marks', []):
                self.add_doc_marks(d, marks)

    def add_doc_marks(self, doc, marks):
        """Add marks in treeview."""
        treestore = self.treeview.get_model()
        filename = doc.tablabel
        doc_id = str(id(doc))
        tooltip = GLib.markup_escape_text(doc.get_doc_fullname())
        itr = treestore.append(None, [filename, Pango.Weight.BOLD, None, False, tooltip, doc_id])
        for pos, category in marks:
            text = doc.get_text_at_pos(pos) or ""
            mtxt = re.sub(r"\s+", " ", text.strip())[:256]
            tooltip = GLib.markup_escape_text(category)
            treestore.append(itr, [mtxt, Pango.Weight.NORMAL, f"{pos}:", True, tooltip, doc_id])
        self.treeview.expand_row(treestore.get_path(itr), False)

    def go_to_mark(self, treestore, itr):
        """Go to mark from tree iter."""
        doc_id = int(treestore.get_value(itr, 5))
        pos = int(treestore.get_value(itr, 2).strip(":"))
        for d in self.wbench.get_docs_sorted():
            if doc_id == id(d):
                self.wbench.select_doc(d)
                d.go_to(pos)
                break

    def on_map(self, w):
        """Callback on treeview mapped, i.e. becomes visible."""
        GLib.idle_add(self.wbench.on_marks_reload, priority=GLib.PRIORITY_DEFAULT)

    def on_row_activated(self, tv, path, column):
        """Callback on treeview row activation."""
        treestore = tv.get_model()
        itr = treestore.get_iter(path)
        if itr is not None:
            if treestore.iter_has_child(itr):
                if tv.row_expanded(path):
                    tv.collapse_row(path)
                else:
                    tv.expand_to_path(path)
                tv.get_selection().unselect_all()
            elif treestore.get_value(itr, 3):   # line-visible
                self.go_to_mark(treestore, itr)
            else:
                tv.get_selection().unselect_all()
