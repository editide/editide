#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Advanced alternative to GtkNotebook."""

from gi.repository import GObject, GLib, Gio, Gdk, Gtk

from ..utils.history import EdHistoryTracker
from ..utils.misc import ed_get_datapath, ed_get_modif_indicator, ed_is_rtl
from ..utils.wrapper import EdBuilderWrapper


__all__ = ['EdNotebook']


class EdNotebookDragContent(GObject.Object):
    """Type for tab drag'n'drop content provider."""
    __gtype_name__ = __qualname__

    def __init__(self, nb, tab):
        super().__init__()
        self.notebook = nb
        self.tab = tab


class EdNotebookTab(Gtk.Label):
    """Tab item in notebook header."""
    __gtype_name__ = __qualname__

    def __init__(self, title):
        super().__init__(single_line_mode=True, max_width_chars=40, ellipsize='end', accessible_role='tab')
        self.add_css_class('ed-class-notebooktab')
        self.update_state([Gtk.AccessibleState.SELECTED], [int(False)])
        self.set_focus_on_click(False)
        self.set_focusable(False)
        for c in list(self.observe_controllers()):
            self.remove_controller(c)   # useless and conflicts with kbd focus navigation
        self.modif_indicator = ed_get_modif_indicator(self)
        self.set_title(title, False)
        # Actions group
        self.agroup = Gio.SimpleActionGroup()
        self.insert_action_group('tab', self.agroup)
        # Context menu
        self.popup = Gtk.PopoverMenu(menu_model=Gio.Menu(), has_arrow=True, halign='center', autohide=True)
        self.popup.set_parent(self)
        # Events
        ev_click = Gtk.GestureClick(name='ed-sclick', button=Gdk.BUTTON_SECONDARY)
        ev_click.connect('pressed', self.on_event_click)
        self.add_controller(ev_click)

    def free(self):
        """Release resources and break circular references."""
        for a in self.agroup.list_actions():
            self.agroup.remove_action(a)
        self.popup.unparent()
        self.popup = None

    def get_title(self):
        """Get title of tab."""
        return self.get_text()

    def set_title(self, title, modified):
        """Set title of tab."""
        m = f"{self.modif_indicator}\u2002" if modified else ""
        self.set_text(m + title)

    def set_active_state(self, active):
        """Set tab active state."""
        if active:
            self.add_css_class('ed-class-notebookactivetab')
        else:
            self.remove_css_class('ed-class-notebookactivetab')
        self.update_state([Gtk.AccessibleState.SELECTED], [int(active)])
        self.set_focusable(active)

    def is_closing(self):
        """Check if tab is closing."""
        return self.popup is None or self.in_destruction()

    def move_focus(self):
        """Ensure the focus moves to the active tab."""
        self.get_parent().child_focus(Gtk.DirectionType.TAB_FORWARD)

    def do_focus(self, d):   # override
        """Override focus navigation."""
        if not self.get_focusable():
            return False
        if self.is_focus():
            match d:
                case Gtk.DirectionType.LEFT:
                    self.activate_action('win.nexttab' if ed_is_rtl() else 'win.prevtab', None)
                    GLib.idle_add(self.move_focus, priority=GLib.PRIORITY_LOW)
                    return True
                case Gtk.DirectionType.RIGHT:
                    self.activate_action('win.prevtab' if ed_is_rtl() else 'win.nexttab', None)
                    GLib.idle_add(self.move_focus, priority=GLib.PRIORITY_LOW)
                    return True
            # Bypass label handler
            return Gtk.Widget.do_focus(self, d)
        return self.grab_focus()

    def do_grab_focus(self):   # override
        """Override focus grabbing."""
        # Bypass label handler
        return Gtk.Widget.do_grab_focus(self)

    def do_size_allocate(self, width, height, baseline):   # override
        """Override size-allocate handler to manage popup menu."""
        Gtk.Label.do_size_allocate(self, width, height, baseline)
        self.popup.present()

    def on_event_click(self, event, n, x, y):
        """Callback on click event."""
        if n == 1:  ## event.get_current_event().triggers_context_menu():
            self.popup.popup()
            event.set_state(Gtk.EventSequenceState.CLAIMED)


class EdNotebookHeader():
    """Container for notebook tabs."""

    def __init__(self, nbcore):
        self.nbcore = nbcore
        self.active_tab = None
        self.drag_src = None
        self.drag_ignore = None
        # GUI
        self.header = self.nbcore.get_nb_ui('ed_box_nbheader')
        self.tabs_box = self.nbcore.get_nb_ui('ed_box_nbtabs')
        self.scrollwin = self.nbcore.get_nb_ui('ed_scrollwin_nbtabs')
        # Events
        sprop = Gtk.PropagationPhase.CAPTURE   # catch before forwarding to scrollwin
        sflags = Gtk.EventControllerScrollFlags.VERTICAL | Gtk.EventControllerScrollFlags.DISCRETE
        dnd_actions = Gdk.DragAction.COPY | Gdk.DragAction.MOVE
        ev_scroll = Gtk.EventControllerScroll(name='ed-scroll', propagation_phase=sprop, flags=sflags)
        ev_click = Gtk.GestureClick(name='ed-click', button=0)
        ev_drag = Gtk.DragSource(name='ed-drag', actions=dnd_actions)
        ev_scroll.connect('scroll', self.on_event_scroll)
        ev_click.connect('pressed', self.on_event_click)
        ev_drag.connect('prepare', self.on_event_dragprepare)
        ev_drag.connect('drag-end', self.on_event_dragend)
        self.scrollwin.add_controller(ev_scroll)
        self.scrollwin.add_controller(ev_click)
        self.scrollwin.add_controller(ev_drag)

    def get_ordered_tabs(self):
        """Get list of tabs, in their displayed order."""
        return list(self.tabs_box)

    def get_current_tab(self):
        """Get currently active tab."""
        return self.active_tab

    def get_tab_index(self, tab):
        """Get tab position if header."""
        return self.get_ordered_tabs().index(tab)

    def get_tab_at_coords(self, x, y):
        """Find tab relative to the scrollwin coordinates."""
        rel_x, rel_y = self.scrollwin.translate_coordinates(self.tabs_box, x, y)
        if w := self.tabs_box.pick(rel_x, rel_y, Gtk.PickFlags.DEFAULT):
            if t := w.get_ancestor(EdNotebookTab.__gtype__):
                return t
        return None

    def add_tab(self, title):
        """Add new tab in the header."""
        t = EdNotebookTab(title)
        self.tabs_box.append(t)
        self.header.set_visible(True)
        GLib.idle_add(self.scroll_to, t, priority=GLib.PRIORITY_DEFAULT_IDLE-1)
        return t

    def del_tab(self, tab):
        """Remove tab from the header, and switch to the previous active one."""
        tab.free()
        self.tabs_box.remove(tab)
        tabs = self.get_ordered_tabs()
        assert tab not in tabs   # destroy success
        if len(tabs) == 0:
            self.active_tab = None
            self.header.set_visible(False)

    def activate_tab(self, tab):
        """Activate tab."""
        if self.active_tab is not tab:
            for t in self.get_ordered_tabs():
                t.set_active_state(t is tab)
            self.active_tab = tab
        self.scroll_to(tab)

    def tab_prev(self):
        """Activate previous tab by position, loop if first one."""
        tabs = self.get_ordered_tabs()
        if len(tabs) > 1:
            n = self.get_tab_index(self.active_tab)
            t = tabs[(n - 1) % len(tabs)]
            self.nbcore.show_page_of_tab(t)

    def tab_next(self):
        """Activate next tab by position, loop if last one."""
        tabs = self.get_ordered_tabs()
        if len(tabs) > 1:
            n = self.get_tab_index(self.active_tab)
            t = tabs[(n + 1) % len(tabs)]
            self.nbcore.show_page_of_tab(t)

    def scroll_to(self, tab):
        """Scroll to tab so it's visible in the header."""
        x0, y = tab.translate_coordinates(self.tabs_box, 0, 0) or (-1, -1)
        if x0 > 0:   # tab allocated
            u = self.scrollwin.get_hadjustment().get_upper()
            x1 = x0 + tab.get_width()
            if self.active_tab not in (tab, None):   # try to keep active tab visible
                ax, ay = self.active_tab.translate_coordinates(self.tabs_box, 0, 0) or (-1, -1)
                x0 = min(x0, ax)
            self.scrollwin.get_hadjustment().clamp_page(max(0, x0 - 75), min(u, x1 + 75))
        elif not tab.is_closing():
            GLib.idle_add(self.scroll_to, tab)   # retry later
        return GLib.SOURCE_REMOVE

    def on_event_scroll(self, event, dx, dy):
        """Callback on tabs scroll event."""
        step = self.scrollwin.get_hadjustment().get_page_size() / 4.0
        if dy < 0:
            if event.get_current_event_state() & Gdk.ModifierType.CONTROL_MASK:
                self.tab_prev()
            else:
                self.scrollwin.get_hadjustment().set_step_increment(step)
                self.scrollwin.emit('scroll-child', Gtk.ScrollType.STEP_BACKWARD, True)
        elif dy > 0:
            if event.get_current_event_state() & Gdk.ModifierType.CONTROL_MASK:
                self.tab_next()
            else:
                self.scrollwin.get_hadjustment().set_step_increment(step)
                self.scrollwin.emit('scroll-child', Gtk.ScrollType.STEP_FORWARD, True)
        return Gdk.EVENT_STOP

    def on_event_click(self, event, n, x, y):
        """Callback on click event."""
        tab = self.get_tab_at_coords(x, y)
        button = event.get_current_button()
        if button == Gdk.BUTTON_PRIMARY:
            self.nbcore.wbench.select_notebook(self.nbcore)
        if tab is None:
            if button == Gdk.BUTTON_PRIMARY and n == 2:
                event.get_widget().activate_action('win.newdoc', None)
                event.set_state(Gtk.EventSequenceState.CLAIMED)
        elif button == Gdk.BUTTON_PRIMARY:
            self.nbcore.show_page_of_tab(tab)
        elif button == Gdk.BUTTON_MIDDLE and n == 1:
            GLib.timeout_add(200, self.nbcore.request_close_tab, tab)   # timeout for debouncing
            event.set_state(Gtk.EventSequenceState.CLAIMED)

    def on_event_dragprepare(self, event, x, y):
        """Callback on drag event prepare."""
        if self.drag_src is None:
            tab = self.get_tab_at_coords(x, y)
            if tab is not None:
                self.drag_src = tab
                self.drag_ignore = None
                content = EdNotebookDragContent(self.nbcore, tab)
                return Gdk.ContentProvider.new_for_value(content)
        return None

    def on_event_dragupdate(self, drop_widget, dx, dy):
        """Callback on drag event update."""
        if self.drag_src is not None:
            x, y = drop_widget.translate_coordinates(self.scrollwin, dx, dy) or (-1, -1)
            tab = self.get_tab_at_coords(x, y)
            if tab not in [None, self.drag_src, self.drag_ignore]:
                self.drag_ignore = tab
                new_pos = tab.get_prev_sibling() if self.get_tab_index(tab) < self.get_tab_index(self.drag_src) else tab
                self.tabs_box.reorder_child_after(self.drag_src, new_pos)
                GLib.timeout_add(200, self.scroll_to, self.drag_src)
            elif tab is not self.drag_ignore:
                self.drag_ignore = None
            ##if self.drag_src.get_cursor() is None:
            ##    self.drag_src.set_cursor_from_name('grabbing')

    def on_event_dragend(self, event, drag, delete):
        """Callback on drag event end."""
        ##if self.drag_src is not None:
        ##    self.drag_src.set_cursor(None)
        self.drag_src = None
        self.drag_ignore = None


class EdNotebook():
    """Custom notebook implementation, to replace obsolete GtkNotebook."""

    def __init__(self, wbench):
        self.wbench = wbench
        self.tabs = {}   # page_widget: header_tab
        self.history = EdHistoryTracker()
        self.signals = {_s: [] for _s in ('page-added', 'page-removed', 'switch-page')}
        # GUI
        self.builderwrapper = EdBuilderWrapper(self)
        self.builder = Gtk.Builder(self.builderwrapper)
        self.builder.add_from_file(ed_get_datapath("notebook.ui"))
        self.header = EdNotebookHeader(self)
        self.get_nb_ui('ed_menu_nbheader').set_create_popup_func(self.on_create_menu)
        self.pages_container = self.get_nb_ui('ed_stack_nbpages')
        if placeholder := self.pages_container.get_child_by_name('placeholder'):
            self.tabs[placeholder] = None
        # Actions
        action = Gio.SimpleAction.new('selecttab', GLib.VariantType('i'))
        action.connect('activate', self.action_selecttab)
        agroup = Gio.SimpleActionGroup()
        agroup.add_action(action)
        self.get_widget().insert_action_group('notebook', agroup)
        # Events
        dnd_actions = Gdk.DragAction.COPY | Gdk.DragAction.MOVE
        ev_drop = Gtk.DropTarget.new(EdNotebookDragContent.__gtype__, dnd_actions)
        ev_drop.set_name('ed-drop')
        ev_drop.set_preload(True)   # make drag value available in motion events
        ev_drop.connect('motion', self.on_event_dropmotion)
        ev_drop.connect('drop', self.on_event_drop)
        self.get_widget().add_controller(ev_drop)

    def free(self):
        """Release resources and break circular references."""
        for cbks in self.signals.values():
            cbks.clear()
        for page, tab in self.tabs.items():
            if tab is not None:
                self.header.del_tab(tab)
            self.pages_container.remove(page)
        self.builderwrapper.free()
        self.history.clear()
        self.tabs.clear()
        self.builder = None
        self.builderwrapper = None
        self.wbench = None

    def get_nb_ui(self, elem):
        """Get notebook UI element by name."""
        return self.builder.get_object(elem)

    def get_widget(self):
        """Get notebook widget."""
        return self.get_nb_ui('ed_box_notebook')

    def set_active(self, active):
        """Set notebook as the active one."""
        if active:
            self.get_widget().add_css_class('ed-class-activenotebook')
        else:
            self.get_widget().remove_css_class('ed-class-activenotebook')

    def get_page_of_tab(self, tab):
        """Get page of tab."""
        for p, t in self.tabs.items():
            if t is tab:
                return p
        return None

    def show_page_of_tab(self, tab):
        """Once a tab was made active, show its page content."""
        self.select_page(self.get_page_of_tab(tab))

    def connect(self, sig, cbk, *usrdata):
        """Register signal handler."""
        assert callable(cbk)
        if sig in self.signals:
            self.signals[sig].append((cbk, usrdata))
            return self
        return None

    def emit(self, sig, *data):
        """Emit signal, calling its registered handlers."""
        if sig in self.signals:
            for cbk, usrdata in self.signals[sig]:
                cbk(self, *data, *usrdata)

    def add_tab_action(self, page, name, gvar, cbk, *user_data):
        """Add tab menu action."""
        assert callable(cbk)
        if page in self.tabs:
            action = Gio.SimpleAction.new(name, None if gvar is None else GLib.VariantType(gvar))
            action.connect('activate', self.on_tab_action, cbk, *user_data)
            self.tabs[page].agroup.add_action(action)

    def add_tab_menuitem(self, page, label, detailed_action):
        """Add tab menu item."""
        if page in self.tabs:
            self.tabs[page].popup.get_menu_model().append(label, detailed_action)

    def select_page(self, page):
        """Activate page and show its content."""
        if self.get_selected_page() is page:
            return
        if page in self.tabs:
            for p, t in self.tabs.items():
                if p is page:
                    self.pages_container.set_visible_child(p)
                    if t is not None:   # placeholder page has no tab
                        self.header.activate_tab(t)
                        self.history.set(t)
                        self.emit('switch-page', p)

    def select_prev_page(self):
        """Activate previous page by position, loop if first one."""
        self.header.tab_prev()

    def select_next_page(self):
        """Activate next page by position, loop if last one."""
        self.header.tab_next()

    def select_prev_visited_page(self):
        """Activate previous visited page."""
        if tab := self.history.prev():
            self.show_page_of_tab(tab)

    def select_next_visited_page(self):
        """Activate next visited page."""
        if tab := self.history.next():
            self.show_page_of_tab(tab)

    def get_selected_page(self):
        """Get active page widget."""
        if tab := self.header.get_current_tab():
            if page := self.get_page_of_tab(tab):
                return page
        return None

    def get_tab_title(self, page):
        """Get tab title of page."""
        if page in self.tabs:
            return self.tabs[page].get_title()
        return None

    def set_tab_title(self, page, title, modified=False, /):
        """Set tab title of page."""
        if page in self.tabs:
            self.tabs[page].set_title(title or "???", modified)

    def set_tab_tooltip(self, page, tooltip):
        """Set tab tooltip of page."""
        if page in self.tabs:
            self.tabs[page].set_tooltip_text(tooltip)

    def move_tab(self, pos, page):
        """Move the page's tab to position in header."""
        if page in self.tabs:
            tab = self.tabs[page]
            new_pos = self.header.get_ordered_tabs()[pos]
            if pos >= 0:
                new_pos = new_pos.get_prev_sibling()
            self.header.tabs_box.reorder_child_after(tab, new_pos)
            if self.header.get_current_tab() is tab:
                GLib.idle_add(self.header.scroll_to, tab)

    def add_page(self, page, title):
        """Add page to notebook."""
        if page not in self.tabs:
            self.pages_container.add_child(page)
            self.tabs[page] = tab = self.header.add_tab(title or _("Untitled"))
            tab.update_relation([Gtk.AccessibleRelation.CONTROLS], [Gtk.AccessibleList.new_from_list([page])])
            self.history.add(tab)
            self.emit('page-added', page)
            if self.get_n_pages() == 1:
                self.select_page(page)

    def detach_page(self, page):
        """Detach page from notebook."""
        if page in self.tabs and self.tabs[page] is not None:
            fallback = self.history.remove(self.tabs[page])
            self.header.del_tab(self.tabs[page])
            del self.tabs[page]
            self.pages_container.remove(page)
            self.emit('page-removed')
            self.show_page_of_tab(fallback)

    def request_close_tab(self, tab):
        """Forward tab closing request to application."""
        if page := self.get_page_of_tab(tab):
            self.wbench.close_doc_request(page)

    def action_selecttab(self, action, param):
        """Action handler for notebook menu select tab."""
        if param is not None:
            i = param.unpack()
            self.select_page(self.get_children()[i])
            self.wbench.select_notebook(self)

    def on_create_menu(self, menubutton):
        """Callback on menubutton request to create its menu."""
        menu = Gio.Menu()
        for i, page in enumerate(self.get_children()):
            title = self.get_tab_title(page).replace("_", "__")   # escape mnemonics
            menu.append(title, f'notebook.selecttab({i})')
        menu.freeze()
        pop = Gtk.PopoverMenu(menu_model=menu, has_arrow=False, halign='end')
        menubutton.set_popover(pop)

    def on_tab_action(self, action, param, cbk, *user_data):
        """Callback on tab menu action."""
        args = (() if param is None else (param.unpack(),)) + user_data
        cbk(*args)
        self.wbench.focus_active_doc()

    def on_event_dropmotion(self, event, x, y):
        """Callback on drop motion event."""
        self.header.on_event_dragupdate(event.get_widget(), x, y)
        if gvalue := event.get_value():
            if self.wbench is gvalue.notebook.wbench:
                return Gdk.DragAction.MOVE
            return Gdk.DragAction.COPY
        return 0

    def on_event_drop(self, event, gvalue, x, y):
        """Callback on drop event."""
        if isinstance(gvalue, EdNotebookDragContent):
            nb, tab = gvalue.notebook, gvalue.tab
            if nb is not self:
                page = nb.get_page_of_tab(tab)
                return self.wbench.move_page(nb, self, page)
        return False

    # Gtk.Notebook APIs --------------------------------------------------------

    def get_children(self):
        """Get list of notebook pages, in their tab's order (placeholder page not included)."""
        return [self.get_page_of_tab(_t) for _t in self.header.get_ordered_tabs()]

    def get_n_pages(self):
        """Get number of pages in notebook (placeholder page not included)."""
        return len(self.get_children())
