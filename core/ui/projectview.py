#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Project tree browser."""

from gi.repository import GLib, Gio, Gtk

from .contextmenu import EdTreeMenu
from ..utils.filename import EdFileName
from ..utils.misc import ed_copy_file_location, ed_open_extern
from ..utils.pathmatch import EdPathMatch


__all__ = ['EdProjectView']


class EdProjectView():
    """Project tree viewer."""

    def __init__(self, wbench, tv, spinner):
        self.wbench = wbench
        self.treeview = tv
        self.spinner = spinner
        self.spin_requests = 0
        if self.treeview.get_model() is None:
            # [basename, icon, fullpath, GFile, isFile]
            treestore = Gtk.TreeStore(str, str, str, Gio.File, bool)
            self.treeview.set_model(treestore)
            self.setup_treeview()
        # Popup menu
        self.wbench.menumanager.add_action('project.hide', None, self.on_menu_hide)
        self.wbench.menumanager.add_action('project.copy', 's', self.on_menu_copy)
        self.wbench.menumanager.add_action('project.search', None, self.on_menu_search)
        self.wbench.menumanager.add_action('project.openext', 'b', self.on_menu_openext)
        self.wbench.menumanager.add_action('project.openwin', None, self.on_menu_openwin)
        menu = EdTreeMenu(self.wbench.menumanager, self.treeview)
        menu.add_item(_("_Hide"), 'project.hide')
        menu.add_item(_("Copy File _Name"), 'project.copy::name')
        menu.add_item(_("Copy File _Path"), 'project.copy::path')
        menu.add_item(_("Copy File _Raw Path"), 'project.copy::raw')
        menu.add_item(_("Copy File _URI"), 'project.copy::uri')
        menu.add_item(_("_Search in This Folder..."), 'project.search')
        menu.add_item(_("Open With _Other App..."), 'project.openext(true)')
        menu.add_item(_("Open With _Default App"), 'project.openext(false)')
        menu.add_item(_("Open in New _Window"), 'project.openwin')
        # Events
        self.treeview.connect('row-activated', self.on_row_activated)

    def free(self):
        """Release resources and break circular references."""
        self.treeview.disconnect_by_func(self.on_row_activated)
        self.reset()
        self.wbench = None

    def setup_treeview(self):
        """Setup project browser treeview."""
        c = Gtk.TreeViewColumn()
        c.set_expand(True)
        c.set_spacing(6)
        # Column cell: icon
        cr = Gtk.CellRendererPixbuf()
        c.pack_start(cr, False)
        c.add_attribute(cr, 'icon_name', 1)
        # Column cell: file name
        cr = Gtk.CellRendererText()
        c.pack_start(cr, True)
        c.add_attribute(cr, 'text', 0)
        # Add column
        self.treeview.append_column(c)
        self.treeview.set_tooltip_column(2)

    def reset(self):
        """Reset view."""
        self.treeview.get_model().clear()

    def reveal_in_tree(self, gfile):
        """Show file in browser treeview."""
        self.treeview.get_selection().unselect_all()
        treestore = self.treeview.get_model()
        itr = treestore.get_iter_first()
        while itr is not None:
            gf = treestore.get_value(itr, 3)
            if gf is None:
                itr = treestore.iter_next(itr)
            elif gfile.equal(gf):
                path = treestore.get_path(itr)
                self.treeview.expand_to_path(path)
                self.treeview.get_selection().select_path(path)
                self.treeview.scroll_to_cell(path, None, True, 0.5, 0.0)
                break
            elif gfile.has_prefix(gf):
                itr = treestore.iter_children(itr)
            else:
                itr = treestore.iter_next(itr)

    def foreach_treemodel_add_gfile(self, model, path, itr, gfiles):
        """Foreach callback over GtkTreeModel, add gfile to list."""
        if gf := model.get_value(itr, 3):
            if model.get_value(itr, 4):   # regular file
                gfiles.append(gf)
        return False

    def get_gfiles(self):
        """Get list of project files (no reload)."""
        gfiles = []
        self.treeview.get_model().foreach(self.foreach_treemodel_add_gfile, gfiles)
        return gfiles

    def update_filetree(self, dirs, files):
        """Load files tree into project browser treestore."""
        ts = self.treeview.get_model()
        assert ts.get_flags() & Gtk.TreeModelFlags.ITERS_PERSIST
        self.merge_treestore(ts, None, dirs, files)

    def merge_treestore(self, ts, parent, dirs, files):
        """Recursively merge subdirectories into project browser treestore."""
        itr = ts.iter_children(parent)
        # Update folders
        for bn, gf, d, f in sorted(dirs):
            row = [bn, 'ed-folder-symbolic', None, gf, False]
            while True:   # lookup insertion point in treestore
                if itr is None:
                    itr = ts.append(parent, row)
                else:
                    tn = EdFileName.new(ts.get_value(itr, 0))   # basename
                    if ts.get_value(itr, 4) or bn < tn:
                        itr = ts.insert_before(parent, itr, row)
                    elif bn > tn:
                        itr_old, itr = itr, ts.iter_next(itr)
                        ts.remove(itr_old)
                        continue
                break
            self.merge_treestore(ts, itr, d, f)
            itr = ts.iter_next(itr)
        # Update files
        for bn, gf, tip in sorted(files):
            row = [bn, 'ed-file-symbolic', tip, gf, True]
            while True:   # lookup insertion point in treestore
                if itr is None:
                    itr = ts.append(parent, row)
                else:
                    tn = EdFileName.new(ts.get_value(itr, 0))
                    if not ts.get_value(itr, 4) or bn > tn:
                        itr_old, itr = itr, ts.iter_next(itr)
                        ts.remove(itr_old)
                        continue
                    elif bn < tn:
                        itr = ts.insert_before(parent, itr, row)
                break
            itr = ts.iter_next(itr)
        # Cleanup treestore leftovers
        while itr is not None:
            itr_old, itr = itr, ts.iter_next(itr)
            ts.remove(itr_old)

    def spin_start(self):
        """Start loading indication spinner."""
        if self.spin_requests == 0:
            self.spinner.set_visible(True)
            self.spinner.start()
        self.spin_requests += 1

    def spin_stop(self):
        """Stop loading indication spinner."""
        if self.spin_requests > 0:
            self.spin_requests -= 1
        if self.spin_requests == 0:
            self.spinner.stop()
            self.spinner.set_visible(False)

    def on_menu_hide(self, itr):
        """Callback on menu action hide."""
        self.treeview.get_model().remove(itr)
        GLib.idle_add(self.wbench.on_project_loaded)

    def on_menu_copy(self, itr, part):
        """Callback on menu action copy."""
        if gfile := self.treeview.get_model().get_value(itr, 3):
            ed_copy_file_location(gfile, part)

    def on_menu_search(self, itr):
        """Callback on menu action search."""
        model = self.treeview.get_model()
        if gfile := model.get_value(itr, 3):
            if model.get_value(itr, 4):   # regular file
                gfile = gfile.get_parent()
            relpath = self.wbench.get_prj_relpath(gfile)
            pattern = EdPathMatch.escape(relpath) + "/**" if relpath else ""
            self.wbench.set_search_settings(fltr=pattern)
            self.wbench.sidebar.show_page('search')

    def on_menu_openext(self, itr, ask):
        """Callback on menu action openext."""
        if gfile := self.treeview.get_model().get_value(itr, 3):
            ed_open_extern(gfile, ask, self.wbench)

    def on_menu_openwin(self, itr):
        """Callback on menu action openwin."""
        if gfile := self.treeview.get_model().get_value(itr, 3):
            self.wbench.request_open([gfile], "newwin=true")

    def on_row_activated(self, tv, path, column):
        """Callback on treeview row activation."""
        treestore = tv.get_model()
        itr = treestore.get_iter(path)
        if itr is not None:
            if treestore.iter_has_child(itr):
                if tv.row_expanded(path):
                    tv.collapse_row(path)
                else:
                    tv.expand_to_path(path)
                tv.get_selection().unselect_all()
            elif (gfile := treestore.get_value(itr, 3)) and treestore.get_value(itr, 4):
                self.wbench.request_open([gfile], "")   # open selected file
            else:
                tv.get_selection().unselect_all()
