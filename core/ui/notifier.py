#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Manage notifications."""

import contextlib

from gi.repository import GLib, Gtk


__all__ = ['EdCoreNotifier', 'EdToastBoxNotifier', 'EdToastRevealNotifier']


class EdCoreNotifier():
    """Core notifications manager."""

    def __init__(self, stsbar, logslist):
        self.display_notif = None
        self.statusbar = stsbar
        self.logslist = logslist

    def setup_logview(self, listview):
        """Bind the listview with the logs listmodel."""
        factory = Gtk.SignalListItemFactory()
        factory.connect('setup', lambda _l, _i: _i.set_child(Gtk.Inscription(margin_start=6)))
        factory.connect('bind', lambda _l, _i: _i.get_child().set_text(_i.get_item().get_string()))
        listview.get_vadjustment().connect('changed', lambda _a: GLib.idle_add(_a.set_value, _a.get_upper()))
        listview.set_factory(factory)

    def log(self, *params):
        """Log notifications."""
        *ctx, msg = params
        ed_log(*params)
        self.logslist.append("".join(f"[{_c}]" for _c in ctx) + " " + msg)

    def status(self, message, important=True, /):
        """Push notification in statusbar."""
        if important:
            self.log("status", message)
        self.statusbar.set_text(message)

    def question(self, message, secondary, act_label, cbk, *user_data):
        """Raise a notification with question."""
        assert callable(cbk)
        b = self.push_notif(Gtk.MessageType.QUESTION, 'dialog-question-symbolic', message, secondary, cbk, *user_data)
        if act_label:
            b.add_button(act_label, Gtk.ResponseType.ACCEPT)

    def info(self, message, secondary):
        """Raise a notification as information."""
        self.push_notif(Gtk.MessageType.INFO, 'dialog-information-symbolic', message, secondary)

    def warn(self, message, secondary):
        """Raise a notification as warning."""
        self.push_notif(Gtk.MessageType.WARNING, 'dialog-warning-symbolic', message, secondary)

    def error(self, message, secondary):
        """Raise a notification as error."""
        self.push_notif(Gtk.MessageType.ERROR, 'dialog-error-symbolic', message, secondary)

    def push_notif(self, mtyp, icon_name, message, secondary, cbk=None, /, *user_data):
        """Push a notification in container."""
        assert callable(self.display_notif)   # use through an EdDispNotifier wrapper
        self.log("notify", mtyp.value_nick, message)
        banner = EdInfoBanner()
        banner.set_message_type(mtyp)
        banner.set_show_close_button(True)
        if icon_name:
            icon = Gtk.Image(icon_name=icon_name, pixel_size=24, accessible_role='presentation')
            banner.add_widget(icon)
        markup = self.format_markup(message, secondary)
        msg_label = Gtk.Label(label=markup, use_markup=True, wrap=True, wrap_mode='word-char', xalign=0, hexpand=True)
        banner.add_widget(msg_label)
        banner.connect_response(self.on_notif_response, cbk, *user_data)
        self.display_notif(banner, msg_label)
        self.display_notif = None
        return banner

    def format_markup(self, message, secondary):
        """Reformat message with markup."""
        markup = f'<b>{GLib.markup_escape_text(message)}</b>'
        if secondary:
            markup += f'\n<small>{GLib.markup_escape_text(secondary)}</small>'
        return markup

    def on_notif_response(self, banner, resp, cbk, *user_data):
        """Callback on infobar response."""
        banner.free()
        banner.get_parent().remove(banner)
        if callable(cbk):
            cbk(resp == Gtk.ResponseType.ACCEPT, *user_data)


class EdDispNotifier():
    """Abstract wrapper class to extend notifier with a display frontend."""

    def __init__(self, core):
        self.core = core
        self.category = None

    def __getattr__(self, attr):   # delegates
        self.core.display_notif = self.do_display_notif
        return getattr(self.core, attr)

    @contextlib.contextmanager
    def set_category(self, category):
        """Context for setting a notification category."""
        try:
            self.category = category
            yield None   # return of __enter__, will block until __exit__
        finally:
            self.category = None

    def do_display_notif(self, banner, msg_label):   # virtual
        """Display the notification."""
        banner.free()
        raise NotImplementedError   # abstract class, no display frontend


class EdToastBoxNotifier(EdDispNotifier):
    """Extend notifier with a GtkBox toast display frontend."""

    def __init__(self, core, box):
        super().__init__(core)
        self.box = box

    def free(self):
        """Release resources and break circular references."""
        for banner in list(self.box):
            banner.free()

    def clear_category(self, category):
        """Clear notifications matching the category."""
        for banner in list(self.box):
            if banner.is_category(category):
                banner.free()
                self.box.remove(banner)

    def do_display_notif(self, banner, msg_label):   # override
        """Display the notification."""
        self.clear_category(self.category)
        banner.set_category(self.category)
        self.box.append(banner)


class EdToastRevealNotifier(EdToastBoxNotifier):
    """Extend notifier with a GtkRevealer+GtkBox toast display frontend."""

    def __init__(self, core, rev, box):
        super().__init__(core, box)
        self.revealer = rev
        self.box_listmodel = self.box.observe_children()
        self.box_listmodel.connect('items-changed', self.on_notiflist_changed)

    def free(self):
        """Release resources and break circular references."""
        self.box_listmodel.disconnect_by_func(self.on_notiflist_changed)
        super().free()

    def do_display_notif(self, banner, msg_label):   # override
        """Display the notification."""
        msg_label.set_max_width_chars(50)
        super().do_display_notif(banner, msg_label)
        self.revealer.set_reveal_child(True)

    def on_notiflist_changed(self, listmodel, pos, removed, added):
        """Callback on notifications list changed."""
        if listmodel.get_n_items() == 0:   # empty notifs list
            self.revealer.set_reveal_child(False)


class EdInfoBanner(Gtk.Box):
    """Embedded notification widget."""
    __gtype_name__ = __qualname__

    def __init__(self):
        super().__init__(orientation='horizontal', spacing=12, accessible_role='alert')
        self.add_css_class('ed-class-infobar')
        self.category = None
        self.resp_callback = None
        # Content
        self.childsbox = Gtk.Box(orientation='horizontal', spacing=12, hexpand=True)
        self.buttonbox = Gtk.Box(orientation='horizontal', valign='center')
        self.buttonbox.add_css_class('linked')
        self.bclose = Gtk.Button(icon_name='window-close-symbolic', focus_on_click=False, valign='center')
        self.bclose.set_tooltip_text(_("Dismiss"))
        self.bclose.connect('clicked', self.on_clicked, Gtk.ResponseType.CLOSE)
        self.append(self.childsbox)
        self.append(self.buttonbox)
        self.append(self.bclose)

    def free(self):
        """Release resources and break circular references."""
        for b in self.buttonbox:
            b.disconnect_by_func(self.on_clicked)
        self.bclose.disconnect_by_func(self.on_clicked)
        self.resp_callback = None

    def set_message_type(self, mtyp):
        """Set type of message."""
        assert isinstance(mtyp, Gtk.MessageType)
        self.add_css_class(mtyp.value_nick)

    def set_show_close_button(self, visible):
        """Set close button visibility."""
        self.bclose.set_visible(visible)

    def set_category(self, category):
        """Set category."""
        self.category = category

    def is_category(self, category):
        """Check category."""
        return category is not None and self.category == category

    def add_widget(self, widget):
        """Add child widget to infobar."""
        self.childsbox.append(widget)

    def add_button(self, act_label, resp_id):
        """Add button to infobar."""
        b = Gtk.Button(label=act_label, focus_on_click=False)
        b.connect('clicked', self.on_clicked, resp_id)
        self.buttonbox.append(b)

    def connect_response(self, cbk, *user_data):
        """Set response callback."""
        assert callable(cbk)
        self.resp_callback = (cbk, user_data)

    def on_clicked(self, button, resp_id):
        """Callback on response buttons click."""
        self.set_visible(False)
        if self.resp_callback:
            cbk, user_data = self.resp_callback
            self.resp_callback = None
            cbk(self, resp_id, *user_data)
