#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Manage command entry to run custom actions."""

from gi.repository import Gtk

from ...exts import ed_extensions_get_data


__all__ = ['EdCommandLauncher']


class EdCommandLauncher():
    """Commands launcher."""

    def __init__(self, wbench, entry):
        self.wbench = wbench
        self.model = Gtk.ListStore(str, str, str)   # [command, description, casefold(command)]
        self.entry = entry
        cmpl = entry.get_completion()
        cmpl.set_model(self.model)
        cmpl.set_text_column(0)
        cmpl.set_match_func(self.match_func)
        # Extend completion with description
        cr = Gtk.CellRendererText()
        cr.set_alignment(1.0, 0.5)
        cr.set_sensitive(False)
        cmpl.pack_end(cr, False)
        cmpl.add_attribute(cr, 'text', 1)
        # Rendering speedup
        for cell in cmpl.get_cells():
            cell.set_fixed_height_from_font(1)
        # Collect commands from extensions
        self.commands = {}   # name: (cbk, descr, menu, [predef_params])
        for cmdlist in ed_extensions_get_data('commands', self.wbench.app):
            self.commands.update(cmdlist)
        self.update_completion()
        # Events
        self.entry.connect('icon-press', lambda _e, _p: _e.activate_action('win.runcmd', None))

    def free(self):
        """Release resources and break circular references."""
        self.model.clear()
        self.wbench = None

    def update_completion(self):
        """Rebuild command completion list."""
        self.model.clear()
        for cmd in sorted(self.commands):
            self.model.append([cmd, self.commands[cmd][1], cmd.casefold()])
            for p in self.commands[cmd][3]:
                subcmd = f"{cmd} {p}"
                self.model.append([subcmd, None, subcmd.casefold()])

    def match_func(self, w, key, itr):
        """Match function to filter completion list with user input."""
        # Note: 'key' is already casefolded
        cmd = self.model.get_value(itr, 2)
        if (s := key.find(" ") + 1) > 0:
            return cmd.startswith(key[:s]) and key[s:] in cmd[s:]
        elif " " in cmd:
            return False
        return key in cmd

    def set_cmd_params(self, **cmd_params):
        """Set command parameters."""
        for cmd, params in cmd_params.items():
            if cmd in self.commands:
                self.commands[cmd][3].clear()
                self.commands[cmd][3].extend(params)
        self.update_completion()

    def run(self, cmd="", /, *param):
        """Run command with parameters."""
        if not cmd:   # cancel
            return True
        handler = self.commands.get(cmd, (None, None, None, []))[0]
        res = False
        if callable(handler):
            self.wbench.notifier.status(_("Run command '{}'").format(cmd))
            w, doc = self.wbench.get_active_doc()
            try:
                res = handler(self.wbench, doc, *param)
            except Exception as e:
                res = False
                self.wbench.notifier.error(_("Command '{}' error!").format(cmd), f"{e}")
            if not res:
                self.wbench.notifier.status(_("Failed to run command '{}'").format(cmd))
        else:
            self.wbench.notifier.status(_("Unknown command '{}'").format(cmd))
        return res
