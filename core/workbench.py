#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Application window."""

from gi.repository import GObject, GLib, Gio, Gtk

from .actions import EdActionsManager
from .config import EdWbenchConfig, ed_get_subconfig
from .docs import ed_docs_get_classes
from .project import EdProject
from .search import EdMultiSearch
from .ui.commands import EdCommandLauncher
from .ui.contextmenu import EdContextMenuManager
from .ui.dialogs import EdFileChooser, EdMessage
from .ui.marksview import EdMarksView
from .ui.notebook import EdNotebook
from .ui.notifier import EdCoreNotifier, EdToastRevealNotifier
from .ui.projectview import EdProjectView
from .ui.sidebarswitcher import EdSidebarSwitcher
from .ui.symbolsview import EdSymbolsView
from .utils.history import ed_lru_pop, ed_lru_push
from .utils.misc import ed_get_gfile_display_path, ed_get_gfile_display_name, ed_wrap_path
from .utils.misc import ed_get_datapath, ed_open_extern, ed_set_transient_style
from .utils.thread import ed_new_threadevent
from .utils.wrapper import EdBuilderWrapper
from ..exts import ed_extensions_notify


__all__ = ['EdWorkbench']


class EdWorkbench():
    """Window instance, optionally attached to a project."""

    def __init__(self, app):
        self.app = app
        self.project = None
        self.notebooks = []
        self.docs = {}   # widget: document
        self.saved_contexts = {}   # uri: dict(hints)
        self.last_closed = []
        self.last_focused = None
        self.stopthread = ed_new_threadevent()
        self.cancellable = Gio.Cancellable.new()
        self.time_find = 0
        self.added_pages = []
        self.single_notebook = False
        self.ext_data = {}
        # GUI
        self.builderwrapper = EdBuilderWrapper(self)
        self.builder = Gtk.Builder(self.builderwrapper)
        self.builder.add_from_file(ed_get_datapath("main.ui"))
        self.win = self.get_ui('ed_win')
        self.win.set_application(self.app)
        self.actions = EdActionsManager(self)
        notif_core = EdCoreNotifier(*self.get_ui('ed_statusbar_notif', 'ed_list_logs'))
        notif_core.setup_logview(self.get_ui('ed_listview_logs'))
        self.notifier = EdToastRevealNotifier(notif_core, *self.get_ui('ed_revealer_notif', 'ed_box_notif'))
        self.config = EdWbenchConfig(self)
        match self.app.get_setting('core', 'menu', str, 'bar'):
            case 'bar':
                self.win.set_show_menubar(True)
            case 'button':
                b = self.get_ui('ed_button_mainmenu')
                b.get_popover().set_menu_model(self.app.get_menubar())
                b.set_properties(primary=True, visible=True)
        if pos := self.app.get_setting('core', 'sidebar_size', int, 0):
            self.get_ui('ed_paned_sidebar').set_position(pos)
        if self.app.get_setting('core', 'maximize', bool, False):
            self.win.maximize()
        # Left pane
        self.menumanager = EdContextMenuManager(self, self.get_ui('ed_box_sidebar'))
        self.sidebar = EdSidebarSwitcher(*self.get_ui('ed_box_sidebar', 'ed_stack_sidebar', 'ed_box_sidebarswitcher'))
        self.sidebar.set_visibility(False)
        # Right pane
        self.active_notebook = self.add_notebook(0, 0, 1, 1)
        self.active_notebook.set_active(True)
        # Bottom pane
        self.get_ui('ed_stack_bottom').get_pages().connect('items-changed', self.on_bottomstack_changed)
        # Misc
        self.multisearch = EdMultiSearch(self, *self.get_ui('ed_tree_search', 'ed_entry_find'))
        self.projectview = EdProjectView(self, *self.get_ui('ed_tree_project', 'ed_spinner_project'))
        self.symbolsview = EdSymbolsView(self, self.get_ui('ed_tree_symbols'))
        self.marksview = EdMarksView(self, self.get_ui('ed_tree_marks'))
        self.cmdlauncher = EdCommandLauncher(self, self.get_ui('ed_entry_cmd'))
        self.update_modif_actions(None)
        self.win.set_title(GLib.get_application_name())
        ed_extensions_notify('wbench_opened', self)
        if ED_DEBUG > 1:
            self.win.weak_ref(ed_log, "memory", "finalize workbench window")

    if ED_DEBUG > 1:
        def __del__(self):
            ed_log("memory", "del workbench")

    def free(self):
        """Release resources and break circular references."""
        self.active_notebook = None
        self.last_focused = None
        self.get_ui('ed_stack_bottom').get_pages().disconnect_by_func(self.on_bottomstack_changed)
        for attr, obj in self.__dict__.items():
            if obj.__class__.__name__.startswith('Ed'):
                if hasattr(obj, 'free'):
                    obj.free()
                setattr(self, attr, None)
        for nb in self.notebooks:
            nb.free()
        self.docs.clear()
        self.ext_data.clear()
        self.notebooks.clear()
        self.builder = None
        self.win = None

    def get_ui(self, *elems):
        """Get UI elements by name."""
        if len(elems) == 1:
            return self.builder.get_object(*elems)
        return map(self.builder.get_object, elems)

    def bind(self, elem1, prop1, elem2, prop2):
        """Bind UI elements properties."""
        flags = GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE
        obj1 = self.get_ui(elem1)
        obj2 = self.get_ui(elem2)
        obj1.bind_property(prop1, obj2, prop2, flags)

    def show_window(self):
        """Make window visible for user."""
        if self.win.get_visible():
            self.win.present()
        else:
            self.win.set_visible(True)

    def set_window_subtitle(self, txt):
        """Set the workbench window subtitle."""
        subtitle = self.get_ui('ed_label_subtitle')
        if txt:
            subtitle.set_text(txt)
            subtitle.set_visible(True)
        else:
            subtitle.set_visible(False)

    def add_notebook(self, x, y, w, h):
        """Add new notebook to workbench."""
        self.single_notebook = False
        self.update_single_notebook()
        nb = EdNotebook(self)
        nb.connect('page-added', self.on_page_add)
        nb.connect('page-removed', self.on_page_remove)
        nb.connect('switch-page', self.on_page_switch)
        self.get_ui('ed_grid_splitview').attach(nb.get_widget(), x, y, w, h)
        self.notebooks.append(nb)
        ed_extensions_notify('notebook_added', self, nb)
        return nb

    def get_cancellable(self):
        """Get an initialized GCancellable object, for setting up cancellable operations."""
        if self.cancellable.is_cancelled():
            del self.cancellable
            self.cancellable = Gio.Cancellable.new()
        return self.cancellable

    def set_config(self, key, val):
        """Set config parameter."""
        return self.config.set_intern_config(key, val)

    def get_config(self, key, typ, default=None, /):
        """Get config parameter."""
        default = ed_get_subconfig(self.config.usercfg, key, typ, default)
        if self.project is not None:
            default = ed_get_subconfig(self.project.config, key, typ, default)
        return ed_get_subconfig(self.config.interncfg, key, typ, default)

    def filter_config(self, *configs):
        """Get filtered list of config parameters."""
        for key, typ, *default in configs:
            yield key, self.get_config(key, typ, *default)

    def is_empty(self):
        """Check if workbench is empty."""
        return self.project is None and not self.docs

    def is_quitting(self):
        """Check if workbench is quitting."""
        return self.win is None or self.win.in_destruction()

    def request_open(self, gfiles, hint, **ext_hints):
        """Request application to open files."""
        GLib.idle_add(self.app.on_open, self.app, gfiles, len(gfiles), hint, ext_hints)

    def open_dir(self, gfile, **hints):
        """Open project from directory."""
        if self.project is not None:
            self.project.free()
            del self.project
        ed_get_gfile_display_path(gfile)   # cache portal host path
        self.project = EdProject(self, gfile, self.projectview)
        self.project.reload()
        self.sidebar.show_page('project')

    def open_file(self, gfile, **hints):
        """Open file."""
        ed_lru_pop(self.last_closed, gfile.get_uri())
        for d in self.docs.values():
            tf = d.get_gfile()
            if tf is not None and gfile.equal(tf):   # file already open, switch to doc
                if not self.added_pages:
                    self.select_doc(d)
                if 'goto' in hints:
                    d.go_to(hints['goto'])
                return
        ctx = self.saved_contexts.get(gfile.get_uri(), {}) | hints
        if self.add_doc(gfile, **ctx) is None:   # open in new tab
            msg = _("File doesn't seem to contain text. Open externally?")
            sec = ed_get_gfile_display_path(gfile)
            self.notifier.question(msg, sec, _("Open Ext."), lambda _a: _a and ed_open_extern(gfile, True, self))

    def add_doc(self, gfile, **hints):
        """Add document to notebook."""
        cls_hint = hints.get('docclass', None)
        for cls_doc in ed_docs_get_classes():
            if cls_hint == cls_doc.__name__ if cls_hint else cls_doc.is_gfile_supported(gfile):
                d = cls_doc(self, gfile, **hints)
                self.docs[d.get_widget()] = d
                d.add_to_notebook(self.active_notebook)
                ed_extensions_notify('doc_added', self, d)
                self.notifier.status(_("Add document '{}'").format(d.get_doc_fullname()))
                return d
        return None

    def save_doc(self, doc, close=False, /):
        """Save document content on disk."""
        if not doc.is_modified():
            if close:
                self.close_doc(doc)
        elif doc.get_gfile() is None:   # no target, ask where to save
            self.save_doc_as(doc, close)
        else:
            doc.save(close)

    def save_doc_as(self, doc, close=False, /, **kwargs):
        """Save document content on disk, ask where."""
        for d in self.docs.values():
            if d is doc:
                self.select_doc(d)
        location = dict(folder=self.get_prj_gpath(), gfile=doc.get_gfile()) | kwargs
        fcs = EdFileChooser(self.win, _("Save File"))
        fcs.set_response_handler(lambda _fc: doc.save_as(_fc.get_file(), close))
        fcs.show_save(**location)

    def close_doc_request(self, w):
        """Request to close document."""
        d = self.docs[w]
        if d.is_modified():
            self.select_doc(d)
            m = EdMessage(self.win, ('+yes', '-no', 'cancel'),
                          _("Save changes before closing?"),
                          ed_wrap_path(d.get_doc_fullname(), 50))
            m.add_response_handler(Gtk.ResponseType.YES, self.save_doc, d, True)
            m.add_response_handler(Gtk.ResponseType.NO, self.close_doc, d)
            m.show_message()
        else:
            self.close_doc(d)

    def close_doc(self, d):
        """Close document."""
        self.notifier.status(_("Close '{}'").format(d.tablabel))
        ed_extensions_notify('doc_closed', self, d)
        w = d.get_widget()
        if gf := d.get_gfile():
            uri = gf.get_uri()
            self.saved_contexts[uri] = d.get_context()
            ed_lru_push(self.last_closed, uri)
        d.remove_from_notebook()
        d.free()
        del self.docs[w]

    def close_win(self):
        """Close window."""
        ed_extensions_notify('wbench_closed', self)
        self.on_processing_stop()
        for d in self.docs.values():
            d.free()

    def get_active_doc(self):
        """Get active notebook document (widget and EdBaseDocument instance)."""
        if not self.is_quitting():
            if w := self.active_notebook.get_selected_page():
                return (w, self.docs[w])
        return (None, None)

    def get_docs_sorted(self):
        """Get sorted list of notebook docs."""
        for nb in self.notebooks:
            for w in nb.get_children():
                yield self.docs[w]

    def get_prj_gpath(self):
        """Get the project gpath if available, or None."""
        if self.project is not None:
            return self.project.get_gpath()
        return None

    def get_prj_relpath(self, gfile):
        """Get the file's path relative to the project if available, or None."""
        if self.project is not None:
            return self.project.get_gpath().get_relative_path(gfile)
        return None

    def get_path_context(self):
        """Get the path used to resolve local settings when running external commands."""
        gfroot = self.get_prj_gpath()
        prj_path = None if gfroot is None else gfroot.peek_path()
        return prj_path or GLib.get_home_dir()

    def set_search_settings(self, **settings):
        """Set user search settings in GUI."""
        if 'find' in settings:
            self.get_ui('ed_entry_find').set_text(settings['find'])
        if 'repl' in settings:
            self.get_ui('ed_entry_replace').set_text(settings['repl'])
        if 'wb' in settings:
            self.get_ui('ed_toggle_findwordbounds').set_active(settings['wb'])
        if 'cs' in settings:
            self.get_ui('ed_toggle_findcasesensitive').set_active(settings['cs'])
        if 'rx' in settings:
            self.get_ui('ed_toggle_findregex').set_active(settings['rx'])
        if 'es' in settings:
            self.get_ui('ed_toggle_findescape').set_active(settings['es'])
        if 'fltr' in settings:
            self.get_ui('ed_toggle_findadvanced').set_active(True)
            self.get_ui('ed_entry_filter').set_text(settings['fltr'])

    def get_search_settings(self, replace):
        """Get user search settings from GUI."""
        settings = {
            'find': self.get_ui('ed_entry_find').get_text(),
            'repl': self.get_ui('ed_entry_replace').get_text(),
            'wb':   self.get_ui('ed_toggle_findwordbounds').get_active(),
            'cs':   self.get_ui('ed_toggle_findcasesensitive').get_active(),
            'rx':   self.get_ui('ed_toggle_findregex').get_active(),
            'es':   self.get_ui('ed_toggle_findescape').get_active(),
            'fltr': self.get_ui('ed_entry_filter').get_text(),
        }
        if settings['find']:
            self.add_combo_history(self.get_ui('ed_combo_find'), settings['find'])
        if replace and settings['repl']:
            self.add_combo_history(self.get_ui('ed_combo_replace'), settings['repl'])
        if self.get_ui('ed_toggle_findadvanced').get_active() and settings['fltr']:
            self.add_combo_history(self.get_ui('ed_combo_filter'), settings['fltr'])
        else:
            del settings['fltr']
        self.get_ui('ed_entry_find').set_position(-1)
        self.get_ui('ed_entry_replace').set_position(-1)
        self.get_ui('ed_entry_filter').set_position(-1)
        return settings

    def search_in_project(self):
        """Search in project or open files."""
        gfroot = self.get_prj_gpath()
        if self.project is not None:
            gfiles = self.projectview.get_gfiles()
        else:   # no project, search in open files
            gfiles = filter(None, (_d.get_gfile() for _d in self.get_docs_sorted()))
        self.multisearch.find(gfiles, gfroot, **self.get_search_settings(False))

    def notify_search_failure(self, error=None, /):
        """Notify user about search issue."""
        style_class = 'error' if error else 'warning'
        ed_set_transient_style(self.get_ui('ed_entry_find'), style_class)

    def add_combo_history(self, combo, text):
        """Add text to a GtkComboBoxText if not already there."""
        col = combo.get_entry_text_column()
        for row in combo.get_model():
            if row[col] == text:
                row.model.remove(row.iter)
                break
        combo.prepend_text(text)

    def update_modif_actions(self, doc):
        """Update undo/redo status."""
        w, d = self.get_active_doc()
        if d is doc:
            can_undo, can_redo = doc.get_modif_actions() if doc else (False, False)
            self.win.lookup_action('undo').set_enabled(can_undo)
            self.win.lookup_action('redo').set_enabled(can_redo)

    def update_symbols_for_doc(self, doc, **kwargs):
        """Update doc's symbols in side pane."""
        if self.is_quitting():
            return
        w, d = self.get_active_doc()
        if doc is None or d is doc:   # skip if doc is hidden
            syms = [] if d is None else d.get_symbols()
            sort = self.get_ui('ed_toggle_sortsyms').get_active()
            complete = self.symbolsview.update(syms, sort=sort, **kwargs)
            self.get_ui('ed_button_symsshowmore').set_visible(not complete)

    def update_doc_properties(self, doc):
        """Update doc's properties in interface."""
        w, d = self.get_active_doc()
        if d is doc:   # skip if doc is hidden
            fn = doc.get_doc_fullname()
            if doc.get_gfile() is not None:
                sep = "\\" if "\\" in fn else "/"
                if self.project:
                    pp = ed_get_gfile_display_path(self.get_prj_gpath())
                    if pp and fn.startswith(pp + sep):
                        fn = f".{fn[len(pp):]}"
                hp = GLib.get_home_dir()
                if fn.startswith(hp + sep):
                    fn = f"~{fn[len(hp):]}"
            self.set_window_subtitle(fn)
            self.update_modif_actions(doc)

    def focus_active_doc(self):
        """Give focus to active doc."""
        w, d = self.get_active_doc()
        if d is not None:
            d.focus()

    def select_doc(self, doc):
        """Activate document and show its content."""
        doc.notebook.select_page(doc.get_widget())
        doc.focus()

    def select_notebook(self, nb):
        """Activates notebook and its selected page."""
        if nb is not self.active_notebook:
            self.active_notebook.set_active(False)
            self.active_notebook = nb
            self.active_notebook.set_active(True)
            if page := nb.get_selected_page():
                GLib.idle_add(self.on_page_switch, nb, page)
        self.update_single_notebook()

    def update_single_notebook(self):
        """Apply the single notebook mode."""
        if self.single_notebook:
            self.get_ui('ed_button_splitsinglemode').set_icon_name('ed-grid-symbolic')
        else:
            self.get_ui('ed_button_splitsinglemode').set_icon_name('ed-stack-symbolic')
        for nb in self.notebooks:
            nb.get_widget().set_visible(nb is self.active_notebook or not self.single_notebook)

    def setup_tab_menu(self, nb, w):
        """Setup notebook tab context menu."""
        d = self.docs[w]
        nb.add_tab_action(w, 'move', 'i', nb.move_tab, w)
        nb.add_tab_action(w, 'copy', 's', d.copy_file_location)
        nb.add_tab_action(w, 'saveas', None, self.save_doc_as, d)
        nb.add_tab_action(w, 'close', None, self.close_doc_request, w)
        nb.add_tab_menuitem(w, _("Move to _Start"), 'tab.move(0)')
        nb.add_tab_menuitem(w, _("Move to _End"), 'tab.move(-1)')
        nb.add_tab_menuitem(w, _("Copy _Name"), 'tab.copy::name')
        nb.add_tab_menuitem(w, _("Copy _Path"), 'tab.copy::path')
        nb.add_tab_menuitem(w, _("Copy _Raw Path"), 'tab.copy::raw')
        nb.add_tab_menuitem(w, _("Copy _Folder"), 'tab.copy::dir')
        nb.add_tab_menuitem(w, _("Copy _URI"), 'tab.copy::uri')
        nb.add_tab_menuitem(w, _("Save _As..."), 'tab.saveas')
        nb.add_tab_menuitem(w, _("_Close"), 'tab.close')

    def move_page(self, nb_src, nb_dst, w):
        """Move page to another notebook."""
        d = nb_src.wbench.docs[w]
        if nb_src.wbench is nb_dst.wbench:
            d.remove_from_notebook()
            d.add_to_notebook(nb_dst)
            GLib.idle_add(d.focus, priority=GLib.PRIORITY_LOW)
        elif gf := d.get_gfile():
            nb_dst.wbench.open_file(gf, **d.get_context())
        else:
            return False
        return True

    def defer_select_page(self):
        """If only one single tab was added, select its notebook page."""
        if len(self.added_pages) == 1:
            nb, w = self.added_pages.pop()
            nb.select_page(w)
        self.added_pages.clear()

    def on_page_add(self, nb, w):
        """Callback on notebook page added."""
        self.setup_tab_menu(nb, w)
        if not self.added_pages:
            GLib.idle_add(self.defer_select_page, priority=GLib.PRIORITY_DEFAULT)
        self.added_pages.append((nb, w))

    def on_page_remove(self, nb):
        """Callback on notebook page removed."""
        if self.active_notebook.get_n_pages() == 0:   # last page closed
            self.set_window_subtitle(None)
            self.update_modif_actions(None)
            self.update_symbols_for_doc(None)
        GLib.idle_add(self.on_marks_reload)

    def on_page_switch(self, nb, w):
        """Callback on notebook page switched."""
        d = self.docs[w]
        if nb is self.active_notebook:
            self.update_doc_properties(d)
            self.update_symbols_for_doc(d)
        d.on_activate()
        ed_extensions_notify('doc_switched', self, d)
        GLib.idle_add(self.focus_active_doc, priority=GLib.PRIORITY_LOW)

    def on_drop(self, event, gvalue, x, y):
        """Callback on drop event."""
        if gfiles := gvalue.get_files():
            w = event.get_widget()
            for nb in self.notebooks:
                nbw = nb.get_widget()
                rel_x, rel_y = w.translate_coordinates(nbw, x, y)
                if nbw.contains(rel_x, rel_y):
                    self.select_notebook(nb)
            self.request_open(gfiles, "")
        return True

    def on_project_loaded(self):
        """Callback on project loaded."""
        ed_extensions_notify('project_loaded', self, self.project)
        title = self.project.get_name() or GLib.get_application_name()
        file_params = sorted(set(map(ed_get_gfile_display_name, self.projectview.get_gfiles())))
        task_params = sorted(self.project.get_tasknames())
        self.cmdlauncher.set_cmd_params(file=file_params, task=task_params)
        for sf in reversed(self.get_config('search_filters', list, [])):
            if sf and isinstance(sf, str):
                self.add_combo_history(self.get_ui('ed_combo_filter'), sf)
                self.get_ui('ed_entry_filter').set_text(sf)
        self.win.set_title(title)

    def on_project_config(self, button):
        """Callback on project config open request."""
        if self.project is not None:
            self.project.open_config()

    def on_project_reveal(self, button=None, /):
        """Callback on project reveal document in tree."""
        w, d = self.get_active_doc()
        if d is not None:
            if gf := d.get_gfile():
                self.projectview.reveal_in_tree(gf)

    def on_symbols_reload(self, button):
        """Callback on request to reload symbols treeview."""
        w, d = self.get_active_doc()
        if d is not None:
            d.reload_symbols()

    def on_symbols_expand(self, button):
        """Callback on request to expand symbols tree."""
        self.symbolsview.expand()

    def on_symbols_collapse(self, button):
        """Callback on request to collapse symbols tree."""
        self.symbolsview.collapse()

    def on_symbols_sort(self, button):
        """Callback on request to sort symbols tree."""
        self.update_symbols_for_doc(None)

    def on_symbols_showmore(self, button=None, /):
        """Callback on request to show more symbols."""
        self.update_symbols_for_doc(None, limit=-1)

    def on_command_activate(self, entry):
        """Callback on command run request."""
        if self.cmdlauncher.run(*entry.get_text().split(None, 1)):
            entry.get_root().set_focus(None)
            entry.set_text("")
            self.focus_active_doc()
        else:
            ed_set_transient_style(entry, 'error')
            entry.error_bell()

    def on_search_start(self, button=None, /):
        """Callback on project search start request."""
        self.sidebar.show_page('search')
        self.focus_active_doc()
        self.search_in_project()

    def on_search_clear(self, button):
        """Callback on search results clear request."""
        self.multisearch.reset()
        self.get_ui('ed_entry_find').set_text("")
        self.get_ui('ed_entry_replace').set_text("")
        for d in self.docs.values():
            d.find(direction=0)
        self.focus_active_doc()

    def on_find_activate(self, entry):
        """Callback on find entry activation."""
        current_time = GLib.get_monotonic_time()
        if 100_000 <= current_time - self.time_find <= 1_000_000:   # 100ms to 1s
            self.on_search_start()
        else:
            w, d = self.get_active_doc()
            if d is not None:
                d.find(direction=0, **self.get_search_settings(False))
        self.time_find = current_time

    def on_replace_all(self, button):
        """Callback on replace all request."""
        w, d = self.get_active_doc()
        if d is not None:
            d.replace_all(**self.get_search_settings(True))

    def on_marks_reload(self, button=None, /):
        """Callback on request to reload marks treeview."""
        if not self.is_quitting():
            if self.sidebar.get_visible_page_name() == 'marks':
                self.marksview.load_from_docs(self.get_docs_sorted())

    def on_processing_stop(self, button=None, /):
        """Callback on processing stop request."""
        self.stopthread.set()
        if not self.cancellable.is_cancelled():
            self.cancellable.cancel()
        self.notifier.status(_("Stop processing"))

    def on_bottomstack_changed(self, select_model, pos, removed, added):
        """Callback on bottom stack modified."""
        n_items = select_model.get_n_items()
        if self.is_quitting():
            pass
        elif n_items == 0:   # empty
            self.get_ui('ed_box_bottom').set_visible(False)
        elif added > 0:
            self.get_ui('ed_box_bottom').set_visible(True)
            select_model.select_item(pos, True)
        elif removed > 0:
            select_model.select_item(min(pos, n_items - 1), True)

    def on_focus_widget(self, window, pspec):
        """Callback for window's focused widget change event."""
        wfocus = self.win.get_focus()
        # Hack for auto-closing popovers
        old = self.last_focused
        old_in_popup = old.get_ancestor(Gtk.Popover.__gtype__) is not None if old else False
        while old:
            if wfocus is not None:
                if wfocus is old or wfocus.is_ancestor(old):
                    break
            for c in old:
                if c is not wfocus and isinstance(c, Gtk.Popover) and (wfocus is None or not wfocus.is_ancestor(c)):
                    c.set_visible(False)   # don't use popdown(), it may close parents in cascade
            old = old.get_parent()
        self.last_focused = wfocus
        # Hack against focus stuck on menubuttons
        if wfocus is not None:
            if wfocus.get_ancestor(Gtk.MenuButton.__gtype__) is not None:
                if isinstance(wfocus.get_parent(), Gtk.MenuButton) and old_in_popup:
                    self.focus_active_doc()
                return
        # Track active notebook
        if wfocus is not None:
            if a := [_n for _n in self.notebooks if wfocus.is_ancestor(_n.get_widget())]:
                self.select_notebook(a.pop())

    def on_close_request(self, window):
        """Callback on window close request."""
        if self.docs:
            if any(_t.is_modified() for _t in self.docs.values()):
                self.single_notebook = False
                self.update_single_notebook()
                m = EdMessage(window, ('-yes', 'cancel'),
                              _("Some files have been modified, really quit?"),
                              _("If leaving now, all unsaved changes will be lost!"))
            else:
                m = EdMessage(window, ('+yes', 'cancel'),
                              _("Close this window?"),
                              self.project.get_name() if self.project else " ")
            m.add_response_handler(Gtk.ResponseType.YES, lambda: self.close_win() or self.win.destroy())
            m.show_message()
            return True
        self.close_win()
        return False
