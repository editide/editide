#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Improved threading for usage with GLib."""

import contextlib
import queue
import subprocess
import threading

from gi.repository import GLib


__all__ = ['EdThread', 'ed_new_threadevent', 'ed_run_subprocess_in_thread']


def ed_new_threadevent():
    """Create new thread event."""
    return threading.Event()


def __run_subprocess_async(cmd, sp_params, finish_cbk):
    """Thread handler for running a subprocess."""
    try:
        ret, ex = subprocess.run(cmd, check=True, shell=False, **sp_params), None
    except Exception as e:
        ret, ex = None, e
    threading.current_thread().run_in_maincontext(finish_cbk, ret, ex)


def ed_run_subprocess_in_thread(cmd_args, cbk, ctx, **sp_params_user):
    """Run subprocess in thread."""
    assert callable(cbk)
    sp_params = dict(text=True, capture_output=True, encoding='utf-8', errors='replace')
    sp_params.update(sp_params_user)
    t_args = (cmd_args, sp_params, lambda _r, _e: cbk(_r, _e, ctx))
    thread = EdThread(target=__run_subprocess_async, args=t_args)
    thread.start()


class EdThread(threading.Thread):
    """Improved thread to communicate with GLib main context."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, daemon=False)
        self.queue = queue.Queue()

    def push_queue(self, data):
        """Add element for main context in shared queue."""
        self.queue.put(data)

    @contextlib.contextmanager
    def pop_queue(self):
        """Read element in shared queue from main context."""
        try:
            yield self.queue.get_nowait()   # return of __enter__, will block until __exit__
        finally:
            self.queue.task_done()

    def flush(self):
        """Wait until all shared queue elements have been processed."""
        self.queue.join()

    def idle_cbk_wrapper(self, cbk):
        """Callback wrapper, retrieving parameters from the shared queue."""
        with self.pop_queue() as data:
            cbk(*data)

    def run_in_maincontext(self, cbk, *data):
        """Run callback in GLib main context, data is safely exchanged over shared queue."""
        assert callable(cbk)
        if data:
            self.push_queue(data)
            GLib.idle_add(self.idle_cbk_wrapper, cbk)
        else:
            GLib.idle_add(cbk)
