#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Wrapping utilities."""


__all__ = ['EdBuilderWrapper']


class EdBuilderWrapper():
    """Wrapper for GtkBuilder callbacks, used to prevent bound method references on main object."""

    def __init__(self, obj):
        self.impl = obj
        for a in dir(obj):
            if a.startswith('on_') and callable(getattr(obj, a)):
                self.__create_wrapper(a)

    def free(self):
        """Release resources and break circular references."""
        self.impl = None

    def __create_wrapper(self, func):
        """Create a local wrapper to method."""
        setattr(self, func, lambda *_args: getattr(self.impl, func)(*_args) if self.impl else None)
