#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Code completion management."""

from gi.repository import GtkSource

from ...exts import ed_extensions_get_data


__all__ = ['EdCompletionManager']


class EdCompletionManager():
    """Code completion manager."""
    SINGLETON = None

    def __init__(self):
        assert self.SINGLETON is None
        self.providers = {}   # srcview_id: set(providers)
        self.factories = {}   # type(Provider): list(register_attrs)
        for cpf, attrs in ed_extensions_get_data('completion'):
            self.declare_provider_factory(cpf, attrs)

    @classmethod
    def get_default(cls):
        """Return default completion manager."""
        if cls.SINGLETON is None:
            cls.SINGLETON = cls()
        return cls.SINGLETON

    def declare_provider_factory(self, ptyp, register_attrs):
        """Declare a provider factory for sourceviews."""
        assert isinstance(ptyp, type)   # object type, not instance
        assert ptyp not in self.factories
        self.factories[ptyp] = register_attrs[:]

    def setup(self, srcview, obj):
        """Setup completion and hover, add available providers."""
        pset = set()
        for ptyp, attrs in self.factories.items():
            args = [getattr(obj, _a) for _a in attrs]
            provider = ptyp()
            provider.register(*args)
            if isinstance(provider, GtkSource.CompletionProvider):
                srcview.get_completion().add_provider(provider)
            if isinstance(provider, GtkSource.HoverProvider):
                srcview.get_hover().add_provider(provider)
            pset.add(provider)
        self.providers[id(srcview)] = pset

    def release(self, srcview, obj):
        """Unregister and remove providers."""
        provs = self.providers.pop(id(srcview), set())
        for p in provs:
            ptyp = type(p)
            if ptyp in self.factories and hasattr(p, 'unregister'):
                args = [getattr(obj, _a) for _a in self.factories[ptyp]]
                p.unregister(*args)
            if isinstance(p, GtkSource.CompletionProvider):
                srcview.get_completion().remove_provider(p)
            if isinstance(p, GtkSource.HoverProvider):
                srcview.get_hover().remove_provider(p)
