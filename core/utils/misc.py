#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Misc utilities."""

import colorsys
import functools
import mimetypes
import os
import re
import subprocess

from gi.repository import GObject, GLib, Gio, Pango, Gtk, GtkSource


ED_PORTAL_DOCS_DIR = f"{GLib.get_user_runtime_dir()}/doc/"
ed_portal_host_paths_cache = {}


def ed_attr_object(clsname, **attrs):
    """Create new attributes-only object."""
    assert isinstance(clsname, str)
    return type(clsname, (), attrs)()


def ed_isinstance(obj, clsname):
    """Similar as the builtin 'isinstance' but with class name, to avoid circular imports."""
    assert isinstance(clsname, str)
    return clsname in (_c.__name__ for _c in obj.__class__.__mro__)


def ed_get_vars(wbench, doc=None, /):
    """Get context variables, useful for running external tools."""
    # Inspired by 'https://code.visualstudio.com/docs/editor/variables-reference'
    doc = wbench.get_active_doc()[1] if doc is None else doc
    gfile = None if doc is None else doc.get_gfile()
    fpath = None if gfile is None else gfile.peek_path()
    ppath = None if wbench.project is None else wbench.get_prj_gpath().peek_path()
    v = {f'env_{_k}': _v for _k, _v in os.environ.items() if _k.isidentifier()}   # environment
    v['userHome'] = GLib.get_home_dir()
    v['execPath'] = GLib.canonicalize_filename(ED_PATH, None)
    v['pathSeparator'] = os.sep
    if ppath:
        v['workspaceFolder'] = ppath
        v['workspaceFolderBasename'] = GLib.path_get_basename(v['workspaceFolder'])
    if doc is not None:
        v['lineNumber'] = doc.get_pos()
        v['selectedText'] = doc.get_selected_text() or ""
    if fpath:
        v['file'] = fpath
        v['fileBasename'] = GLib.path_get_basename(v['file'])
        v['fileBasenameNoExtension'] = v['fileBasename'].rsplit(".", 1)[0]
        v['fileDirname'] = GLib.path_get_dirname(v['file'])
        v['fileDirnameBasename'] = GLib.path_get_basename(v['fileDirname'])
        v['fileExtname'] = v['fileBasename'][len(v['fileBasenameNoExtension']):]
        if 'workspaceFolder' in v:
            v['fileWorkspaceFolder'] = v['workspaceFolder']
            try:
                v['relativeFile'] = os.path.relpath(v['file'], v['workspaceFolder'])
                v['relativeFileDirname'] = os.path.relpath(v['fileDirname'], v['workspaceFolder'])
            except Exception:
                pass
    return v


def ed_get_datapath(*rel_path):
    """Get path to application data."""
    return os.path.join(ED_PATH, "data", *rel_path)


def ed_get_configpath(*rel_path):
    """Get path to user config."""
    return os.path.join(GLib.get_user_config_dir(), ED_APPNAME, *rel_path)


def ed_get_userpath(*rel_path):
    """Get path to user data."""
    return os.path.join(GLib.get_user_data_dir(), ED_APPNAME, *rel_path)


def ed_get_gfile_host_path(gfile, local_path=None, /):
    """Get the portal host path of the provided GFile."""
    local_path = local_path or gfile.get_parse_name()
    attr = 'xattr::document-portal.host-path'
    info = gfile.query_info(attr, Gio.FileQueryInfoFlags.NONE, None)
    if host_path := info.get_attribute_as_string(attr):
        host_path = host_path.removesuffix("\\x00")
        if local_path.count("/") == 6:
            ed_portal_host_paths_cache[f"{local_path}/"] = f"{host_path}/"
        return host_path
    return local_path


def ed_get_gfile_display_path(gfile, portal=True, /):
    """Get a displayable path for the provided GFile."""
    path = gfile.get_parse_name()
    if not gfile.is_native():
        try:
            if GLib.uri_is_valid(path, GLib.UriFlags.NONE):
                path = GLib.uri_unescape_string(path)
        except GLib.Error:
            pass
    elif portal and path.startswith(ED_PORTAL_DOCS_DIR):
        for lp, hp in ed_portal_host_paths_cache.items():
            if path.startswith(lp):
                return hp + path[len(lp):]
        return ed_get_gfile_host_path(gfile, path)
    return path


def ed_get_gfile_display_name(gfile):
    """Get a displayable basename for the provided GFile."""
    return GLib.path_get_basename(ed_get_gfile_display_path(gfile, False))


def ed_get_gfile_mimetype(gfile):
    """Get mime type of the provided GFile."""
    m, e = mimetypes.guess_type(ed_get_gfile_display_name(gfile), False)
    if os.name == 'posix':   # for better guess, also check content
        t, u = Gio.content_type_guess(gfile.get_basename())
        if t and not (u and Gio.content_type_is_unknown(t)):
            m = Gio.content_type_get_mime_type(t)
        try:
            info = gfile.query_info(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, Gio.FileQueryInfoFlags.NONE, None)
            if t := info.get_content_type():
                if not Gio.content_type_is_unknown(t):
                    m = Gio.content_type_get_mime_type(t)
        except GLib.Error:
            pass
    return m


def ed_is_gfile_dir(gfile):
    """Check if GFile is a directory."""
    return Gio.FileType.DIRECTORY == gfile.query_file_type(Gio.FileQueryInfoFlags.NONE, None)


def ed_is_gfile_text(gfile):
    """Check if GFile has a text-derived mimetype."""
    if gfile is None:
        return True
    try:
        info = gfile.query_info(Gio.FILE_ATTRIBUTE_STANDARD_SIZE, Gio.FileQueryInfoFlags.NONE, None)
        if info.get_size() == 0:
            return True
    except GLib.Error:
        pass
    if m := ed_get_gfile_mimetype(gfile):
        if Gio.content_type_is_a(m, 'text/plain'):
            return True
    if GtkSource.LanguageManager.get_default().guess_language(gfile.get_basename(), m):
        return True
    if fpath := gfile.peek_path():
        args = ["file", "--mime-type", "--keep-going", "--", fpath]
        try:
            ret = subprocess.run(args, text=True, capture_output=True, check=False, encoding='utf-8', errors='replace')
            out = ret.stdout.rsplit(":", 1)[-1].strip().split(r"\012- ")
        except Exception:
            out = ()
        if 'application/octet-stream' in out:
            return False
    ed_log("error", f"could not detect content type of '{gfile.get_parse_name()}'")
    return True   # can't detect content, assume the users know what they do


def ed_wrap_path(path, limit):
    """Wrap a path by directory separator."""
    wraps, line = [], ""
    for c in re.split(r"(.+?/|\\)", path):
        if line and len(line + c) > limit:
            wraps.append(line)
            line = ""
        line += c
    return "\n".join(wraps + [line])


def ed_open_extern(gfile, ask, wbench=None, /):
    """Open a GFile externally with its default application."""
    try:
        win = None if wbench is None else wbench.win
        Gtk.FileLauncher(file=gfile, always_ask=ask).launch(win, None, None)
    except Exception as e:
        if wbench is not None:
            errmsg = _("Failed to open '{}' externally!").format(ed_get_gfile_display_path(gfile))
            wbench.notifier.error(errmsg, f"{e}")


def ed_get_clipboard_text(cbk):
    """Get text from clipboard."""
    assert callable(cbk)
    widget = Gio.Application.get_default().get_active_window()
    clipb = widget.get_clipboard()
    clipb.read_text_async(None, lambda _c, _r: cbk(_c.read_text_finish(_r)))


def ed_set_clipboard_text(txt):
    """Set text to clipboard."""
    assert isinstance(txt, str)
    widget = Gio.Application.get_default().get_active_window()
    widget.get_clipboard().set(txt)


def ed_copy_file_location(gfile, part):
    """Copy file location to clipboard."""
    txt = None
    match part:
        case 'name':
            txt = ed_get_gfile_display_name(gfile)
        case 'path':
            txt = ed_get_gfile_display_path(gfile)
        case 'raw':
            txt = gfile.get_parse_name()
        case 'dir':
            if gd := gfile.get_parent():
                txt = ed_get_gfile_display_path(gd)
        case 'uri':
            txt = gfile.get_uri()
    if txt:
        ed_set_clipboard_text(txt)


def ed_get_source_theme_scheme(app):
    """Get source code theme scheme."""
    dark = Gtk.Settings.get_default().get_property('gtk-application-prefer-dark-theme')
    lt = app.get_setting('core', 'highlight_theme_light', str, f'{ED_APPNAME}-light')
    dt = app.get_setting('core', 'highlight_theme_dark', str, f'{ED_APPNAME}-dark')
    theme = dt if dark else lt
    return GtkSource.StyleSchemeManager.get_default().get_scheme(theme)


def ed_set_transient_style(widget, style_class, timeout=2000, /):
    """Apply style, then revert after timeout."""
    widget.add_css_class(style_class)
    GLib.timeout_add(timeout, widget.remove_css_class, style_class)


def ed_is_rtl():
    """Check if widgets use a right-to-left reading direction."""
    return Gtk.Widget.get_default_direction() == Gtk.TextDirection.RTL


def ed_get_char_size(layout, char):
    """Get character width for the Pango layout."""
    layout.set_text(char)
    if layout.get_unknown_glyphs_count() > 0:
        return -1
    return layout.get_size().width


def ed_get_modif_indicator(w):
    """Get an appropriate modification indicator for a widget."""
    pl = Pango.Layout.new(w.get_pango_context())
    size = ed_get_char_size(pl, "M")
    sc = ((ed_get_char_size(pl, _c), _c) for _c in "\u2022\u2219\u23fa\u25cf\u26ab")
    ok = ["\u2022"] + [_c for _s, _c in sorted(sc) if 0 < _s < size]
    return ok[-1]


def ed_get_text_filter_change(old, new):
    """Compare texts and return filter change type."""
    if old is None or new is None:
        return Gtk.FilterChange.DIFFERENT
    if old in new:
        return Gtk.FilterChange.MORE_STRICT
    if new in old:
        return Gtk.FilterChange.LESS_STRICT
    return Gtk.FilterChange.DIFFERENT


@functools.cache
def ed_get_kind_color(kl):
    """Get the color of a symbol's kind."""
    dark = Gtk.Settings.get_default().get_property('gtk-application-prefer-dark-theme')
    lum = 0.625 if dark else 0.5
    hue = ((49 * ord(kl[0])) % 64) / 64   # pseudorandomize the color hue of each kind letter
    rgb = colorsys.hls_to_rgb(hue, lum, 0.75)
    return '#{:02x}{:02x}{:02x}'.format(*(int(255 * _c) for _c in rgb))


def ed_get_comment_delimiters(lang):
    """Get comment delimiters for language."""
    cs = ce = cl = None   # blockstart, blockend, linestart
    if lang is not None:
        lcs = lang.get_metadata('line-comment-start')
        bcs = lang.get_metadata('block-comment-start')
        bce = lang.get_metadata('block-comment-end')
        if bcs is not None and bce is not None:
            cs, ce = bcs, bce
            if bcs == "/*" and bce == "*/":
                cl = "*"
        elif lcs is not None:
            cs, cl = lcs, lcs
    return cs, ce, cl


def ed_set_textbuffer_defaults(buffer, bind_obj=None, bind_prop='', /):
    """Set default textbuffer attributes, suitable for source code display."""
    tag = buffer.create_tag('ed-default', show_spaces='ignorables', insert_hyphens=False)
    tag.set_priority(0)   # lowest prio
    if bind_obj and bind_prop:
        bind_obj.bind_property(bind_prop, tag, 'show-spaces', GObject.BindingFlags.SYNC_CREATE,
                               transform_to=lambda _b, _v: 'ignorables' if _v else 'none')
    buffer.connect('changed', lambda _b: _b.apply_tag(tag, *_b.get_bounds()))


# Export
__all__ = [_k for _k, _v in locals().items() if _k.startswith('ed_') and callable(_v)]
