#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Utilities for managing resources access history."""


__all__ = ['EdHistoryTracker', 'ed_lru_push', 'ed_lru_pop']


def ed_lru_push(lru_list, obj):
    """Push object into least-recently-used list."""
    if obj in lru_list:
        lru_list.remove(obj)
    lru_list.append(obj)


def ed_lru_pop(lru_list, obj):
    """Remove object from least-recently-used list, then return the most recent one."""
    if obj in lru_list:
        lru_list.remove(obj)
    return lru_list[-1] if lru_list else None


class EdHistoryTracker():
    """History tracker."""

    def __init__(self):
        self.hist_curr = None
        self.hist_prev = []
        self.hist_next = []

    def clear(self):
        """Clear history."""
        self.hist_curr = None
        self.hist_prev.clear()
        self.hist_next.clear()

    def set(self, item):
        """Set current item in history."""
        if self.hist_curr is not item:
            curr = [] if self.hist_curr is None else [self.hist_curr]
            self.hist_prev = [_i for _i in self.hist_prev + self.hist_next + curr if _i is not item]
            self.hist_next = []
            self.hist_curr = item

    def add(self, item):
        """Add item to history."""
        self.hist_prev.insert(0, item)

    def remove(self, item):
        """Remove item from history, return the fallback item."""
        self.hist_prev = [_i for _i in self.hist_prev if _i is not item]
        self.hist_next = [_i for _i in self.hist_next if _i is not item]
        if self.hist_curr is item:
            if self.hist_next:
                self.hist_curr = self.hist_next.pop(0)
            elif self.hist_prev:
                self.hist_curr = self.hist_prev.pop()
            else:
                self.hist_curr = None
        return self.hist_curr

    def prev(self):
        """Switch history to previous item and return it."""
        if self.hist_prev:
            self.hist_next.insert(0, self.hist_curr)
            self.hist_curr = self.hist_prev.pop()
            return self.hist_curr
        return None

    def next(self):
        """Switch history to next item and return it."""
        if self.hist_next:
            self.hist_prev.append(self.hist_curr)
            self.hist_curr = self.hist_next.pop(0)
            return self.hist_curr
        return None
