#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""File monitoring."""

from gi.repository import Gio


__all__ = ['EdFileManager']


class EdFileMonitor():
    """Helper for GFileMonitor with multiple callbacks."""

    def __init__(self, gfile):
        self.notifiers = set()
        self.cancellable = Gio.Cancellable.new()
        self.monitor = gfile.monitor_file(Gio.FileMonitorFlags.NONE, self.cancellable)
        self.monitor.connect('changed', self.on_changed)

    def register_notifier(self, notifier):
        """Register a notifier callback for the file monitor. Duplicates ignored."""
        assert callable(notifier)
        self.notifiers.add(notifier)

    def unregister_notifier(self, notifier):
        """Register a notifier callback for the file monitor."""
        self.notifiers.discard(notifier)
        if not self.notifiers:
            self.monitor.disconnect_by_func(self.on_changed)
            self.monitor.cancel()
        return self.monitor.is_cancelled()

    def on_changed(self, m, gf, gf_other, event):
        """Callback for external changes reported by the file monitor."""
        if event in (Gio.FileMonitorEvent.CHANGES_DONE_HINT, Gio.FileMonitorEvent.DELETED):
            for n in self.notifiers:
                n(event)


class EdFileManager():
    """File manager for sharing common resources."""

    def __init__(self):
        self.fmonitors = {}   # filemonitor: gfile

    def get_file_monitor(self, gfile):
        """Get the active file monitor for the given file, or None."""
        for m, gf in self.fmonitors.items():
            if gf.equal(gfile):
                return m
        return None

    def watch_file(self, gfile, notifier):
        """Watch file for changes. Can be called multiple times."""
        assert gfile is not None
        m = self.get_file_monitor(gfile) or EdFileMonitor(gfile)
        m.register_notifier(notifier)
        if m not in self.fmonitors:
            self.fmonitors[m] = gfile

    def unwatch_file(self, gfile, notifier):
        """Unwatch file."""
        if m := self.get_file_monitor(gfile):
            if m.unregister_notifier(notifier):
                del self.fmonitors[m]
