#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Pattern matching for paths."""

import fnmatch
import pathlib

from gi.repository import GLib


__all__ = ['EdMatchPattern', 'EdPathMatch']


class EdMatchPattern():
    """Path matching pattern."""
    __slots__ = ('dir_only', 'neg', 'parts', 'pattern')

    def __init__(self, p, n):
        self.pattern = p
        self.neg = n
        self.dir_only = False
        # Generalize pattern
        workpattern = p
        if p.endswith("/"):   # folders only
            workpattern = workpattern[:-1]
            self.dir_only = True
        if p.startswith("./"):   # relative path
            workpattern = workpattern[2:]
        elif p.startswith("/"):   # relative path
            workpattern = workpattern[1:]
        elif p and not p.startswith("**") and "/" not in p[:-1]:   # leaf
            workpattern = "**/" + workpattern
        self.parts = tuple(workpattern.split("/"))

    @classmethod
    def expand_braces(cls, text):
        """Expand comma-separated chunks inside curly braces."""
        if (i1 := text.find("}")) > 0:
            if (i0 := text.rfind("{", 0, i1)) >= 0:
                # Note: escaped braces not supported, ignore "{num1..num2}"
                for sub in text[i0+1:i1].split(","):
                    yield from cls.expand_braces(text[:i0] + sub + text[i1+1:])
                return
        yield text

    @classmethod
    def from_user(cls, text):
        """Create new pattern from user input."""
        if n := text.startswith("!"):   # negative
            text = text[1:]
        text = text.replace("\\", "")   # unescape
        return cls(text, n)

    @classmethod
    def from_gitignore(cls, gfile):
        """Read list of patterns from gitignore file."""
        mpatterns = []
        try:
            success, contents, etag = gfile.load_contents(None)
            if success:
                for line in contents.decode('utf-8').splitlines():
                    if line and not line.startswith("#") and not line.isspace():   # skip comments
                        mpatterns.append(cls.from_user(line))
        except GLib.Error:
            pass
        except Exception:
            ed_log("error", f"could not load patterns from '{gfile.get_parse_name()}'")
        return mpatterns

    @classmethod
    def from_editorconfig(cls, section):
        """Get list of patterns from editorconfig section name."""
        return (cls(_p, False) for _p in cls.expand_braces(section))


class EdPathMatch():
    """Utilities for pattern matching."""
    __slots__ = ('matched',)

    def __init__(self):
        self.matched = False

    @staticmethod
    def escape(path):
        """Escape path for using as pattern."""
        path = pathlib.Path(path).as_posix()
        for special in ("\\", "*", "?", "[", "]", " "):
            path = path.replace(special, "\\" + special)
        return path

    @staticmethod
    def get_path_parts(path):
        """Split path by separator."""
        if pparts := pathlib.Path(path).parts:
            if any(_d in pparts[0] for _d in ("/", "\\", ":")):   # absolute path
                return ("$",) + pparts[1:]   # use '$' as drive
        return pparts

    @classmethod
    def match(cls, path, txtpattern, is_dir=False, /):
        """Check if path matches text pattern."""
        pparts = cls.get_path_parts(path)
        mp = EdMatchPattern(txtpattern, False)
        return (is_dir or not mp.dir_only) and cls.match_parts(pparts, mp.parts)

    def match_patterns(self, path, mpatterns, is_dir=False, /):
        """Check if path matches patterns, in order of precedence."""
        pparts = self.get_path_parts(path)
        for mp in mpatterns:
            if is_dir or not mp.dir_only:
                if self.match_parts(pparts, mp.parts):
                    self.matched = not mp.neg   # last matching pattern decides the outcome
        return self.matched

    @classmethod
    def match_parts(cls, path_parts, pattern_parts):
        """
        Gitignore-style pattern matching.
        Reference: https://git-scm.com/docs/gitignore
        """
        if path_parts and pattern_parts:
            if pattern_parts[-1] == "**":
                if len(pattern_parts) == 1:
                    return True   # catch-all
                elif cls.match_parts(path_parts, pattern_parts[:-1]):   # try non-globbing
                    return True   # path match
                else:   # try globbing subpath
                    return cls.match_parts(path_parts[:-1], pattern_parts)
            elif fnmatch.fnmatch(path_parts[-1], pattern_parts[-1]):   # element match
                return cls.match_parts(path_parts[:-1], pattern_parts[:-1])   # continue on subpath
            else:
                return False   # element mismatch
        if not path_parts and pattern_parts == ("**",):
            return True   # catch-all empty path
        if path_parts or pattern_parts:
            return False   # remaining unmatched elements
        return True   # all parts consumed
