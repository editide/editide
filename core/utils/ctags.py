#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Ctags wrapper."""

import bisect
import fnmatch
import functools
import json
import re
import subprocess

from .thread import EdThread


__all__ = ['EdCtags']


@functools.cache
def ed_get_ctags_languages():
    """Get full list of supported languages."""
    langs = set()
    try:
        args = ["ctags", "--machinable=yes", "--list-languages"]
        ret = subprocess.run(args, text=True, capture_output=True, check=False, encoding='utf-8', errors='replace')
        lines = ret.stdout.lower().splitlines()
        langs.update(lines)
    except Exception:
        ed_log("error", "could not get ctags languages list")
    try:
        args = ["ctags", "--machinable=yes", "--list-aliases"]
        ret = subprocess.run(args, text=True, capture_output=True, check=False, encoding='utf-8', errors='replace')
        lines = ret.stdout.lower().splitlines()
        assert lines[0][0] == "#"   # expect header
        for line in lines[1:]:
            langs.add(line.split()[-1])
    except Exception:
        ed_log("error", "could not get ctags aliases list")
    return tuple(langs)


@functools.cache
def ed_get_kinds_full():
    """Get full list of kinds for all languages."""
    kinds = []
    try:
        args = ["ctags", "--machinable=yes", "--list-kinds-full"]
        ret = subprocess.run(args, text=True, capture_output=True, check=False, encoding='utf-8', errors='replace')
        lines = ret.stdout.splitlines()
        assert lines[0][0] == "#"   # expect header
        head = [_h.lower() for _h in lines[0][1:].split()]
        for line in lines[1:]:
            k = dict(zip(head, line.split(None, len(head) - 1)))
            kinds.append(k)
    except Exception:
        ed_log("error", "could not get ctags kinds list")
    return tuple(kinds)


@functools.cache
def ed_kind_lookup(**properties):
    """Lookup for kind, return first match."""
    for k in ed_get_kinds_full():
        if all(k[_p] == _v for _p, _v in properties.items()):
            return k
    return None


class EdCtags():
    """Retrieve symbols from source file using 'ctags'."""

    def __init__(self, wbench):
        self.wbench = wbench
        self.notif_cbk = []
        self.symlist = []
        self.context = [(0, 0, None)]
        self.sym_at_line_cache = {}   # line: EdSymbol
        self.thread = None
        # Pre-load in maincontext
        ed_get_kinds_full()
        ed_get_ctags_languages()

    def free(self):
        """Release resources and break circular references."""
        self.sym_at_line_cache.clear()
        self.notif_cbk.clear()
        self.wbench = None

    def register_notif(self, cbk):
        """Register notification on symbols update."""
        assert callable(cbk)
        self.notif_cbk.append(cbk)

    def get_symbols(self):
        """Get symbols tree (no reload)."""
        return self.symlist

    def get_symbol_at_line(self, line):
        """Get symbol at line."""
        sym = self.sym_at_line_cache.get(line, LookupError)
        if sym is LookupError:
            i = bisect.bisect_right(self.context, (line, float('inf'), None))
            self.sym_at_line_cache[line] = sym = self.context[i-1][2]
        return sym

    def loop_symbols(self, symlist=None, /):
        """"Loop over available symbols."""
        if symlist is None:
            symlist = self.symlist
        for s in symlist:
            yield s
            yield from self.loop_symbols(s.children)

    def find_symbol(self, name, symlist=None, /):
        """Find symbols by name."""
        if symlist is None:
            symlist = self.symlist
        for s in symlist:
            if s['name'] == name:
                yield s
            yield from self.find_symbol(name, s.children)

    def parse(self, fpath, lang, source):
        """Parse symbols from file (async)."""
        if self.thread is None:
            cwd = self.wbench.get_path_context()
            user_args = self.wbench.get_config('ctags_args', list, [])
            self.thread = EdThread(target=self.parse_async, args=(fpath, lang, source, cwd, user_args))
            self.thread.start()

    def parse_async(self, fpath, lang, source, cwd, user_args):
        """Async tags parsing operation, to be run in own thread."""
        scopedsyms = EdScopedSyms()
        if fpath is not None:
            lines_pos = [0] + [_m.start() for _m in re.finditer(r"$", source, re.M)]
            if any(fnmatch.fnmatch(lang, _l) for _l in ed_get_ctags_languages()):
                cl = f"--language-force={lang}"
            else:
                cl = "-G"
            try:
                args = ["ctags", "--_interactive", cl, "-f-", "-u", "--fields=-P+KSEeiln", "--extras=+r"] + user_args
                source_raw = source.encode(encoding='utf-8', errors='replace')
                header = json.dumps({'command': "generate-tags", 'filename': fpath, 'size': len(source_raw)})
                cmd = header.encode(encoding='utf-8', errors='replace') + b"\n" + source_raw
                ret = subprocess.run(args, input=cmd, cwd=cwd, check=False, capture_output=True)
                for line in ret.stdout.decode(encoding='utf-8', errors='replace').splitlines():
                    if sym := EdSymbol.from_ctag(line):
                        sl = sym['line']
                        if 0 < sl < len(lines_pos):
                            src = source[lines_pos[sl-1]:lines_pos[sl]]
                            sym.props['source'] = re.sub(r"\s+", " ", src.strip())[:128]
                        scopedsyms.add(sym)
                for line in ret.stderr.decode(encoding='utf-8', errors='replace').splitlines():
                    ed_log("warn", "ctags", line)
            except Exception as e:
                ed_log("error", "ctags", f"{e}".splitlines().pop(0))
                scopedsyms.add(EdSymbol(name=_("ERROR! Parsing failed...")))
        self.thread.run_in_maincontext(self.parse_end, scopedsyms)

    def parse_end(self, scopedsyms):
        """End of tags parsing operation."""
        self.symlist = scopedsyms.symlist
        self.context = scopedsyms.context
        del scopedsyms
        self.sym_at_line_cache.clear()
        for cbk in self.notif_cbk:
            cbk()
        self.thread = None


class EdSymbol():
    """Code symbol."""
    __slots__ = ('children', 'parent', 'props', 'scopelist')

    def __init__(self, **properties):
        self.props = {'name': "???", 'path': "", 'line': 0, 'kl': "#", 'language': "Unknown"}
        self.props.update(properties)
        self.parent = None
        self.children = []
        self.scopelist = tuple()

    def __contains__(self, item):
        return item in self.props

    def __getitem__(self, item):
        return self.props.get(item, None)

    def __lt__(self, other):
        a = self['name'].casefold()
        b = other['name'].casefold()
        if a == b:
            return self['name'] < other['name']
        return a < b

    @classmethod
    def from_ctag(cls, tag):
        """Create new symbol from ctag's output."""
        properties = json.loads(tag)
        if properties.pop('_type', "") != 'tag':
            return None
        if sig := properties.get('signature'):
            properties['signature'] = re.sub(r",(?=\S)", ", ", sig)
        sym = cls(**properties)
        if k := ed_kind_lookup(language=sym['language'], name=sym['kind']):
            sym.props['kl'] = k['letter']
        if scope := sym['scope']:
            if '""' in scope:
                sym.scopelist = tuple(scope.split('""'))
            elif "::" in scope:
                sym.scopelist = tuple(scope.split("::"))
            elif " " in scope:
                sym.scopelist = (scope,)
            else:
                sym.scopelist = tuple(scope.split("."))
        else:
            sym.scopelist = tuple()
        return sym

    def update(self, sym):
        """Update properties using values from another symbol."""
        self.props.update(sym.props)


class EdScopedSyms():
    """Organize symbols hierarchically."""

    def __init__(self):
        self.symlist = []
        self.context = [(0, 0, None)]   # line, counter, sym
        self.context_syms = set()
        self.placeholders = []
        self.n = 0   # counter used for sorting the context tuples when on same line

    def find_incomplete(self, symlist, sym):
        """Look for similar incomplete symbol."""
        for s in reversed(self.placeholders):
            if s in symlist:
                if (s['line'] == 0 and s['kl'] in ("#", sym['kl'])
                                   and s['name'] == sym['name']
                                   and s['language'] == sym['language']):
                    return s
        return None

    def is_local_scope(self, sym, pname, rel_symlist):
        """Check context at symbol line, return if it's a symbol matching the parent scope."""
        # Some languages don't give fully qualified scopes, but only the last part
        # Check if context at symbol line matches that scope
        if len(sym.scopelist) == 1 and sym['line']:
            i = bisect.bisect_right(self.context, (sym['line'], float('inf'), None))
            cl, cn, cs = self.context[i-1]
            if cs:   # symbol at line, check context if it can be the parent
                ce = int(cs['end'] or cl)
                if (pname == cs['name'] and sym['line'] in range(cl, ce + 1)
                                        and cs['kind'] == sym['scopeKind']
                                        and cs['language'] == sym['language']):
                    return cs
        # Could be a bad scope split, try to match the full scope
        if scope := sym['scope']:
            for s in rel_symlist:
                if (scope == s['name'] and sym['scopeKind'] == s['kind']
                                       and sym['language'] == s['language']):
                    return s
        return None

    def add(self, sym):
        """Add symbol to symbols tree."""
        newsym = self.add_for_scope(sym, None, self.symlist, sym.scopelist)
        if extras := newsym['extras']:
            if any(_e in extras.split(',') for _e in ('qualified', 'configPrefixed')):
                return   # aliases, don't add to context
        if line := newsym['line']:
            if newsym in self.context_syms:
                return   # symbol already in context
            self.n += 1
            end = int(newsym['end'] or line)
            i = bisect.bisect_right(self.context, (line, float('inf'), None))
            self.context.insert(i, (end + 1, -self.n, self.context[i-1][2]))
            self.context.insert(i, (line, self.n, newsym))
            self.context_syms.add(newsym)

    def add_for_scope(self, sym, rel_parent, rel_symlist, rel_scope):
        """Insert symbol in tree, under provided relative scope."""
        if rel_scope:   # not final scope
            pname = rel_scope[0]
            for s in reversed(rel_symlist):   # search for real parent
                if s['name'] == pname:   # parent scope found
                    parent = s
                    break
            else:
                if cs := self.is_local_scope(sym, pname, rel_symlist):
                    sym.parent = cs
                    cs.children.append(sym)
                    return sym
                trim = len(sym.scopelist) - len(rel_scope)
                parent = EdSymbol(name=pname, language=sym['language'])   # parent placeholder
                self.placeholders.append(parent)
                if 'scope' in sym and len(rel_scope) == 1:
                    parent.props['kind'] = sym['scopeKind']
                    if k := ed_kind_lookup(language=parent['language'], name=parent['kind']):
                        parent.props['kl'] = k['letter']
                parent.parent = rel_parent
                parent.scopelist = sym.scopelist[:trim]
                rel_symlist.append(parent)
            return self.add_for_scope(sym, parent, parent.children, rel_scope[1:])
        elif s := self.find_incomplete(rel_symlist, sym):   # symbol placeholder exists
            self.placeholders.remove(s)
            s.update(sym)
            return s
        else:   # final scope
            sym.parent = rel_parent
            rel_symlist.append(sym)
            return sym
