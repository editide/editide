#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""File name utilities."""


__all__ = ['EdFileName']


class EdFileName(str):
    """File name, sortable in casefolded order."""
    __slots__ = ('key',)
    key: str

    @classmethod
    def new(cls, val):
        """Create new instance."""
        fn = cls(val)
        fn.key = val.casefold()
        return fn

    def __lt__(self, other):
        return str.__lt__(self.key, other.key) if str.__ne__(self.key, other.key) else str.__lt__(self, other)

    def __gt__(self, other):
        return str.__gt__(self.key, other.key) if str.__ne__(self.key, other.key) else str.__gt__(self, other)

    def __le__(self, other):
        return str.__eq__(self, other) or self.__lt__(other)

    def __ge__(self, other):
        return str.__eq__(self, other) or self.__gt__(other)
