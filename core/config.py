#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Configuration manager."""

import configparser
import json
import tomllib

from gi.repository import GLib, Gio, GtkSource

from .utils.misc import ed_get_configpath
from .utils.pathmatch import EdMatchPattern, EdPathMatch


__all__ = ['EdWbenchConfig', 'EdSettings', 'ed_get_subconfig']


def ed_get_default_project_config():
    """Get a default project config."""
    return {
        'exclude': [".*/"],      # list[str]
        'gitignore': False,      # bool
        'associate': {},         # dict[str,str]
        'ctags_args': [],        # list[str]
        "search_filters": [],    # list[str]
        "margin_pos": 100,       # int
        "margin_show": False,    # bool
        "minimap_show": False,   # bool
        "spell_lang": None,      # str?
        'tasks': [],             # list[dict]
    }


def ed_get_subconfig(subcfg, key, typ, default=None, /):
    """Get item from (sub)config, with type check."""
    if isinstance(subcfg, dict):
        val = subcfg.get(key, None)
        return val if isinstance(val, typ) else default
    return default


# EditorConfig support, see https://spec.editorconfig.org/
# Using GLib/Gio backend to access non-native paths.
#
class EdEditorConfig():
    """Manage EditorConfig options."""
    PREAMBLE = "*******"   # fake section for root

    def __init__(self):
        self.editorconfigs = {}   # uri: ConfigParser|None

    def reset(self):
        """Reset editorconfig cache, to force reloading at next check."""
        self.editorconfigs.clear()

    def is_root(self, cfg):
        """Check if config is a root one."""
        return cfg.getboolean(self.PREAMBLE, 'root', fallback=False)

    def add_editorconfig(self, gfile):
        """Load editorconfig from file."""
        cfg = configparser.ConfigParser(default_section=None, interpolation=None)
        try:
            success, contents, etag = gfile.load_contents(None)
            if success:
                cfg.read_string(f"[{self.PREAMBLE}]\n" + contents.decode('utf-8'))
        except GLib.Error as e:
            if e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.NOT_FOUND):
                return None
            ed_log("error", f"editorconfig: {e.message}")
        except Exception:
            ed_log("error", f"could not load config from '{gfile.get_parse_name()}'")
        return cfg

    def get_cfg_at_path(self, gpath):
        """Get editorconfig at path (cached)."""
        uri = gpath.get_uri()
        if uri in self.editorconfigs:   # already known
            return self.editorconfigs[uri]
        gfile = gpath.get_child_for_display_name(".editorconfig")
        cfg = self.add_editorconfig(gfile)
        self.editorconfigs[uri] = cfg
        return cfg

    def solve_option(self, gfile, option, fallback=None, /):
        """Get option value by resolving editorconfig hierarchy."""
        val = fallback
        gpath = gfile
        while gpath := gpath.get_parent():
            if cfg := self.get_cfg_at_path(gpath):
                relpath = gpath.get_relative_path(gfile)
                new = None
                for sec in cfg.sections():
                    if sec == self.PREAMBLE:
                        continue
                    mpatterns = EdMatchPattern.from_editorconfig(sec)
                    if EdPathMatch().match_patterns(relpath, mpatterns):
                        opt = cfg.get(sec, option, fallback=None)
                        new = new if opt is None else opt
                if new == "unset":
                    break   # ignore everything above
                if new is not None:
                    val = new
                    break   # got last value
                if self.is_root(cfg):
                    break   # root
        return val


class EdWbenchConfig():
    """Manage workbench configuration."""

    def __init__(self, wbench):
        self.wbench = wbench
        self.usercfg = ed_get_default_project_config()
        self.interncfg = {}   # volatile
        self.filecfg = EdEditorConfig()
        self.subscribers = set()
        self.load_user_config()

    def free(self):
        """Release resources and break circular references."""
        self.subscribers.clear()
        self.filecfg = None
        self.wbench = None

    def reset_file_config(self):
        """Clear file config cache, typically call this on project reload."""
        self.filecfg.reset()

    def subscribe(self, obj):
        """Subscribe notification when config value changes."""
        self.subscribers.add(obj)

    def unsubscribe(self, obj):
        """Unsubscribe notifications."""
        self.subscribers.discard(obj)

    def load_user_config(self):
        """Load user config settings."""
        user_cfg = ed_get_default_project_config()
        try:
            gfile = Gio.File.new_for_path(ed_get_configpath("defaults.json"))
            load_success, contents, etag = gfile.load_contents(None)
            if load_success:
                user_cfg |= json.loads(contents)
        except json.decoder.JSONDecodeError as e:
            self.wbench.notifier.error(_("Invalid default config!"), f"{e}")
            return   # keep current config
        except GLib.Error as e:
            if not e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.NOT_FOUND):
                self.wbench.notifier.error(_("Unreadable default config!"), f"{e.message}")
        self.usercfg = user_cfg

    def set_intern_config(self, key, val):
        """Set internal (non-persistent) config parameter."""
        assert isinstance(key, str) and key.isidentifier()
        self.interncfg[key] = val
        # Notify subscribers
        for s in self.subscribers:
            cbk = f'on_config_{key}'
            if hasattr(s, cbk):
                getattr(s, cbk)(val)

    def get_file_config(self, gfile, key, default="", /):
        """Get parameter value from file config."""
        assert isinstance(default, str)   # config values are string only, avoid mix-up
        if self.wbench.app.get_setting('core', 'editorconfig', bool, True):
            gfroot = self.wbench.get_prj_gpath()
            if gfile is None and gfroot is not None:
                gfile = gfroot.get_child("*")
            if gfile is not None and gfile.is_native():
                return self.filecfg.solve_option(gfile, key, default)
        return default

    def setup_sourceview(self, view, gfile):
        """Setup GtkSourceView indentation setting according to file config."""
        # tab_width: int (use indent_size if unspecified)
        # indent_size: int/"tab" (use tab_width if "tab")
        # indent_style: "space"/"tab"
        tab_width = self.get_file_config(gfile, 'tab_width')
        indent_size = self.get_file_config(gfile, 'indent_size')
        indent_style = self.get_file_config(gfile, 'indent_style', "tab" if tab_width else "space")
        # Sanitize
        tab_width = int(tab_width) if tab_width in map(str, range(9)) else 0
        if indent_size == "tab":
            indent_size = tab_width
        else:
            indent_size = int(indent_size) if indent_size in map(str, range(9)) else 0
        # Resolve
        if indent_size or tab_width:
            use_spaces = "space" == indent_style
            i = indent_size if indent_size and tab_width and indent_size != tab_width else -1
            t = tab_width or indent_size or (4 if use_spaces else 8)
            view.set_insert_spaces_instead_of_tabs(use_spaces)
            view.set_tab_width(t)
            view.set_indent_width(i)
            return True
        return False

    def setup_fileloader(self, fileloader, gfile, encoding=None, /):
        """Setup GtkSourceFileLoader properties according to file config."""
        if encoding is None:   # if user-requested encoding, skip config
            charset = self.get_file_config(gfile, 'charset')
            encoding = GtkSource.Encoding.get_from_charset(charset.upper())
        if encoding is not None:
            candidates = [encoding] + GtkSource.Encoding.get_default_candidates()
            fileloader.set_candidate_encodings(candidates)

    def setup_filesaver(self, filesaver, gfile):
        """Setup GtkSourceFileSaver properties according to file config."""
        if charset := self.get_file_config(gfile, 'charset'):
            if e := GtkSource.Encoding.get_from_charset(charset.upper()):
                filesaver.set_encoding(e)
        if eol := self.get_file_config(gfile, 'end_of_line'):
            for nl in GtkSource.NewlineType.__enum_values__.values():
                if eol.lower() == nl.value_nick.replace('-', ''):
                    filesaver.set_newline_type(nl)


class EdSettings():
    """Manage user settings."""

    def __init__(self):
        self.raw_cfg = {}
        try:
            with open(ed_get_configpath("settings.toml"), 'rb') as f:
                self.raw_cfg = tomllib.load(f)
        except FileNotFoundError:
            pass
        except tomllib.TOMLDecodeError:
            ed_log("error", "could not decode user settings")
        except Exception:
            ed_log("error", "could not load user settings")

    def get(self, section, key, default=None, /):
        """Get setting value."""
        subcfg = self.raw_cfg
        for subsec in section.split('.'):
            if subsec not in subcfg or not isinstance(subcfg[subsec], dict):
                return default
            subcfg = subcfg[subsec]
        return subcfg.get(key, default)

    def set(self, section, key, val):
        """Set setting value."""
        subcfg = self.raw_cfg
        for subsec in section.split('.'):
            if subsec not in subcfg or not isinstance(subcfg[subsec], dict):
                subcfg[subsec] = {}
            subcfg = subcfg[subsec]
        subcfg[key] = val

    def has_option(self, section, key):
        """Check if a setting has a value set."""
        return self.get(section, key, KeyError) is not KeyError

    def resolve(self, section, key, typ, default=None, /):
        """Resolve setting, with type check."""
        val = self.get(section, key, default)
        return ed_get_subconfig({key: val}, key, typ, default)
