#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Core application."""

from gi.repository import GLib, Gio, Gdk, Gtk, GtkSource

from .actions import EdActionsManager
from .config import EdSettings
from .utils.filemanager import EdFileManager
from .utils.misc import ed_get_datapath, ed_get_configpath, ed_is_gfile_dir
from .workbench import EdWorkbench
from ..exts import ed_extensions_init


__all__ = ['EdApp']


class EdApp(Gtk.Application):
    """Unique application, manages workbenches."""
    __gtype_name__ = __qualname__

    def __init__(self, appid):
        super().__init__(application_id=appid, flags=Gio.ApplicationFlags.HANDLES_OPEN)
        self.add_main_option('debug', 0, GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
                             "Log debug info to stderr", None)
        self.add_main_option('standalone', 0, GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
                             "Start in a standalone instance", None)
        self.instances = {}   # window: wbench
        self.ext_data = {}
        self.settings = EdSettings()
        self.filemanager = EdFileManager()
        # Events
        self.connect('handle-local-options', self.on_local_options)
        self.connect_after('startup', self.on_startup)
        self.connect('activate', self.on_activate)
        self.connect('open', self.on_open)
        self.connect('window-removed', self.on_win_removed)
        self.connect('shutdown', self.on_shutdown)

    def on_local_options(self, app, gvariant_opts):
        """Callback on local options."""
        opts = gvariant_opts.end().unpack()
        if 'standalone' in opts:
            assert not self.get_is_registered()
            self.set_flags(self.get_flags() | Gio.ApplicationFlags.NON_UNIQUE)
        return -1   # continue default option processing

    def on_startup(self, app):
        """Callback on app startup."""
        GtkSource.init()
        GtkSource.StyleSchemeManager.get_default().append_search_path(ed_get_datapath("styles"))
        Gtk.IconTheme.get_for_display(Gdk.Display.get_default()).add_search_path(ed_get_datapath("icons"))
        Gtk.Window.set_default_icon_name(ED_ICON)
        self.apply_css(0, path=ed_get_datapath("app.css"))
        self.apply_css(100, path=ed_get_configpath("gtk4.css"))
        ed_extensions_init(self)
        # Actions
        EdActionsManager.setup_shortcuts(self)
        EdActionsManager.setup_menu(self)

    def on_activate(self, app):
        """Callback on app activation."""
        self.get_instance().show_window()

    def on_open(self, app, gfiles, n, hint, ext_hints=None, /):
        """Callback on open request."""
        assert len(gfiles) == n
        app_hints = dict(tuple(_h.split("=", 1)) for _h in hint.split(",") if "=" in _h)
        ext_hints = ext_hints or {}
        wbench = None
        for gf in gfiles:
            if gf is None:
                continue
            elif ed_is_gfile_dir(gf):
                for wb in self.instances.values():
                    if wb.project is not None and gf.equal(wb.project.get_gpath()):
                        wbench = wb
                        break
                else:
                    if app_hints.get('newwin', "true") == "true":
                        wbench = self.start_instance()
                    else:
                        wbench = self.get_instance()
                    wbench.open_dir(gf, **ext_hints)
            else:   # regular file
                if wbench is not None:
                    pass
                elif app_hints.get('newwin', "false") == "true":
                    wbench = self.start_instance()
                else:
                    wbench = self.get_instance()
                wbench.open_file(gf, **ext_hints)
            wbench.show_window()

    def on_win_removed(self, app, w):
        """Callback on app window closed."""
        if w in self.instances:
            self.instances[w].free()
            del self.instances[w]

    def on_shutdown(self, app):
        """Callback on app shutdown."""
        self.ext_data.clear()
        GtkSource.finalize()

    def apply_css(self, priority, /, *, path=None, txt=None):
        """Set application's style."""
        priority += Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        cssp = Gtk.CssProvider()
        if path and GLib.file_test(path, GLib.FileTest.IS_REGULAR):
            cssp.load_from_path(path)
        if txt:
            cssp.load_from_string(txt)
        d = Gdk.Display.get_default()
        Gtk.StyleContext.add_provider_for_display(d, cssp, priority)
        return cssp

    def get_setting(self, section, key, typ, default=None, /):
        """Get user setting."""
        return self.settings.resolve(section, key, typ, default)

    def get_instance(self):
        """Get active window's instance, create one if none."""
        w = self.get_active_window()
        if w is None:
            wbench = self.start_instance()
        else:
            wbench = self.instances[w]
        return wbench

    def start_instance(self):
        """Instantiate a new window."""
        for wb in self.instances.values():
            if wb.is_empty():
                wbench = wb
                break
        else:
            wbench = EdWorkbench(self)
            self.instances[wbench.win] = wbench
        return wbench
