#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Project manager."""

import json

from gi.repository import GLib, Gio

from .config import ed_get_default_project_config, ed_get_subconfig
from .utils.filename import EdFileName
from .utils.misc import ed_get_gfile_display_path, ed_get_gfile_display_name
from .utils.misc import ed_get_vars, ed_is_gfile_dir
from .utils.pathmatch import EdMatchPattern, EdPathMatch
from .utils.thread import EdThread, ed_run_subprocess_in_thread


__all__ = ['EdProject']


class EdProject():
    """Manage project."""

    def __init__(self, wbench, gpath, prjview):
        self.wbench = wbench
        self.prjview = prjview
        self.prj_gpath = gpath
        self.cfgfile = self.prj_gpath.get_child_for_display_name(f".{ED_APPNAME}.json")
        self.config = {}
        self.thread = None
        self.prjview.reset()

    def free(self):
        """Release resources and break circular references."""
        self.prjview = None
        self.wbench = None

    def get_gpath(self):
        """Get project path as GFile."""
        return self.prj_gpath

    def get_name(self):
        """Get project name."""
        name = self.wbench.get_config('name', str)
        return name or ed_get_gfile_display_name(self.get_gpath())

    def get_tasks(self):
        """Get available tasks from config."""
        return self.wbench.get_config('tasks', list, [])

    def get_tasknames(self):
        """Get list of tasks by name."""
        tasks = self.get_tasks()
        return filter(None, (ed_get_subconfig(_t, 'name', str) for _t in tasks))

    def get_tasks_for_key(self, key):
        """Get the tasks name mapped to given keyboard shortcut."""
        if not key:
            return
        for t in self.get_tasks():
            if key == ed_get_subconfig(t, 'key', str, None):
                yield ed_get_subconfig(t, 'name', str, None)

    def open_config(self):
        """Open project configuration file in editor."""
        self.wbench.request_open([self.cfgfile], "", initdata=self.get_config_txt())

    def reload(self):
        """Reload project (async)."""
        assert self.wbench.project is not None
        if self.thread is None:
            self.load_cfg()
            cfg = dict(self.wbench.filter_config(('exclude', list, []), ('gitignore', bool, False)))
            self.wbench.config.reset_file_config()
            self.wbench.config.load_user_config()
            self.wbench.stopthread.clear()
            self.thread = EdThread(target=self.reload_async, args=(self.get_gpath().dup(),), kwargs=cfg)
            self.thread.start()
            self.prjview.spin_start()
            self.wbench.notifier.status(_("Load project '{}'").format(ed_get_gfile_display_path(self.get_gpath())))

    def reload_async(self, gfroot, **cfg):
        """Async project reload operation, to be run in own thread."""
        ftree = EdFileTree(gfroot, self.wbench.stopthread, **cfg)
        self.thread.run_in_maincontext(self.reload_end, ftree)

    def reload_end(self, ftree):
        """End of project reload operation."""
        if self.wbench is not None and not self.wbench.is_quitting():
            ftree.update_prjview(self.prjview)
            self.wbench.on_project_loaded()
            self.prjview.spin_stop()
        del ftree
        self.thread = None

    def load_cfg(self):
        """Load project configuration."""
        try:
            load_success, contents, etag = self.cfgfile.load_contents(None)
            if load_success:
                self.config = json.loads(contents)
        except json.decoder.JSONDecodeError as e:
            self.wbench.notifier.error(_("Invalid project config!"), f"{e}")
        except GLib.Error as e:
            if not e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.NOT_FOUND):
                self.wbench.notifier.error(_("Unreadable project config!"), f"{e.message}")

    @staticmethod
    def get_config_txt():
        """Get current project configuration as text, for loading in editor."""
        cfg = ed_get_default_project_config()
        cfg['_comment_'] = _("This is a default configuration, adapt to your needs!")
        return json.dumps(cfg, sort_keys=True, indent=4)

    def run_task(self, name):
        """Run a task from config."""
        for t in self.get_tasks():
            if name == ed_get_subconfig(t, 'name', str, None):
                try:
                    ctx = {'task': t}
                    v = ed_get_vars(self.wbench)
                    cwd = ed_get_subconfig(t, 'cwd', str)
                    cwd = cwd.format_map(v) if cwd else self.wbench.get_path_context()
                    args = ed_get_subconfig(t, 'args', list, [])
                    args = [_a.format(cwd=cwd, **v) for _a in args]
                    opts = {'cwd': cwd}
                    ed_run_subprocess_in_thread(args, self.task_result, ctx, **opts)
                except Exception as e:
                    self.task_result(None, e, ctx)
                break
        else:
            self.wbench.notifier.error(_("No task '{}' in config!").format(name), None)

    def task_result(self, ret, ex, ctx):
        """Callback on task result."""
        if self.wbench is not None:
            name = ed_get_subconfig(ctx['task'], 'name', str, "?")
            if ret is not None:
                self.wbench.notifier.status(_("Task '{}' completed").format(name))
            err = ex or ((ret.stderr.strip().splitlines() or [""])[0][:512] if ret.returncode else None)
            if err:
                self.wbench.notifier.error(_("Task '{}' error!").format(name), f"{err}")


class EdFileTree():
    """Files tree."""

    def __init__(self, gfroot, stopthread, **cfg):
        self.gfroot = gfroot
        self.excl_cfg = ((gfroot, [EdMatchPattern.from_user(_e) for _e in cfg['exclude']]),)
        self.gitignore = cfg['gitignore']
        self.stopthread = stopthread
        self.dirs, self.files = self.parse(gfroot, self.init_gitignore_patterns())

    def parse(self, gpath, excl_git):
        """Recursively parse files tree."""
        dirs, files = [], []
        if self.stopthread.is_set():
            return dirs, files
        excl_git += self.get_gitignore_patterns(gpath)
        attrs = ','.join((Gio.FILE_ATTRIBUTE_STANDARD_NAME, Gio.FILE_ATTRIBUTE_STANDARD_TYPE))
        try:
            e = gpath.enumerate_children(attrs, Gio.FileQueryInfoFlags.NONE, None)
            while True:
                success, ginfo, gf = e.iterate(None)
                if ginfo is None or gf is None:
                    break   # no more files
                t = ginfo.get_file_type()
                is_dir, is_file = t == Gio.FileType.DIRECTORY, t == Gio.FileType.REGULAR
                if not self.is_excluded(gf, excl_git + self.excl_cfg, is_dir):
                    pn = ed_get_gfile_display_path(gf)
                    bn = EdFileName.new(GLib.path_get_basename(pn))
                    if is_dir:
                        d, f = self.parse(gf, excl_git)
                        if d or f:
                            dirs.append((bn, gf, d, f))
                    elif is_file:
                        files.append((bn, gf, GLib.markup_escape_text(pn)))
            e.close()
        except GLib.Error:
            # can't scan this folder, skip
            ed_log("error", f"could not scan folder '{gpath.get_parse_name()}'")
        return dirs, files

    def is_excluded(self, gfile, excl_list, is_dir):
        """Check if file matches the exclusion list."""
        pm = EdPathMatch()
        for gfr, mpatterns in excl_list:
            pm.match_patterns(gfr.get_relative_path(gfile), mpatterns, is_dir)
        return pm.matched

    def init_gitignore_patterns(self):
        """Load gitignore patterns from project's parent directories."""
        gip = ()
        if self.gitignore:
            gpath = self.gfroot
            try:
                while not ed_is_gfile_dir(gpath.get_child_for_display_name(".git")):
                    if gpath := gpath.get_parent():
                        gip = self.get_gitignore_patterns(gpath) + gip
                    else:
                        break
            except GLib.Error:
                # can't access path, stop here
                ed_log("error", f"could not get gitignores above '{gpath.get_parse_name()}'")
        return gip

    def get_gitignore_patterns(self, gpath):
        """Read gitignore patterns from gpath folder."""
        if self.gitignore:
            gf_ignore = gpath.get_child_for_display_name(".gitignore")
            if mplist := EdMatchPattern.from_gitignore(gf_ignore):
                return ((gpath, mplist),)
        return ()

    def update_prjview(self, prjview):
        """Load files tree into project view."""
        prjview.update_filetree(self.dirs, self.files)
