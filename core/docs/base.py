#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Base class for document viewer or editor."""

from gi.repository import Gtk

from ..ui.notifier import EdToastBoxNotifier
from ..utils.misc import ed_get_gfile_display_path, ed_get_gfile_display_name
from ..utils.misc import ed_copy_file_location, ed_set_clipboard_text


__all__ = ['EdBaseDocument']


class EdBaseDocument():
    """Abstract class for document container."""

    def __init__(self, wbench):
        self.wbench = wbench
        self.root = Gtk.Box(orientation='vertical', vexpand=True)
        self.notebook = None
        self.tablabel = _("Untitled")
        self.description = None
        self.save_conflict = False
        self.ext_data = {}
        notif_box = Gtk.Box(orientation='vertical', vexpand=False)
        self.notifier = EdToastBoxNotifier(self.wbench.notifier.core, notif_box)
        self.root.append(notif_box)

    if ED_DEBUG > 1:
        def __del__(self):
            ed_log("memory", "del document")

    def free(self):
        """Release resources and break circular references."""
        self.stop_monitor()
        self.notifier.free()
        self.ext_data.clear()
        self.notifier = None
        self.root = None
        self.wbench = None

    @staticmethod
    def is_gfile_supported(gfile):
        """Check if this document class supports the GFile."""
        return False   # nothing supported by base class

    def set_main_widget(self, w):   # final
        """Set doc's main widget."""
        w.set_vexpand(True)
        self.root.append(w)
        if ED_DEBUG > 1:
            w.weak_ref(ed_log, "memory", "finalize document widget")

    def get_widget(self):   # final
        """Get doc's top widget."""
        return self.root

    def get_context(self):   # virtual
        """Get document context, used to save/restore state."""
        return {'docclass': self.__class__.__name__}

    def focus(self):
        """Set focus on document."""
        self.get_widget().child_focus(Gtk.DirectionType.TAB_FORWARD)

    def is_closing(self):
        """Check if document is closing."""
        return self.wbench is None or self.root is None or self.root.in_destruction()

    def is_modified(self):   # virtual
        """Check if document has unsaved modifications."""
        return False

    def get_modif_actions(self):   # virtual
        """Get if undo and redo are possible."""
        return False, False

    def get_gfile(self):   # virtual
        """Get the document's GFile."""
        return None

    def get_doc_fullname(self):
        """Get displayable full path of document's file, or tab title if none."""
        if gf := self.get_gfile():
            return ed_get_gfile_display_path(gf)
        if self.description:
            return self.description
        return self.tablabel

    def copy_file_location(self, part):
        """Put file location in clipboard."""
        if gf := self.get_gfile():
            ed_copy_file_location(gf, part)
        elif part == 'name':
            ed_set_clipboard_text(self.tablabel)

    def start_monitor(self):   # final
        """Start monitoring the document's file for changes."""
        if gf := self.get_gfile():
            self.wbench.app.filemanager.watch_file(gf, self.on_extern_changed)

    def stop_monitor(self):   # final
        """Stop monitoring the document's file."""
        if gf := self.get_gfile():
            self.wbench.app.filemanager.unwatch_file(gf, self.on_extern_changed)

    def add_to_notebook(self, nb):   # final
        """Add document to notebook."""
        self.notebook = nb
        self.notebook.add_page(self.get_widget(), None)
        self.update_title()

    def remove_from_notebook(self):   # final
        """Remove document from notebook."""
        self.notebook.detach_page(self.get_widget())
        self.notebook = None

    def update_title(self):
        """Update tab title, from file basename and modification status."""
        if gfile := self.get_gfile():
            self.tablabel = ed_get_gfile_display_name(gfile)
        if self.notebook is not None:
            self.notebook.set_tab_title(self.get_widget(), self.tablabel, self.is_modified())
            self.notebook.set_tab_tooltip(self.get_widget(), self.get_doc_fullname())
        self.wbench.update_doc_properties(self)

    def update_theme(self):   # virtual
        """Updated theme."""
        pass

    def reload_symbols(self):   # virtual
        """Reload symbols from document's file."""
        pass

    def get_symbols(self):   # virtual
        """Get symbols of document's file (must have been loaded before)."""
        return []

    def get_selected_text(self):   # virtual
        """Get selected text in document."""
        return None

    def get_text_at_pos(self, pos):   # virtual
        """Get text at position."""
        return None

    def get_pos(self):   # virtual
        """Get current position in document."""
        return 0

    def go_to(self, pos):   # virtual
        """Go to position in document content."""
        pass

    def toggle_mark(self, category, /, *, line=None):   # virtual
        """Mark current position in document."""
        pass

    def goto_next_mark(self, category):   # virtual
        """Move to next marked position."""
        pass

    def find(self, **settings):   # virtual
        """Find text in document."""
        pass

    def replace(self, **settings):   # virtual
        """Replace current occurrence of text in document."""
        pass

    def replace_all(self, **settings):   # virtual
        """Replace all occurrences of text in document."""
        pass

    def undo(self):   # virtual
        """Undo last modification."""
        pass

    def redo(self):   # virtual
        """Redo last undone modification."""
        pass

    def save(self, close=False, /):   # virtual
        """Save document content in current GFile."""
        pass

    def save_as(self, gfile, close=False, /):   # virtual
        """Save document content in new GFile."""
        pass

    def on_activate(self):   # virtual
        """Notification when document is set as the active one."""
        pass

    def on_action(self, actionname, **kwargs):   # final
        """Execute action, if handler exists."""
        handler = f'action_{actionname}'
        if hasattr(self, handler):
            getattr(self, handler)(**kwargs)

    def on_extern_changed(self, event):   # virtual
        """Callback on external changes reported by the file monitor."""
        pass
