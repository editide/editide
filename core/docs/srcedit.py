#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Source code editor."""

import fnmatch
import re

from gi.repository import GObject, GLib, Gio, Gdk, Gtk, GtkSource

from .base import EdBaseDocument
from ..ui.outlinebar import EdOutlineBar
from ..utils.completion import EdCompletionManager
from ..utils.ctags import EdCtags
from ..utils.misc import ed_attr_object, ed_get_datapath
from ..utils.misc import ed_get_clipboard_text, ed_set_clipboard_text
from ..utils.misc import ed_get_gfile_mimetype, ed_is_gfile_text
from ..utils.misc import ed_is_rtl, ed_get_kind_color, ed_get_source_theme_scheme
from ..utils.misc import ed_get_comment_delimiters, ed_set_textbuffer_defaults
from ..utils.wrapper import EdBuilderWrapper
from ...exts import ed_extensions_notify


__all__ = ['EdSrcEditor']


ED_AUTOCLOSE_PAIRS = dict(("()", "[]", "{}", "''", '""', "``"))
ED_AUTOCLOSE_SKIP = [_v for _k, _v in ED_AUTOCLOSE_PAIRS.items() if _k != _v]


class EdSrcEditor(EdBaseDocument):
    """Document container for source code editing."""

    def __init__(self, wbench, gfile, **hints):
        super().__init__(wbench)
        self.sfile = GtkSource.File()
        self.sfile.set_location(gfile)
        self.hints = hints
        self.goto_undo, self.goto_redo = [0], []
        self.last_edit_line = 0
        self.sym_at_cur = None
        self.jumpdef = None
        self.blockmode = ed_attr_object('EdBlockMode', enabled=False, marks=[])
        self.unfocus_selection = None
        self.spin_requests = 0
        self.is_indent_configured = False
        self.is_loading = False
        self.scroll_pending = False
        # GUI
        self.builderwrapper = EdBuilderWrapper(self)
        self.builder = Gtk.Builder(self.builderwrapper)
        self.builder.add_from_file(ed_get_datapath("srcedit.ui"))
        self.set_main_widget(self.get_doc_ui('ed_box_doc'))
        self.curpos = self.get_doc_ui('ed_label_cursorpos')
        self.cursel = self.get_doc_ui('ed_label_cursorselect')
        self.breadcrumb = self.get_doc_ui('ed_label_breadcrumb')
        self.view = self.get_doc_ui('ed_sourceview')
        sd = self.view.get_space_drawer()
        sd.set_types_for_locations(GtkSource.SpaceLocationFlags.ALL,
                                   GtkSource.SpaceTypeFlags.ALL ^ GtkSource.SpaceTypeFlags.NEWLINE)
        sd.set_enable_matrix(self.wbench.app.get_setting('core', 'show_spaces', bool, True))
        self.view.set_mark_attributes('bookmark', GtkSource.MarkAttributes(icon_name='ed-bookmark'), 0)
        self.view.get_gutter(Gtk.TextWindowType.LEFT).get_last_child().set_margin_end(3)
        self.action_minimap()
        self.set_right_margin()
        for v, e in sorted(Gtk.WrapMode.__enum_values__.items()):
            self.get_doc_ui('ed_combo_wrapmode').get_model().append(e.value_nick)
        # Internals
        self.buffer = self.view.get_buffer()
        self.buffer.set_implicit_trailing_newline(False)
        ed_set_textbuffer_defaults(self.buffer, self.get_doc_ui('ed_switch_showignorables'), 'active')
        self.ctags = EdCtags(self.wbench)
        self.ctags.register_notif(self.on_ctags_update)
        EdCompletionManager.get_default().setup(self.view, self)
        self.search = GtkSource.SearchContext.new(self.buffer, None)
        self.search.get_settings().set_wrap_around(True)
        autohl_settings = GtkSource.SearchSettings(search_text=None, at_word_boundaries=True,
                                                   case_sensitive=True, regex_enabled=False,
                                                   wrap_around=True)
        self.autohl = GtkSource.SearchContext.new(self.buffer, autohl_settings)
        self.outline = EdOutlineBar(self.view)
        self.outline.set_search_context(self.autohl, 'left')
        self.outline.set_search_context(self.search, 'right')
        self.get_doc_ui('ed_box_views').append(self.outline)
        self.setup_extra_menu()
        self.update_theme()
        # Events
        self.buffer.connect_after('delete-range', self.on_delete_range_after)
        self.buffer.connect('delete-range', self.on_delete_range)
        self.buffer.connect('insert-text', self.on_insert_text)
        self.buffer.connect('notify::cursor-position', self.on_cursor_position)
        self.buffer.connect('notify::can-undo', self.on_modif_actions)
        self.buffer.connect('notify::can-redo', self.on_modif_actions)
        self.buffer.connect('modified-changed', self.on_modified_changed)
        self.get_doc_ui('ed_scale_indentsize').set_format_value_func(lambda _w, _v: "tab" if _v < 0 else str(int(_v)))
        self.bind('ed_sourceview', 'wrap-mode', 'ed_combo_wrapmode', 'selected')
        self.bind(sd, 'enable-matrix', 'ed_switch_showspaces', 'active')
        # Load content
        if gfile is not None:
            self.load(self.hints.get('charset', ""))
        else:
            self.set_interactive(True)
            self.on_cursor_position()
            self.update_properties()
            GLib.idle_add(self.update_indent)

    def free(self):
        """Release resources and break circular references."""
        self.autohl.disconnect_by_func(self.outline.on_search_occurrences)
        self.search.disconnect_by_func(self.outline.on_search_occurrences)
        self.buffer.disconnect_by_func(self.on_cursor_position)
        self.buffer.disconnect_by_func(self.on_delete_range)
        self.buffer.disconnect_by_func(self.on_delete_range_after)
        self.buffer.disconnect_by_func(self.on_insert_text)
        self.buffer.disconnect_by_func(self.on_modif_actions)
        self.buffer.disconnect_by_func(self.on_modified_changed)
        EdCompletionManager.get_default().release(self.view, self)
        self.builderwrapper.free()
        self.outline.free()
        self.ctags.free()
        super().free()
        self.buffer = None
        self.builder = None
        self.ctags = None
        self.outline = None
        self.view = None

    @staticmethod
    def is_gfile_supported(gfile):
        """Check if this document class supports the GFile."""
        return gfile is None or ed_is_gfile_text(gfile)

    def get_doc_ui(self, elem):
        """Get editor UI element by name."""
        return self.builder.get_object(elem)

    def bind(self, elem1, prop1, elem2, prop2):
        """Bind UI elements properties."""
        flags = GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE
        obj1 = self.get_doc_ui(elem1) if isinstance(elem1, str) else elem1
        obj2 = self.get_doc_ui(elem2) if isinstance(elem2, str) else elem2
        obj1.bind_property(prop1, obj2, prop2, flags)

    def get_context(self):   # override
        """Get document context, used to save/restore state."""
        ctx = super().get_context()
        ctx['goto'] = self.get_cursor_iter().get_line() + 1
        ctx['marks'] = self.get_marks_pos(None)
        if lang := self.buffer.get_language():
            ctx['lang'] = lang.get_id()
        return ctx

    def context_clear(self):
        """Clear source code context."""
        self.hints = {}

    def context_save(self):
        """Save current source code context."""
        self.hints = self.get_context() | self.hints

    def context_restore(self):
        """Restore saved source code context."""
        if self.is_closing():
            return
        self.buffer.remove_source_marks(*self.buffer.get_bounds(), None)
        for pos, category in self.hints.get('marks', []):
            itr = self.buffer.get_iter_at_line(pos - 1).iter
            self.buffer.create_source_mark(None, category, itr)
        if lang_id := self.hints.get('lang', None):
            self.set_lang(GtkSource.LanguageManager.get_default().get_language(lang_id))
        self.go_to(self.hints.get('goto', 1))
        self.context_clear()
        self.goto_undo = self.goto_undo[-1:]
        self.last_edit_line = 0

    def set_interactive(self, active):
        """Allow or block interactive features."""
        if active and self.wbench.app.get_setting('core', 'interactive_completion', bool, True):
            self.view.get_completion().unblock_interactive()
        else:
            self.view.get_completion().block_interactive()
        self.is_loading = not active

    def set_lang(self, lang):
        """Set syntax highlight language."""
        if lang == self.buffer.get_language():
            return
        self.buffer.set_language(lang)
        if lang is not None:
            self.get_doc_ui('ed_label_lang').set_text(lang.get_name())
            self.reload_symbols()
        else:
            self.get_doc_ui('ed_label_lang').set_text(_("Text"))

    def guess_lang(self):
        """Guess syntax highlight language."""
        if self.buffer.get_language() is None:
            gfile = self.get_gfile()
            lm = GtkSource.LanguageManager.get_default()
            for glob, lang_id in self.wbench.get_config('associate', dict, {}).items():
                if isinstance(lang_id, str) and fnmatch.fnmatch(self.tablabel, glob):
                    if lang := lm.get_language(lang_id):
                        break
            else:
                lang = lm.guess_language(gfile.get_basename(), ed_get_gfile_mimetype(gfile))
            self.set_lang(lang)

    def set_text_and_lock(self, title, text, **hints):
        """Set fixed text in editor, then lock it."""
        if self.get_gfile() is None:
            self.spin_start()
            self.set_interactive(False)
            self.view.set_editable(False)
            self.buffer.begin_irreversible_action()
            self.buffer.set_text(text)
            self.buffer.set_modified(False)
            self.buffer.end_irreversible_action()
            self.tablabel = title
            self.update_properties()
            self.hints.update(hints)
            self.set_interactive(True)
            GLib.idle_add(self.context_restore, priority=GLib.PRIORITY_LOW)
            GLib.idle_add(self.spin_stop, priority=GLib.PRIORITY_LOW)

    def set_right_margin(self):
        """Manage right margin visibility."""
        mll = self.wbench.config.get_file_config(self.get_gfile(), 'max_line_length')
        mll = int(mll) if mll.isdecimal() else 0
        if pos := mll or self.wbench.get_config('margin_pos', int):
            self.view.set_right_margin_position(pos)
        self.view.set_show_right_margin(self.wbench.get_config('margin_show', bool, mll > 0))

    def setup_extra_menu(self):
        """Extend context menu."""
        menu = self.view.get_extra_menu()
        menu.append(_("Clear _Search Highlight"), 'win.clearhighlight')

    def focus(self):   # override
        """Set focus on document."""
        self.view.grab_focus()
        self.view.reset_cursor_blink()

    def is_modified(self):   # override
        """Check if document has unsaved modifications."""
        return self.buffer.get_modified()

    def get_modif_actions(self):   # override
        """Get if undo and redo are possible."""
        return self.buffer.get_can_undo(), self.buffer.get_can_redo()

    def get_gfile(self):   # override
        """Get the document's GFile."""
        return self.sfile.get_location()

    def reload_symbols(self):   # override
        """Reload symbols from document's file."""
        if gfile := self.get_gfile():
            fpath = gfile.peek_path() or self.tablabel
        else:
            fpath = self.tablabel
        if lang := self.buffer.get_language():
            lang_id = lang.get_id()
        else:
            lang_id = ""
        self.ctags.parse(fpath, lang_id, self.buffer.get_text(*self.buffer.get_bounds(), False))

    def get_symbols(self):   # override
        """Get symbols of document's file (must have been loaded before)."""
        return self.ctags.get_symbols()

    def get_cursor_iter(self):
        """Get the GtkTextIter at cursor position."""
        return self.buffer.get_iter_at_mark(self.buffer.get_insert())

    def get_has_selection_2(self):
        """Check if editor has selected text, even if selection focus was lost."""
        return self.buffer.get_has_selection() or self.unfocus_selection is not None

    def get_selection_bounds_2(self):
        """Get selection bounds, even if selection focus was lost."""
        if not self.buffer.get_has_selection() and self.unfocus_selection is not None:
            obeg, oend = sorted(self.unfocus_selection)
            return self.buffer.get_iter_at_offset(obeg), self.buffer.get_iter_at_offset(oend)
        return self.buffer.get_selection_bounds()

    def get_selected_text(self):   # override
        """Get selected text in document."""
        if self.get_has_selection_2():
            ibeg, iend = self.get_selection_bounds_2()
            return self.buffer.get_text(ibeg, iend, False)
        return None

    def get_text_at_pos(self, pos):   # override
        """Get text at position."""
        if pos:
            ibeg = self.buffer.get_iter_at_line(pos - 1).iter
            iend = ibeg.copy()
            iend.forward_to_line_end()
            return self.buffer.get_text(ibeg, iend, False)
        return None

    def get_pos(self):   # override
        """Get current position in document."""
        return self.get_cursor_iter().get_line() + 1

    def get_newline(self):
        """Get document's newline string."""
        txt = {'cr': "\r", 'lf': "\n", 'cr-lf': "\r\n"}
        return txt[self.sfile.get_newline_type().value_nick]

    def replace_selection(self, txt):
        """Insert text in place of current selection."""
        self.buffer.begin_user_action()
        editable = self.view.get_editable()
        if self.get_has_selection_2():
            ibeg, iend = self.get_selection_bounds_2()
            self.buffer.delete_interactive(ibeg, iend, editable)
        else:
            ibeg = self.get_cursor_iter()
        self.buffer.insert_interactive(ibeg, txt, -1, editable)
        self.buffer.end_user_action()

    def filter_lines(self, regex, invert=False, /):
        """Remove lines not matching regex."""
        pattern = re.compile(regex)
        editable = self.view.get_editable()
        self.action_blockmode(enable=False)
        self.buffer.begin_user_action()
        line = 0
        while line < self.buffer.get_line_count():
            ibeg = self.buffer.get_iter_at_line(line).iter
            iend = self.buffer.get_iter_at_line(line + 1).iter
            if ibeg.is_end():
                break
            match = pattern.search(self.buffer.get_text(ibeg, iend, False)) is not None
            if match != invert or not self.buffer.delete_interactive(ibeg, iend, editable):
                line += 1
        self.buffer.end_user_action()

    def scroll_to_cursor(self):
        """Scroll editor content to cursor's position."""
        if self.is_closing():
            return
        if not self.view.is_drawable():
            self.scroll_pending = True
            return
        self.scroll_pending = False
        self.unfocus_selection = None
        cr = self.view.get_iter_location(self.get_cursor_iter())
        vr = self.view.get_visible_rect()
        if vr.y < cr.y + cr.height and cr.y < vr.y + vr.height:
            self.view.scroll_to_mark(self.buffer.get_insert(), 0.1875, False, 1.0, 0.5)
        else:
            self.view.scroll_to_mark(self.buffer.get_insert(), 0.0, True, 1.0, 0.25)

    def update_indent(self):
        """Update indentation settings."""
        if self.is_indent_configured or self.is_closing():
            return   # already done
        # Detect from config
        if not self.wbench.config.setup_sourceview(self.view, self.get_gfile()):
            # Detect from source code itself
            self.detect_indent()
        self.is_indent_configured = True

    def detect_indent(self):
        """Detect code indentation."""
        tabs, last_space, space_diff = 0, 0, {}
        full = self.buffer.get_text(*self.buffer.get_bounds(), False)
        for line in full.splitlines():   # count spaces/tabs for each line
            if not line:
                pass
            elif line.startswith("\t"):
                tabs += 1
            else:
                s = len(re.match(r" *", line).group(0))
                if s > 1:
                    d = max(s - last_space, 0)
                    space_diff[d] = space_diff.get(d, 0) + 1
                last_space = s
        if 2 * tabs > sum(space_diff.values()):   # at least 33% of tabs
            self.view.set_insert_spaces_instead_of_tabs(False)
            self.view.set_tab_width(8)
            self.view.set_indent_width(-1)   # follow tab_width
        elif candidates := [(_v, -_k) for _k, _v in space_diff.items() if _k in range(2, 9)]:
            m = max(candidates)
            self.view.set_insert_spaces_instead_of_tabs(True)
            self.view.set_tab_width(-m[1])
            self.view.set_indent_width(-1)   # follow tab_width

    def update_properties(self):
        """Update file properties in header and status bars."""
        self.update_title()
        nl = self.sfile.get_newline_type().value_nick.upper().replace('-', '')
        enc = self.sfile.get_encoding() or GtkSource.Encoding.get_utf8()
        self.get_doc_ui('ed_label_newline').set_text(nl)
        self.get_doc_ui('ed_label_charset').set_text(enc.get_charset())
        self.get_doc_ui('ed_image_readonly').set_visible(not self.view.get_editable())

    def update_breadcrumb(self, line):
        """Update breadcrumb bar with symbol."""
        if self.is_closing():
            return
        sym = self.ctags.get_symbol_at_line(line)
        if sym is not self.sym_at_cur:
            self.sym_at_cur = sym
            k_markup = """<span color="{}"><b><tt><i>{}</i></tt></b></span>"""
            sep = f"""<span alpha="50%"><tt>{"&lt;" if ed_is_rtl() else "&gt;"}</tt></span>"""
            crumbs = []
            while sym is not None:
                kl = sym['kl']
                crumbs += [sep] if crumbs else []
                crumbs.append(GLib.markup_escape_text(sym['name']))
                crumbs.append(k_markup.format(ed_get_kind_color(kl), kl))
                sym = sym.parent
            self.breadcrumb.set_markup("\u2002".join(crumbs if ed_is_rtl() else reversed(crumbs)))

    def update_search_settings(self, /, *, find="", wb=False, cs=False, es=False, rx=False, **kwargs):
        """Update search settings with provided parameters."""
        settings = self.search.get_settings()
        if es:
            find = GtkSource.utils_unescape_search_text(find)
        if find != settings.get_search_text():
            settings.set_search_text(find or None)
        if cs != settings.get_case_sensitive():
            settings.set_case_sensitive(cs)
        if wb != settings.get_at_word_boundaries():
            settings.set_at_word_boundaries(wb)
        if rx != settings.get_regex_enabled():
            settings.set_regex_enabled(rx)

    def update_theme(self):   # override
        """Updated theme."""
        if scheme := ed_get_source_theme_scheme(self.wbench.app):
            match_style = scheme.get_style('quick-highlight-match')
            self.buffer.set_style_scheme(scheme)
            self.autohl.set_highlight(False)   # set_match_style() gets ignored if highlight enabled
            self.autohl.set_match_style(match_style)
            self.autohl.set_highlight(True)
        GLib.idle_add(self.on_ctags_update)

    def spin_start(self):
        """Start loading indication spinner."""
        if self.spin_requests == 0:
            self.get_doc_ui('ed_spinner_doc').set_visible(True)
            self.get_doc_ui('ed_spinner_doc').start()
        self.spin_requests += 1

    def spin_stop(self):
        """Stop loading indication spinner."""
        if self.is_closing():
            return
        if self.spin_requests > 0:
            self.spin_requests -= 1
        if self.spin_requests == 0:
            self.get_doc_ui('ed_spinner_doc').stop()
            self.get_doc_ui('ed_spinner_doc').set_visible(False)

    def push_goto_history(self, itr):
        """Add iter position to goto history."""
        line = itr.get_line()
        self.goto_undo += reversed(self.goto_redo)
        while self.goto_undo and self.buffer.get_iter_at_offset(self.goto_undo[-1]).get_line() == line:
            self.goto_undo.pop()
        self.goto_undo.append(itr.get_offset())
        self.goto_redo = []

    def shift_goto_history(self, offset, size):
        """Shift positions in goto history on edits."""
        for i, o in enumerate(self.goto_undo):
            if offset < o:
                self.goto_undo[i] += size
        for i, o in enumerate(self.goto_redo):
            if offset < o:
                self.goto_redo[i] += size

    def go_to(self, pos):   # override
        """Go to position in document content."""
        if pos:
            itr = self.buffer.get_iter_at_line(pos - 1).iter
            self.push_goto_history(self.get_cursor_iter())
            self.push_goto_history(itr)
            self.buffer.place_cursor(itr)
            if not self.scroll_pending:
                self.scroll_to_cursor()

    def loop_marks(self):
        """Loop over all marks."""
        itr = self.buffer.get_start_iter()
        m0 = m = self.buffer.create_source_mark(None, '', itr)
        yield from self.buffer.get_source_marks_at_iter(itr, None)
        while m := m.next(None):   # GtkSource bug, must use None as param
            yield m
        self.buffer.delete_mark(m0)

    def get_marks_pos(self, category):
        """Get list of marks by category."""
        marks = []
        for m in self.loop_marks():
            c = m.get_category()
            if c and category in (None, c):
                marks.append((self.buffer.get_iter_at_mark(m).get_line() + 1, c))
        return marks

    def toggle_mark(self, category, /, *, line=None):   # override
        """Mark current position in document."""
        if line is None:
            line = self.get_cursor_iter().get_line()
        if marks := self.buffer.get_source_marks_at_line(line, category):
            for m in marks:
                self.buffer.delete_mark(m)
        else:
            itr = self.buffer.get_iter_at_line(line).iter
            self.buffer.create_source_mark(None, category, itr)

    def goto_next_mark(self, category):   # override
        """Move to next marked position."""
        cur_pos = self.get_cursor_iter().get_line() + 1
        if marks := self.get_marks_pos(category):
            nextmarks = [_m for _m in marks if cur_pos < _m[0]] + [marks[0]]
            self.go_to(nextmarks[0][0])

    def autoclose(self, left, around):
        """Insert right counterpart of left input."""
        if not self.view.get_editable():
            return
        right = ED_AUTOCLOSE_PAIRS[left]
        if not around:
            if left == right:
                return
            c = self.get_cursor_iter().get_char() or " "
            if not c.isspace() and c not in ED_AUTOCLOSE_SKIP:
                return
        self.view.emit('insert-at-cursor', around + right)
        self.view.emit('move-cursor', Gtk.MovementStep.LOGICAL_POSITIONS, -1, False)
        if around:
            self.view.emit('move-cursor', Gtk.MovementStep.LOGICAL_POSITIONS, -len(around), True)

    def move_smart_line_home(self, itr):
        """Move iter to its smart line home."""
        ibeg, iend = itr.copy(), itr.copy()
        ibeg.set_line_offset(0)
        iend.forward_to_line_end()
        txt = self.buffer.get_text(ibeg, iend, False) + "#"
        sp = min(_i for _i, _c in enumerate(txt) if _c not in " \t")
        off = 0 if sp == itr.get_visible_line_offset() else sp
        itr.set_visible_line_offset(off)

    def set_iter_line_offset(self, itr, off):
        """Set the line offset of a TextIter, with safe range checks."""
        c = itr.get_chars_in_line() - 1
        itr.set_line_offset(max(0, min(c, off)))

    def block_insert(self, txt):
        """Insert text into buffer at block-cursor marks."""
        editable = self.view.get_editable()
        self.buffer.begin_user_action()
        for mark in self.blockmode.marks[:-1]:
            itr = self.buffer.get_iter_at_mark(mark)
            self.buffer.insert_interactive(itr, txt, -1, editable)
        self.buffer.end_user_action()

    def block_delete(self, count):
        """Remove text from buffer at block-cursor marks."""
        editable = self.view.get_editable()
        self.buffer.begin_user_action()
        for mark in self.blockmode.marks[:-1]:
            i0 = self.buffer.get_iter_at_mark(mark)
            i1 = self.buffer.get_iter_at_mark(mark)
            self.set_iter_line_offset(i1, i0.get_line_offset() + count)
            self.buffer.delete_interactive(i0, i1, editable)
        self.buffer.end_user_action()

    def find(self, **settings):   # override
        """Find text in document."""
        direction = settings.get('direction', 0)
        self.update_search_settings(**settings)
        res = False
        icur = self.get_cursor_iter()
        if direction < 0:
            istart = self.buffer.get_selection_bounds()[0] if self.buffer.get_has_selection() else icur
            res, ibeg, iend, wrap = self.search.backward(istart)
        elif direction > 0:
            istart = self.buffer.get_selection_bounds()[1] if self.buffer.get_has_selection() else icur
            res, ibeg, iend, wrap = self.search.forward(istart)
        if res:
            self.buffer.select_range(iend, ibeg)
            self.scroll_to_cursor()
            GLib.idle_add(self.on_find_result, priority=GLib.PRIORITY_LOW)
        elif self.search.get_settings().get_search_text():
            if err := self.search.get_regex_error():
                self.wbench.notify_search_failure(err)
            GLib.idle_add(self.on_find_result, priority=GLib.PRIORITY_LOW)
        else:
            self.focus()

    def replace(self, **settings):   # override
        """Replace current occurrence of text in document."""
        repl = settings.get('repl', "")
        success = False
        if self.buffer.get_has_selection():
            self.update_search_settings(**settings)
            ibeg, iend = self.buffer.get_selection_bounds()
            if settings.get('es', False):
                repl = GtkSource.utils_unescape_search_text(repl)
            success = self.search.replace(ibeg, iend, repl, -1)
        if success:
            self.find(direction=+1, **settings)
        else:
            self.wbench.notify_search_failure(self.search.get_regex_error())
            self.notifier.status(_("Replace failed!"), False)

    def replace_all(self, **settings):   # override
        """Replace all occurrences of text in document."""
        repl = settings.get('repl', "")
        self.update_search_settings(**settings)
        if settings.get('es', False):
            repl = GtkSource.utils_unescape_search_text(repl)
        n = self.search.replace_all(repl, -1)
        if err := self.search.get_regex_error():
            self.wbench.notify_search_failure(err)
        self.notifier.status(_("Replaced {} occurrences").format(n), False)

    def undo(self):   # override
        """Undo last modification."""
        if self.buffer.get_can_undo():
            self.buffer.emit('undo')
            GLib.idle_add(self.scroll_to_cursor)

    def redo(self):   # override
        """Redo last undone modification."""
        if self.buffer.get_can_redo():
            self.buffer.emit('redo')
            GLib.idle_add(self.scroll_to_cursor)

    def load(self, charset="", /):
        """Load GFile's content in editor."""
        gfile = self.get_gfile()
        if not self.is_loading and gfile is not None:
            self.view.set_editable(False)   # lock during load
            self.set_interactive(False)
            self.context_save()
            self.unfocus_selection = None
            self.action_blockmode(enable=False)
            fl = GtkSource.FileLoader.new(self.buffer, self.sfile)
            enc = GtkSource.Encoding.get_from_charset(charset.upper())
            self.wbench.config.setup_fileloader(fl, gfile, enc)
            fl.load_async(0, self.wbench.get_cancellable(), None, None, self.on_load_result)
            self.spin_start()

    def save(self, close=False, /):   # override
        """Save document content in current GFile."""
        if gfile := self.get_gfile():
            fs = GtkSource.FileSaver.new(self.buffer, self.sfile)
            self.save_common(fs, gfile, close)

    def save_as(self, gfile, close=False, /):   # override
        """Save document content in new GFile."""
        if self.get_gfile() != gfile:
            self.stop_monitor()
        fs = GtkSource.FileSaver.new_with_target(self.buffer, self.sfile, gfile)
        self.wbench.config.setup_filesaver(fs, gfile)
        self.save_common(fs, gfile, close)

    def save_common(self, fs, gfile, close):
        """Common processing for saving file."""
        if self.is_loading:
            self.notifier.error(_("File could not be saved!"), _("Loading in progress, please retry later"))
            return
        ed_extensions_notify('doc_save', self, gfile)
        if self.wbench.config.get_file_config(gfile, 'trim_trailing_whitespace') == "true":
            ss = GtkSource.SearchSettings(search_text=r"[[:blank:]]+$", at_word_boundaries=False,
                                          case_sensitive=False, regex_enabled=True, wrap_around=True)
            sc = GtkSource.SearchContext.new(self.buffer, ss)
            if sc.forward(self.buffer.get_start_iter())[0]:
                sc.replace_all("", -1)
        if self.wbench.config.get_file_config(gfile, 'insert_final_newline') == "true":
            itr = self.buffer.get_end_iter()
            if not itr.starts_line():
                self.buffer.insert_interactive(itr, self.get_newline(), -1, self.view.get_editable())
        flags = GtkSource.FileSaverFlags.IGNORE_INVALID_CHARS | GtkSource.FileSaverFlags.IGNORE_MODIFICATION_TIME
        if self.wbench.app.get_setting('core', 'save_backup', bool, False):
            flags |= GtkSource.FileSaverFlags.CREATE_BACKUP
        fs.set_flags(flags)
        fs.save_async(0, self.wbench.get_cancellable(), None, None, self.on_save_result, close)
        self.spin_start()

    def check_changed(self):
        """Check if the document's file was changed on disk, reload content if necessary."""
        if self.is_closing():
            return
        self.sfile.check_file_on_disk()
        if self.sfile.is_deleted():
            self.buffer.set_modified(True)
            with self.notifier.set_category('changed'):
                self.save_conflict = True
                msg = _("File was removed! Close or keep content?")
                self.notifier.question(msg, None, _("Close"), self.on_close_response)
        elif self.sfile.is_externally_modified():
            if self.is_modified() or self.is_loading:
                with self.notifier.set_category('changed'):
                    self.save_conflict = True
                    msg = _("File was externally modified! Reload or keep content?")
                    self.notifier.question(msg, None, _("Reload"), self.on_reload_response)
            else:
                self.notifier.status(_("Auto-reload '{}'").format(self.tablabel))
                self.load()   # auto sync

    def action_cutline(self):
        """Cut current line, add it to clipboard."""
        line = self.get_cursor_iter().get_line()
        ibeg = self.buffer.get_iter_at_line(line).iter
        iend = self.buffer.get_iter_at_line(line + 1).iter
        txt = self.buffer.get_text(ibeg, iend, False)
        ed_set_clipboard_text(txt)
        self.buffer.delete_interactive(ibeg, iend, self.view.get_editable())

    def action_dupline(self):
        """Duplicate current line."""
        line = self.get_cursor_iter().get_line()
        ibeg = self.buffer.get_iter_at_line(line).iter
        iend = self.buffer.get_iter_at_line(line + 1).iter
        txt = self.buffer.get_text(ibeg, iend, False)
        self.buffer.insert_interactive(iend, txt, -1, self.view.get_editable())

    def action_blockmode(self, /, *, enable=None, pad=False):
        """Switch block mode."""
        if enable is None:
            enable = not self.blockmode.enabled
        if enable == self.blockmode.enabled:
            return
        if enable:
            if not self.buffer.get_has_selection():
                return
            ibeg, iend = self.buffer.get_selection_bounds()
            line0, line1 = ibeg.get_line(), iend.get_line()
            off0, off1 = sorted([ibeg.get_line_offset(), iend.get_line_offset()])
            if line0 >= line1:
                return
            editable = self.view.get_editable()
            # Pad lines or squash columns
            self.buffer.begin_user_action()
            for line in reversed(range(line0, line1 + 1)):
                i0 = self.buffer.get_iter_at_line_offset(line, off0).iter
                i1 = self.buffer.get_iter_at_line_offset(line, off1).iter
                if pad:
                    delta = off1 - i1.get_line_offset()
                    mark = self.buffer.create_mark(None, i1, False)
                    self.buffer.insert_interactive(i1, " " * delta, -1, editable)
                elif off0 <= i1.get_line_offset():
                    mark = self.buffer.create_mark(None, i0, False)
                    self.buffer.delete_interactive(i0, i1, editable)
                else:
                    continue
                mark.set_visible(True)
                self.blockmode.marks.append(mark)
            self.buffer.place_cursor(self.buffer.get_iter_at_mark(mark))
            self.buffer.end_user_action()
        else:
            for mark in self.blockmode.marks:
                mark.set_visible(False)   # fix glitch
                self.buffer.delete_mark(mark)
            self.blockmode.marks.clear()
        self.blockmode.enabled = enable
        GLib.idle_add(self.on_cursor_position)

    def action_blockcopy(self):
        """Block copy selected text into clipboard."""
        if not self.buffer.get_has_selection():
            return
        ibeg, iend = self.buffer.get_selection_bounds()
        line0, line1 = ibeg.get_line(), iend.get_line()
        off0, off1 = sorted([ibeg.get_line_offset(), iend.get_line_offset()])
        txt = []
        for line in range(line0, line1 + 1):
            i0 = self.buffer.get_iter_at_line_offset(line, off0).iter
            i1 = self.buffer.get_iter_at_line_offset(line, off1).iter
            txt.append(self.buffer.get_text(i0, i1, False).rstrip("\r\n"))
        ed_set_clipboard_text(self.get_newline().join(txt))

    def action_blockpaste(self):
        """Block paste clipboard text into selected box."""
        ed_get_clipboard_text(self.on_blockpaste_clipboard)

    def action_comment(self):
        """Comment out selected text."""
        editable = self.view.get_editable()
        if self.buffer.get_has_selection():
            off2 = min(_i for _l in (self.get_selected_text() + "#").splitlines()
                          for _i, _c in enumerate(_l) if not _c.isspace())   # detect block indent
            ibeg, iend = self.buffer.get_selection_bounds()
        else:
            off2 = 0
            ibeg = iend = self.get_cursor_iter()
        line0, line1 = ibeg.get_line(), iend.get_line()
        off0 = off2 if ibeg.starts_line() else ibeg.get_line_offset()
        cs, ce, cl = ed_get_comment_delimiters(self.buffer.get_language())
        # Insert comment delimiters
        self.buffer.begin_user_action()
        if ce:   # comment end
            self.buffer.insert_interactive(iend, " " + ce, -1, editable)
        if cl:
            for line in reversed(range(line0 + 1, line1 + 1)):
                # Insert line comment at indentation position
                i = self.buffer.get_iter_at_line_offset(line, off2).iter
                if off2 == i.get_line_offset():   # ignore empty lines
                    cl_s = "" if i.ends_line() else " "
                    self.buffer.insert_interactive(i, cl + cl_s, -1, editable)
        if cs:   # comment start
            i = self.buffer.get_iter_at_line_offset(line0, off0).iter
            self.buffer.place_cursor(i)
            self.buffer.insert_interactive(i, cs + " ", -1, editable)
        self.buffer.end_user_action()

    def action_docsettings(self):
        """Show document settings."""
        self.get_doc_ui('ed_button_docsettings').popup()

    def action_goprev(self):
        """Go to previous position."""
        if self.goto_undo:
            off = self.goto_undo.pop()
            self.goto_redo.append(off)
            itr = self.buffer.get_iter_at_offset(off)
            if self.get_cursor_iter().get_line() == itr.get_line():
                self.action_goprev()
            else:
                self.buffer.place_cursor(itr)
                self.scroll_to_cursor()

    def action_gonext(self):
        """Go to next position."""
        if self.goto_redo:
            off = self.goto_redo.pop()
            self.goto_undo.append(off)
            itr = self.buffer.get_iter_at_offset(off)
            if self.get_cursor_iter().get_line() == itr.get_line():
                self.action_gonext()
            else:
                self.buffer.place_cursor(itr)
                self.scroll_to_cursor()

    def action_clearhighlight(self):
        """Clear search highlight."""
        self.find(direction=0)

    def action_jumpdef(self):
        """Jump to symbol definition."""
        name = self.get_selected_text() or self.jumpdef
        if name and name.isprintable():
            self.jumpdef = name
            sympos = sorted(_s['line'] for _s in self.ctags.find_symbol(name) if _s['line'] > 0)
            line = self.get_cursor_iter().get_line() + 1
            if sympos:
                nextlines = [_l for _l in sympos if _l > line] + [sympos[0]]
                if nextlines[0] != line:
                    self.go_to(nextlines[0])
                nb = 1 + sympos.index(nextlines[0])
                count = len(sympos)
                self.notifier.status(_("Symbol {0} / {1}").format(nb, count), False)
            else:
                self.notifier.status(_("Symbol not found!"), False)
        else:
            self.jumpdef = None

    def action_minimap(self):
        """Add or remove minimap according to config."""
        box = self.get_doc_ui('ed_box_views')
        view_container = self.view.get_ancestor(Gtk.ScrolledWindow.__gtype__)
        mmaps = [_w for _w in box if isinstance(_w, GtkSource.Map)]
        if self.wbench.get_config('minimap_show', bool, False):
            if not mmaps:
                srcmap = GtkSource.Map(view=self.view, pixels_below_lines=1)
                box.insert_child_after(srcmap, view_container)
        else:
            for mm in mmaps:
                box.remove(mm)

    def on_activate(self):   # override
        """Notification when document is set as the active one."""
        if self.scroll_pending and not self.is_closing():
            GLib.timeout_add(200, self.scroll_to_cursor, priority=GLib.PRIORITY_LOW)

    def on_focus_in(self, event):
        """Callback on editor being focused in."""
        if self.is_closing():
            return
        if not self.buffer.get_has_selection():
            if self.unfocus_selection is not None:
                oins, obnd = self.unfocus_selection
                self.buffer.select_range(*map(self.buffer.get_iter_at_offset, (oins, obnd)))
                self.unfocus_selection = None

    def on_focus_out(self, event):
        """Callback on editor being focused out."""
        if self.is_closing():
            return
        if self.buffer.get_has_selection():
            iins = self.buffer.get_iter_at_mark(self.buffer.get_insert())
            ibnd = self.buffer.get_iter_at_mark(self.buffer.get_selection_bound())
            self.unfocus_selection = (iins.get_offset(), ibnd.get_offset())
        else:
            self.unfocus_selection = None

    def on_load_result(self, loader, res):
        """Callback on file content loading to editor."""
        if self.is_closing():
            return
        try:
            success = True if loader is None else loader.load_finish(res)
        except GLib.Error as e:
            if e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.NOT_FOUND):
                success = True
                if initdata := self.hints.get('initdata', None):
                    self.buffer.set_text(initdata)
                else:
                    self.notifier.info(_("File not found!"), None)
            else:
                success = False
                self.notifier.error(_("File load error!"), f"{e.message}")
        self.view.set_editable(success and not self.sfile.is_readonly())
        if success:
            self.notifier.clear_category('changed')
            self.save_conflict = False
        self.buffer.place_cursor(self.buffer.get_start_iter())
        self.guess_lang()
        self.update_properties()
        self.reload_symbols()
        self.set_interactive(True)
        self.notifier.status((_("'{}' load success") if success else
                              _("'{}' load failure")).format(self.tablabel))
        self.start_monitor()
        ed_extensions_notify('doc_loaded', self)
        GLib.idle_add(self.update_indent)
        GLib.idle_add(self.context_restore, priority=GLib.PRIORITY_LOW)   # textview updated in idle, queue afterwards
        GLib.idle_add(self.spin_stop, priority=GLib.PRIORITY_LOW)

    def on_save_result(self, saver, res, close):
        """Callback on document content saving to file."""
        if self.is_closing():
            return
        try:
            success = saver.save_finish(res)
        except GLib.Error as e:
            success = False
            self.notifier.error(_("File save error!"), f"{e.message}")
        if success:
            self.view.set_editable(not self.sfile.is_readonly())
            self.notifier.clear_category('changed')
            self.save_conflict = False
            self.guess_lang()
            self.update_properties()
            self.reload_symbols()
        self.notifier.status((_("'{}' save success") if success else
                              _("'{}' save failure")).format(self.tablabel))
        if success and close:
            self.wbench.close_doc(self)
        else:
            self.start_monitor()
            GLib.idle_add(self.spin_stop, priority=GLib.PRIORITY_LOW)

    def on_find_result(self):
        """Callback on timeout to check position of 'find' result."""
        count = self.search.get_occurrences_count()
        if self.buffer.get_has_selection():
            ibeg, iend = self.buffer.get_selection_bounds()
            pos = self.search.get_occurrence_position(ibeg, iend)
            if pos > 0:
                self.notifier.status(_("Find {0} / {1}").format(pos, count), False)
                return
        self.notifier.status(_("Found {} occurrences").format(count), False)

    def on_extern_changed(self, event):   # override
        """Callback on external changes reported by the file monitor."""
        # Note: a file save typically emits a DELETED + CHANGES_DONE_HINT burst
        timeout = 1000 if event == Gio.FileMonitorEvent.CHANGES_DONE_HINT else 3000
        GLib.timeout_add(timeout, self.check_changed, priority=GLib.PRIORITY_LOW)

    def on_reload_response(self, accepted):
        """Callback on user response to reload question."""
        if accepted:
            self.load()

    def on_close_response(self, accepted):
        """Callback on user response to close question."""
        if accepted:
            self.wbench.close_doc_request(self.get_widget())

    def on_modif_actions(self, gobj, pspec):
        """Callback on can-undo/can-redo change."""
        self.wbench.update_modif_actions(self)

    def on_modified_changed(self, buf):
        """Callback on GtkTextBuffer changed flag."""
        self.update_title()

    def on_line_mark_activated(self, w, itr, button, state, n):
        """Callback on gutter mark activation."""
        if button == Gdk.BUTTON_PRIMARY:
            self.toggle_mark('bookmark', line=itr.get_line())

    def on_ctags_update(self):
        """Callback on symbols parsing finished."""
        self.jumpdef = None
        self.sym_at_cur = None
        if not self.is_closing():
            ed_extensions_notify('doc_symbols', self)
            self.update_breadcrumb(1 + self.get_cursor_iter().get_line())
            GLib.idle_add(self.wbench.update_symbols_for_doc, self)
            GLib.idle_add(self.view.get_completion().hide, priority=GLib.PRIORITY_LOW)

    def on_button_pressed(self, event, n, x, y):
        """Callback on button pressed event."""
        # Move cursor so context menu will be relevant for click location
        if self.buffer.get_selection_bounds() or n != 1:  ## triggers_context_menu()
            return
        bx, by = self.view.window_to_buffer_coords(Gtk.TextWindowType.WIDGET, x, y)
        itr = self.view.get_iter_at_location(bx, by).iter
        self.buffer.select_range(itr, itr)

    def on_key_press(self, event, keyval, keycode, state):
        """Callback on key press event."""
        if keyval < 0x7F and not self.blockmode.enabled:
            c = chr(keyval)
            if c in ED_AUTOCLOSE_SKIP and c == self.get_cursor_iter().get_char():
                self.view.emit('move-cursor', Gtk.MovementStep.LOGICAL_POSITIONS, 1, False)
                return Gdk.EVENT_STOP
            if c in ED_AUTOCLOSE_PAIRS:
                GLib.idle_add(self.autoclose, c, self.get_selected_text() or "", priority=GLib.PRIORITY_DEFAULT)
        return Gdk.EVENT_PROPAGATE

    def on_insert_text(self, buf, where, txt, txtlen):
        """Callback on text inserted into buffer."""
        self.unfocus_selection = None
        self.shift_goto_history(where.get_offset(), txtlen)
        if self.blockmode.enabled:   # block mode, insert text at other cursors too
            if self.view.get_editable() and where.equal(self.get_cursor_iter()):
                # Async insert, to not invalidate 'where'
                GLib.idle_add(self.block_insert, txt, priority=GLib.PRIORITY_DEFAULT)
            else:
                return
        # Track changes
        line = where.get_line()
        if abs(self.last_edit_line - line) >= 10:
            self.push_goto_history(where)
        self.last_edit_line = line

    def on_delete_range(self, buf, ibeg, iend):
        """Callback on text removed from buffer."""
        self.unfocus_selection = None
        obeg = ibeg.get_offset()
        self.shift_goto_history(obeg, obeg - iend.get_offset())
        if self.blockmode.enabled and self.view.get_editable():   # block mode, delete text at other cursors too
            cur = self.get_cursor_iter()
            delta = iend.get_line_offset() - ibeg.get_line_offset()
            if ibeg.equal(cur):   # 'delete' key
                if cur.ends_line():
                    self.action_blockmode(enable=False)
                else:
                    GLib.idle_add(self.block_delete, delta, priority=GLib.PRIORITY_DEFAULT)
            elif iend.equal(cur):   # 'backspace' key
                if cur.starts_line():
                    self.action_blockmode(enable=False)
                else:
                    GLib.idle_add(self.block_delete, -delta, priority=GLib.PRIORITY_DEFAULT)

    def on_delete_range_after(self, buf, ibeg, iend):
        """Callback on text removed from buffer, after default handler."""
        line = ibeg.get_line()
        # Make sure all source marks are at start of lines, as required by the gutter
        if marks := self.buffer.get_source_marks_at_line(line, None):
            itr = self.buffer.get_iter_at_line(line).iter
            for m in marks:
                if not self.buffer.get_iter_at_mark(m).equal(itr):
                    self.buffer.move_mark(m, itr)
        # Track changes
        if abs(self.last_edit_line - line) >= 10:
            self.push_goto_history(ibeg)
        self.last_edit_line = line

    def on_blockpaste_clipboard(self, txt):
        """Callback on clipboard text available for blockpasting."""
        if txt is not None:
            editable = self.view.get_editable()
            ibeg, iend = self.buffer.get_selection_bounds() or (self.get_cursor_iter(),) * 2
            line0, line1 = ibeg.get_line(), iend.get_line()
            off0, off1 = sorted([ibeg.get_line_offset(), iend.get_line_offset()])
            repl = txt.splitlines()
            if len(repl) == 1:   # single line, clone clipboard at each line
                repl = (1 + line1 - line0) * repl
            else:   # multi-lines, complete extra lines with blanks
                repl.extend((1 + line1 - line0 - len(repl)) * [""])
            # Paste in each line
            self.buffer.begin_user_action()
            for line in reversed(range(line0, line1 + 1)):
                i0 = self.buffer.get_iter_at_line_offset(line, off0).iter
                i1 = self.buffer.get_iter_at_line_offset(line, off1).iter
                self.buffer.delete_interactive(i0, i1, editable)
                self.buffer.insert_interactive(i0, repl[line-line0], -1, editable)
            self.buffer.end_user_action()

    def on_move_cursor(self, textview, step, count, ext_sel):
        """Callback on cursor move request from keyboard."""
        self.unfocus_selection = None
        if self.blockmode.enabled:   # block mode, move other cursors too
            for mark in self.blockmode.marks:
                itr = self.buffer.get_iter_at_mark(mark)
                match step:
                    case Gtk.MovementStep.VISUAL_POSITIONS:
                        itr.forward_chars(count) if count > 0 else itr.backward_chars(-count)
                    case Gtk.MovementStep.DISPLAY_LINE_ENDS if abs(count) == 1:
                        itr.ends_line() or itr.forward_to_line_end() if count > 0 else self.move_smart_line_home(itr)
                    case Gtk.MovementStep.WORDS:
                        if count > 0:
                            itr.forward_visible_word_ends(count) or itr.ends_line() or itr.forward_to_line_end()
                        else:
                            itr.backward_visible_word_starts(-count) or itr.set_line_offset(0)
                    case Gtk.MovementStep.DISPLAY_LINES:
                        off = itr.get_visible_line_offset()
                        itr.forward_visible_lines(count) if count > 0 else itr.backward_visible_lines(-count)
                        if not off < itr.get_chars_in_line():
                            break
                        itr.set_visible_line_offset(off)
                    case _:
                        break
                self.buffer.move_mark(mark, itr)
            else:
                ml = set(self.buffer.get_iter_at_mark(_m).get_line() for _m in self.blockmode.marks)
                if len(self.blockmode.marks) == len(ml):   # one cursor per line
                    return
            self.action_blockmode(enable=False)

    def on_cursor_position(self, gobj=None, pspec=None, /):
        """Callback on cursor position changed."""
        if self.is_loading:
            return
        itr = self.get_cursor_iter()
        line = 1 + itr.get_line()
        col = 1 + self.view.get_visual_column(itr)
        self.curpos.set_text(_("L {0} : C {1}").replace(" ", "\u202f").format(line, col))
        if self.blockmode.enabled and not itr.equal(self.buffer.get_iter_at_mark(self.blockmode.marks[-1])):
            self.action_blockmode(enable=False)   # cursor out of block, disable block mode
        if self.buffer.get_has_selection():
            ibeg, iend = self.buffer.get_selection_bounds()
            nchars = iend.get_offset() - ibeg.get_offset()
            nlines = 1 + iend.get_line() - ibeg.get_line()
            self.cursel.set_text(f"[{nchars}\u202f/\u202f{nlines}]")
            self.cursel.set_visible(True)
            # Auto-highlight
            txt = self.buffer.get_text(ibeg, iend, False) if nlines == 1 else None
            self.autohl.get_settings().set_search_text(txt)
        elif self.blockmode.enabled:
            self.cursel.set_text(f"[#\u202f/\u202f{len(self.blockmode.marks)}]")
            self.cursel.set_visible(True)
        else:
            self.cursel.set_visible(False)
            self.autohl.get_settings().set_search_text(None)
        GLib.idle_add(self.update_breadcrumb, line, priority=GLib.PRIORITY_LOW)
