#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Document viewers or editors."""

from .base import EdBaseDocument
from .srcedit import EdSrcEditor
from .picview import EdPicViewer


__all__ = ['ed_docs_register_class', 'ed_docs_get_classes']


ed_doc_classes = [EdSrcEditor, EdPicViewer]


def ed_docs_register_class(cls):
    """Register a new document class."""
    assert isinstance(cls, EdBaseDocument)
    if cls not in ed_doc_classes:
        ed_doc_classes.append(cls)


def ed_docs_get_classes():
    """Get supported document classes, sorted by decreasing priority."""
    yield from ed_doc_classes
