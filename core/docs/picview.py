#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Picture viewer."""

from gi.repository import GLib, Gio, Gdk, GdkPixbuf, Gtk

from .base import EdBaseDocument
from ...exts import ed_extensions_notify


__all__ = ['EdPicViewer']


class EdPicViewer(EdBaseDocument):
    """Document container for viewing pictures."""

    def __init__(self, wbench, gfile, **hints):
        super().__init__(wbench)
        self.fit = True
        self.pic = Gtk.Picture(file=gfile, halign='center', valign='center')
        self.pic.update_property([Gtk.AccessibleProperty.LABEL], [_("Document view as image")])
        self.pic.add_css_class('ed-class-picturedoc')
        self.set_main_widget(Gtk.ScrolledWindow(has_frame=False, child=self.pic))
        self.get_widget().add_css_class('ed-class-notebookcontent')
        self.apply_fit()
        # Events
        ev_click = Gtk.GestureClick(name='ed-sclick', button=Gdk.BUTTON_SECONDARY)
        ev_click.connect('pressed', self.on_event_click)
        self.pic.add_controller(ev_click)
        ed_extensions_notify('doc_loaded', self)

    def free(self):
        """Release resources and break circular references."""
        super().free()
        self.pic = None

    @staticmethod
    def is_gfile_supported(gfile):
        """Check if this document class supports the GFile."""
        if gfile is not None:
            uri = gfile.get_uri().lower()
            for fmt in GdkPixbuf.Pixbuf.get_formats():
                for ext in fmt.get_extensions():
                    if uri.endswith(f".{ext}"):
                        return True
        return False

    def get_gfile(self):   # override
        """Get the document's GFile."""
        return self.pic.get_file()

    def save_as(self, gfile, close=False, /):   # override
        """Save document content in new GFile."""
        ed_extensions_notify('doc_save', self, gfile)
        try:
            flags = Gio.FileCopyFlags.OVERWRITE | Gio.FileCopyFlags.TARGET_DEFAULT_PERMS
            self.get_gfile().copy(gfile, flags, self.wbench.get_cancellable(), None)
            self.pic.set_file(gfile)
            self.update_title()
        except GLib.Error as e:
            self.wbench.notifier.error(_("File save error!"), f"{e.message}")

    def apply_fit(self):
        """Set picture fit mode."""
        if self.fit:
            self.pic.set_content_fit(Gtk.ContentFit.SCALE_DOWN)
            self.pic.set_can_shrink(True)
        else:
            self.pic.set_content_fit(Gtk.ContentFit.CONTAIN)
            self.pic.set_can_shrink(False)

    def set_center(self, rx, ry):
        """Center picture around position ratios."""
        scrollwin = self.pic.get_ancestor(Gtk.ScrolledWindow.__gtype__)
        ha = scrollwin.get_hadjustment()
        va = scrollwin.get_vadjustment()
        ha.set_value((ha.get_upper() * rx) - (ha.get_page_size() / 2.0))
        va.set_value((va.get_upper() * ry) - (va.get_page_size() / 2.0))

    def on_event_click(self, event, n, x, y):
        """Callback on click event."""
        if n == 1:
            rx = x / self.pic.get_width()
            ry = y / self.pic.get_height()
            self.fit = not self.fit
            self.apply_fit()
            if not self.fit:
                GLib.idle_add(self.set_center, rx, ry)
            event.set_state(Gtk.EventSequenceState.CLAIMED)
