#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

"""Application actions."""

from gi.repository import GLib, Gio, Gtk, GtkSource

from .project import EdProject
from .ui.dialogs import EdFileChooser
from .utils.filename import EdFileName
from .utils.misc import ed_get_configpath, ed_get_gfile_display_path, ed_set_transient_style
from ..exts import ed_extensions_get_data


__all__ = ['EdActionsManager']


ED_ACTIONS = [
    {'title': _("General"), 'menu': _("_File"), 'shcuts': [
        ('newwbench',         _("New Window"),             ['<Control><Shift>n']),
        ('newdoc',            _("New Empty File"),         ['<Control>n']),
        ('newtab',            None,                        ['<Control>t']),
        ('openfile',          _("Open Files..."),          ['<Control>o']),
        ('openprj',           _("Open Project..."),        ['<Control><Shift>o']),
        ('syncprj',           _("Reload Project"),         []),
        ('save',              _("Save File"),              ['<Control>s']),
        ('saveas',            _("Save File As..."),        []),
        ('saveall',           _("Save All Files"),         ['<Control><Shift>s']),
        ('closedoc',          _("Close File"),             ['<Control>w']),
        ('runcmd',            _("Run Command..."),         ['<Control>r']),
        ('quit',              _("Quit"),                   ['<Control>q']),
    ]},
    {'title': _("Editing"), 'menu': _("_Edit"), 'shcuts': [
        ('undo',              _("Undo"),                   ['<Control>z']),
        ('redo',              _("Redo"),                   ['<Control>y']),
        ('cutline',           _("Cut Line"),               ['<Control>k']),
        ('dupline',           _("Duplicate Line"),         ['<Control>d']),
        ('blockmode-del',     _("Block Mode (Squash)"),    ['<Control>b']),
        ('blockmode-pad',     _("Block Mode (Pad)"),       ['<Control><Shift>b']),
        ('blockcopy',         _("Block Copy"),             ['<Control><Shift>c']),
        ('blockpaste',        _("Block Paste"),            ['<Control><Shift>v']),
        ('comment',           _("Comment Code"),           ['<Control><Shift>a']),
        ('docsettings',       _("File Settings..."),       ['<Alt>Return']),
    ]},
    {'title': _("Interface navigation"), 'menu': _("_View"), 'shcuts': [
        ('focusdoc',          _("Focus Editor"),           ['<Control>e']),
        ('showsidebar',       _("Sidebar Visibility"),     ['F9']),
        ('nextsidebar',       _("Next Sidebar Pane"),      ['<Shift>F9']),
        ('focussidebar',      None,                        ['<Control>F9']),
        ('showbottom',        _("Bottom Pane Visibility"), ['F8']),
        ('nextbottom',        _("Next Bottom Pane"),       ['<Shift>F8']),
        ('splitsinglemode',   _("Split View Single Mode"), ['<Control><Shift>g']),
        ('nextgroup',         _("Next Tabs Group"),        ['<Control>g']),
        ('prevtab',           _("Previous Tab"),           ['<Control><Shift>Tab']),
        ('nexttab',           _("Next Tab"),               ['<Control>Tab']),
        ('prevvisitedtab',    _("Previous Visited Tab"),   ['<Alt>Page_Up']),
        ('nextvisitedtab',    _("Next Visited Tab"),       ['<Alt>Page_Down']),
    ]},
    {'title': _("Code navigation"), 'menu': _("_Code"), 'shcuts': [
        ('minimap',           _("Minimap Visibility"),     ['<Control>m']),
        ('bookmarkadd',       _("Bookmark Line"),          ['<Control>F2']),
        ('bookmarknext',      _("Next Bookmark"),          ['F2']),
        ('goprev',            _("Previous Position"),      ['<Alt>Left']),
        ('gonext',            _("Next Position"),          ['<Alt>Right']),
        ('jumpdef',           _("Jump to Definition"),     ['F12']),
    ]},
    {'title': _("Searching"), 'menu': _("_Search"), 'shcuts': [
        ('find',              _("Find..."),                ['<Control>f']),
        ('findreplace',       _("Replace..."),             ['<Control>h']),
        ('findprev',          _("Previous Match"),         ['<Shift>F3']),
        ('findnext',          _("Next Match"),             ['F3']),
        ('replace',           _("Replace Match"),          ['<Control>F3']),
        ('clearhighlight',    _("Clear Search Highlight"), []),
    ]},
    {'title': _("Preferences"), 'menu': _("_Preferences"), 'shcuts': [
        ('config::settings',  _("Global Settings"),        []),
        ('config::style',     _("User Style"),             []),
        ('config::prjdef',    _("Projects Defaults"),      []),
        ('config::prj',       _("Project Settings"),       []),
    ]},
    {'title': _("Help"), 'menu': _("_Help"), 'shcuts': [
        ('help',              _("List Help Topics"),       ['F1']),
        ('website',           _("Visit Website"),          []),
        ('show-help-overlay', _("Keyboard Shortcuts"),     ['<Control>question']),
        ('about',             _("About"),                  []),
    ]},
    {'title': _("Customizable actions"), 'menu': None, 'shcuts': [
        ('customtask-f4',     _("Run Custom Task"),        ['F4']),
        ('customtask-f5',     _("Run Custom Task"),        ['F5']),
        ('customtask-f6',     _("Run Custom Task"),        ['F6']),
        ('customtask-f7',     _("Run Custom Task"),        ['F7']),
    ]},
]


def ed_set_shortcuts_for_action(action, *shortcuts, replace=False):
    """Set keyboard shortcuts for an action."""
    for group in ED_ACTIONS:
        for shcut in group['shcuts']:
            if action == shcut[0]:
                if replace:
                    shcut[2].clear()
                shcut[2].extend(shortcuts)


class EdActionsManager():
    """Actions manager."""

    def __init__(self, wbench):
        self.wbench = wbench
        self.build_help_overlay(wbench.win)
        self.connect_actions(wbench.win)

    def free(self):
        """Release resources and break circular references."""
        self.wbench = None

    @staticmethod
    def setup_shortcuts(app):
        """Map actions keyboard shortcuts to application."""
        for group in ED_ACTIONS:
            for shcut in group['shcuts']:
                app.set_accels_for_action(f'win.{shcut[0]}', shcut[2])

    @staticmethod
    def setup_menu(app):
        """Build application's menubar."""
        menu = Gio.Menu()
        # Populate from app's actions
        for group in ED_ACTIONS:
            if not group.get('menu', None):
                continue
            submenu = Gio.Menu()
            for shcut in group['shcuts']:
                if shcut[1]:
                    submenu.append(shcut[1], f'win.{shcut[0]}')
            submenu.freeze()
            menu.append_submenu(group['menu'], submenu)
        # Populate from extensions
        extmenu = Gio.Menu()
        commands = {}
        for cmdlist in ed_extensions_get_data('commands', app):
            commands.update(cmdlist)
        for cmd in sorted(commands):
            if commands[cmd][2]:
                submenu = Gio.Menu()
                for p in commands[cmd][3]:
                    submenu.append(p, f'''ext.{cmd}('{p}')''')
                submenu.freeze()
                extmenu.append_submenu(commands[cmd][1], submenu)
            elif commands[cmd][2] is not None:
                extmenu.append(commands[cmd][1], f'''ext.{cmd}('')''')
        extmenu.freeze()
        menu.insert_submenu(menu.get_n_items() - 1, _("E_xtensions"), extmenu)
        app.set_menubar(menu)

    def connect_actions(self, win):
        """Connect window actions with handlers."""
        # Config
        action = Gio.SimpleAction.new('config', GLib.VariantType('s'))
        action.connect('activate', self.action_config)
        win.add_action(action)
        # Global actions
        for group in ED_ACTIONS:
            for shcut in group['shcuts']:
                if not Gio.Action.name_is_valid(shcut[0]):
                    continue   # detailed name, declare elsewhere with variant
                if win.has_action(shcut[0]):
                    continue   # action already set, e.g. by set_help_overlay()
                cbk_name = f'action_{shcut[0]}'.replace('-', '_')
                if hasattr(self, cbk_name):
                    action = Gio.SimpleAction.new(shcut[0], None)
                    action.connect('activate', getattr(self, cbk_name))
                    win.add_action(action)
        # Extensions
        ext_ag = Gio.SimpleActionGroup()
        win.insert_action_group('ext', ext_ag)
        for cmdlist in ed_extensions_get_data('commands', win.get_application()):
            for cmd in cmdlist:
                if ext_ag.has_action(cmd):
                    continue
                action = Gio.SimpleAction.new(cmd, GLib.VariantType('s'))
                action.connect('activate', self.action_ext_command)
                ext_ag.add_action(action)

    def build_help_overlay(self, win):
        """Build shortcuts help window."""
        assert not win.has_action('show-help-overlay')
        h = max(len([_s for _s in _g['shcuts'] if _s[2]]) for _g in ED_ACTIONS)
        scwin = Gtk.ShortcutsWindow(section_name='shortcuts')
        scsec = Gtk.ShortcutsSection(section_name='shortcuts', max_height=h)
        for grp in reversed(ED_ACTIONS):
            scgrp = Gtk.ShortcutsGroup(title=grp['title'])
            for shcut in grp['shcuts']:
                if shcut[1] and shcut[2]:
                    scgrp.add_shortcut(Gtk.ShortcutsShortcut(title=shcut[1], action_name=f'win.{shcut[0]}'))
            scsec.add_group(scgrp)
        scwin.add_section(scsec)
        win.set_help_overlay(scwin)

    def menu_runcmd(self, cmd, *params):
        """Run command from menu."""
        if not self.wbench.cmdlauncher.run(cmd, *params):
            ed_set_transient_style(self.wbench.win, 'ed-class-error')
            self.wbench.win.error_bell()
        self.wbench.focus_active_doc()

    def on_open_response(self, fc):
        """Callback on file chooser dialog response."""
        gfiles = sorted(fc.get_files(), key=lambda _gf: EdFileName.new(_gf.get_parse_name()))
        self.wbench.request_open(gfiles, "newwin=false")

    def action_showsidebar(self, action, param):
        """Action handler for sidebar visibility."""
        self.wbench.sidebar.set_visibility()

    def action_nextsidebar(self, action, param):
        """Action handler for switching to next sidebar page."""
        self.wbench.sidebar.show_next_page()

    def action_focussidebar(self, action, param):
        """Action handler for grabbing the sidebar's focus."""
        s = self.wbench.get_ui('ed_box_sidebarswitcher')
        s.child_focus(Gtk.DirectionType.TAB_FORWARD)

    def action_showbottom(self, action, param):
        """Action handler for bottom pane visibility."""
        stack = self.wbench.get_ui('ed_stack_bottom')
        if stack.get_pages().get_n_items() > 0:
            w = self.wbench.get_ui('ed_box_bottom')
            w.set_visible(not w.get_visible())   # toggle visibility
            if not w.get_visible():
                self.wbench.focus_active_doc()

    def action_nextbottom(self, action, param):
        """Action handler for switching to next bottom page."""
        if not self.wbench.get_ui('ed_box_bottom').get_visible():
            return
        stack = self.wbench.get_ui('ed_stack_bottom')
        if stackpages := stack.get_pages():
            for i, page in enumerate(stackpages):
                if page.get_child() is stack.get_visible_child():
                    n = (i + 1) % len(stackpages)
                    stack.set_visible_child(stackpages[n].get_child())
                    break

    def action_minimap(self, action, param):
        """Action handler for minimap visibility."""
        visible = self.wbench.get_config('minimap_show', bool, False)
        self.wbench.set_config('minimap_show', not visible)
        for w, d in self.wbench.docs.items():
            d.on_action(action.get_name())

    def action_syncprj(self, action, param):
        """Action handler for project reload sync."""
        if self.wbench.project is None:
            self.wbench.config.reset_file_config()
            self.wbench.config.load_user_config()
        else:
            GLib.idle_add(self.wbench.project.reload, priority=GLib.PRIORITY_LOW)

    def action_bookmarkadd(self, action, param):
        """Action handler for adding bookmark."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.toggle_mark('bookmark')
            GLib.idle_add(self.wbench.on_marks_reload)

    def action_bookmarknext(self, action, param):
        """Action handler for going to next bookmark."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.goto_next_mark('bookmark')

    def action_find(self, action, param):
        """Action handler for find setup."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            sel = d.get_selected_text()
            if sel is not None:
                if not sel.isprintable():
                    sel = GtkSource.utils_escape_search_text(sel)
                    self.wbench.get_ui('ed_toggle_findescape').set_active(True)
                self.wbench.get_ui('ed_entry_find').set_text(sel)
        self.wbench.sidebar.show_page('search')
        self.wbench.get_ui('ed_combo_find').grab_focus()

    def action_findreplace(self, action, param):
        """Action handler for replace setup."""
        self.action_find(action, param)
        self.wbench.get_ui('ed_combo_replace').grab_focus()

    def action_findprev(self, action, param):
        """Action handler for going to previous found occurrence."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.find(direction=-1, **self.wbench.get_search_settings(False))

    def action_findnext(self, action, param):
        """Action handler for going to next found occurrence."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.find(direction=+1, **self.wbench.get_search_settings(False))

    def action_replace(self, action, param):
        """Action handler for replacing current found occurrence."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.replace(**self.wbench.get_search_settings(True))

    def action_clearhighlight(self, action, param):
        """Action handler for replacing current found occurrence."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_goprev(self, action, param):
        """Action handler for going to previous position."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_gonext(self, action, param):
        """Action handler for going to next position."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_jumpdef(self, action, param):
        """Action handler for jumping to definition."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_undo(self, action, param):
        """Action handler for undoing last change."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.undo()

    def action_redo(self, action, param):
        """Action handler for redoing last undone change."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.redo()

    def action_cutline(self, action, param):
        """Action handler for cutting current line."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_dupline(self, action, param):
        """Action handler for duplicating current line."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_blockmode_del(self, action, param):
        """Action handler for activating squashing block mode."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action('blockmode', pad=False)

    def action_blockmode_pad(self, action, param):
        """Action handler for activating padding block mode."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action('blockmode', pad=True)

    def action_blockcopy(self, action, param):
        """Action handler for block copying."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_blockpaste(self, action, param):
        """Action handler for block pasting."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_comment(self, action, param):
        """Action handler for commenting code."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_docsettings(self, action, param):
        """Action handler for document settings."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            d.on_action(action.get_name())

    def action_config(self, action, param):
        """Action handler for configuration settings."""
        if param is not None:
            GLib.mkdir_with_parents(ed_get_configpath(), 0o700)
            match param.unpack():
                case 'settings':
                    prompt = "# Global user settings, restart the app to apply\n[core]\n"
                    gfile = Gio.File.new_for_path(ed_get_configpath("settings.toml"))
                    self.wbench.request_open([gfile], "", initdata=prompt)
                case 'style':
                    prompt = "/* User style, restart the app to apply */\n"
                    gfile = Gio.File.new_for_path(ed_get_configpath("gtk4.css"))
                    self.wbench.request_open([gfile], "", initdata=prompt)
                case 'prjdef':
                    gfile = Gio.File.new_for_path(ed_get_configpath("defaults.json"))
                    self.wbench.request_open([gfile], "", initdata=EdProject.get_config_txt())
                case 'prj':
                    if self.wbench.project:
                        self.wbench.project.open_config()

    def action_help(self, action, param):
        """Action handler for showing help topics."""
        e = self.wbench.get_ui('ed_entry_cmd')
        e.grab_focus()
        e.delete_text(0, -1)
        e.insert_text("help ", 0)
        e.set_position(-1)

    def action_website(self, action, param):
        """Action handler for launching the website in default browser."""
        Gtk.UriLauncher.new(ED_WEBSITE).launch(self.wbench.win, None, None)

    def action_about(self, action, param):
        """Action handler for showing the about dialog."""
        self.wbench.cmdlauncher.run("help", "about")

    def action_runcmd(self, action, param):
        """Action handler for entering command to run."""
        e = self.wbench.get_ui('ed_entry_cmd')
        e.grab_focus()
        if not e.get_text():   # show commands popup if entry is empty
            e.insert_text(" ", 0)
            e.delete_text(0, -1)

    def action_ext_command(self, action, param):
        """Action handler for "extensions" menu items."""
        if param is not None:
            cmd = action.get_name()
            val = param.unpack()
            cmd_params = (val,) if val else ()
            self.wbench.win.set_focus(None)
            GLib.idle_add(self.menu_runcmd, cmd, *cmd_params)

    def action_customtask_f4(self, action, param):
        """Action handler for custom task F4."""
        if self.wbench.project is not None:
            for name in self.wbench.project.get_tasks_for_key('F4'):
                self.wbench.project.run_task(name)

    def action_customtask_f5(self, action, param):
        """Action handler for custom task F5."""
        if self.wbench.project is not None:
            for name in self.wbench.project.get_tasks_for_key('F5'):
                self.wbench.project.run_task(name)

    def action_customtask_f6(self, action, param):
        """Action handler for custom task F6."""
        if self.wbench.project is not None:
            for name in self.wbench.project.get_tasks_for_key('F6'):
                self.wbench.project.run_task(name)

    def action_customtask_f7(self, action, param):
        """Action handler for custom task F7."""
        if self.wbench.project is not None:
            for name in self.wbench.project.get_tasks_for_key('F7'):
                self.wbench.project.run_task(name)

    def action_openprj(self, action, param):
        """Action handler for selecting a file to open."""
        parent = self.wbench.win
        fcp = EdFileChooser(parent, _("Open Project"))
        fcp.set_response_handler(self.on_open_response)
        fcp.show_select_folder()

    def action_openfile(self, action, param):
        """Action handler for selecting a file to open."""
        parent = self.wbench.win
        fco = EdFileChooser(parent, _("Open Files"))
        fco.set_response_handler(self.on_open_response)
        fco.show_open_multiple(folder=self.wbench.get_prj_gpath())

    def action_newwbench(self, action, param):
        """Action handler for opening a new empty workbench."""
        wb = self.wbench.app.start_instance()
        if wb is self.wbench:
            self.wbench.win.error_bell()
        else:
            GLib.idle_add(wb.show_window)

    def action_newdoc(self, action, param):
        """Action handler for opening a new empty document."""
        self.wbench.add_doc(None, docclass='EdSrcEditor')

    def action_newtab(self, action, param):
        """Action handler for opening a new empty tab."""
        self.wbench.add_doc(None)

    def action_focusdoc(self, action, param):
        """Action handler for focusing active document."""
        self.wbench.focus_active_doc()

    def action_splitsinglemode(self, action, param):
        """Action handler for single tabs group mode."""
        self.wbench.single_notebook = not self.wbench.single_notebook
        self.wbench.update_single_notebook()

    def action_nextgroup(self, action, param):
        """Action handler for going to next tabs group (notebook)."""
        i = self.wbench.notebooks.index(self.wbench.active_notebook)
        i = (i + 1) % len(self.wbench.notebooks)
        self.wbench.select_notebook(self.wbench.notebooks[i])

    def action_prevtab(self, action, param):
        """Action handler for going to previous tab by position."""
        self.wbench.active_notebook.select_prev_page()

    def action_nexttab(self, action, param):
        """Action handler for going to next tab by position."""
        self.wbench.active_notebook.select_next_page()

    def action_prevvisitedtab(self, action, param):
        """Action handler for going to previous visited tab."""
        self.wbench.active_notebook.select_prev_visited_page()

    def action_nextvisitedtab(self, action, param):
        """Action handler for going to next visited tab."""
        self.wbench.active_notebook.select_next_visited_page()

    def action_save(self, action, param):
        """Action handler for saving current document."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            self.wbench.save_doc(d)

    def action_saveas(self, action, param):
        """Action handler for saving current document under another path."""
        w, d = self.wbench.get_active_doc()
        if d is not None:
            self.wbench.save_doc_as(d)

    def action_saveall(self, action, param):
        """Action handler for saving all docs."""
        for d in reversed(list(self.wbench.get_docs_sorted())):
            if gf := d.get_gfile():
                if d.save_conflict:
                    self.wbench.notifier.question(_("Save skipped, conflict with external changes!"),
                                                  ed_get_gfile_display_path(gf),
                                                  _("Examine"),
                                                  lambda _a, _d: _a and self.wbench.select_doc(_d),
                                                  d)
                else:
                    self.wbench.save_doc(d)

    def action_closedoc(self, action, param):
        """Action handler for closing current document."""
        w, d = self.wbench.get_active_doc()
        if w is not None:
            self.wbench.close_doc_request(w)

    def action_quit(self, action, param):
        """Action handler for quitting."""
        self.wbench.win.close()
