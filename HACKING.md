EdiTidE - Hacking
=================

EdiTidE is written in Python, so easily hackable.


Appearance
----------

* GUI theme follows the system config.
* Appearance of specific elements can be customized in the CSS stylesheet.
* GUI layout is mostly designed with Glade, see UI files in folder `data`.
* Code syntax highlight theme is defined in `data/styles`.


Supported languages
-------------------

Using `GtkSourceView` for syntax highlighting, and `ctags` for symbols.
Please refer to their specific documentation for supporting new features.

* [GtkSourceView language definition](https://gnome.pages.gitlab.gnome.org/gtksourceview/gtksourceview5/lang-tutorial.html)
* [ctags manpage](https://docs.ctags.io/en/latest/man/ctags.1.html)
* [ctags configuration](https://docs.ctags.io/en/latest/option-file.html)


Extensions
----------

User extensions shall be placed in folder `${XDG_DATA_HOME}/editide/exts4`.
Filename must start with "ext_".

Each extension must implement a `ed_ext_init()` callout, executed at
loading.

Optionally, feature callouts `ed_ext_xxx()` can be provided, with "xxx"
the name of the feature. Currently supported features: `get_commands`,
`get_completion`, `notif_wbench_opened`, `notif_wbench_closed`,
`notif_project_loaded`, `notif_notebook_added`, `notif_doc_added`,
`notif_doc_loaded`, `notif_doc_symbols`, `notif_doc_save`, `notif_doc_closed`.


Debug
-----

Use the `py` command to open a Python interpreter for introspection.
